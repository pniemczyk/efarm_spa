RailsApp::Application.routes.draw do
  get  'uploads/shp' => 'uploads#form'
  post 'uploads/shp' => 'uploads#confirm'
 # accounts
  get "login"                                => 'accounts#index', as: :login_and_register_page
  get "confirm_email/:confirmation_token"    => 'accounts#confirm_email', as: :confirm_email
  get "reset_password/:reset_password_token" => 'accounts#reset_password_start', as: :reset_password_start

  post "login"           => 'accounts#login'
  get  "logout"          => 'accounts#logout'
  post "register"        => 'accounts#register'
  post 'forgot_password' => 'accounts#forgot_password'
  post 'reset_password/:reset_password_token' => 'accounts#reset_password_finish', as: :reset_password_finish


  namespace :dev do
    get 'es_agri'     => 'es#agri'
    get 'es_plants'   => 'es#plants'
    get 'es_precinct' => 'es#precinct'
    get 'es_precinct_fields' => 'es#precinct_fields'

    get "modules" => 'modules#index'
    get "templates" => 'templates#index'
    get "templates/field_card" => 'templates#field_card'
    get "js" => 'js#index'
  end

  namespace :api do
    post "logger/error"
    post "emails/invite_friends" => 'send_mails#invite_friends', as: :send_email_invite_friends
    get  "profiles/own" => 'profiles#show_own',         as: :profiles_own
    put  "profiles/own" => 'profiles#update_own',       as: :profiles_own
    post "profiles/own" => 'profiles#change_password',  as: :profiles_own
    get  "settings"     => 'settings#index',            as: :settings
    put  "settings"     => 'settings#update',           as: :settings
    scope ':season', :season => /\d{4}/ do
    end

    # farms
    get   "farms"             => 'farms#index',        as: :farms_index
    get   "farms/:id"         => 'farms#show',         as: :farms_show
    get   "farms/:id/seasons" => 'farms#farm_seasons', as: :farm_seasons_show
    post  "farms"             => 'farms#create', as: :farms_create
    put   "farms/:id"         => 'farms#update', as: :farms_update

    post  "farm_in_seasons/active_deactive/:id" => 'farm_in_seasons#active_deactive', as: :active_deactive_farm_in_season

    # seasons
    get   "seasons"           => "seasons#index", as: :seasons_index
    get   "seasons/:id/farms" => "seasons#season_farms", as: :season_farms_show

    # fields
    get    'fis/:fis_id/fields'     => 'fields#index',  as: :fields_index   # localhost:3000/api/fis/19/fields
    post   'fis/:fis_id/fields'     => 'fields#create', as: :fields_create  # localhost:3000/api/fis/19/fields
    put    'fis/:fis_id/fields/:id' => 'fields#update', as: :fields_update  # localhost:3000/api/fis/19/fields/10
    delete 'fis/:fis_id/fields/:id' => 'fields#delete', as: :fields_delete  # localhost:3000/api/fis/19/fields/10
    # dicts
    get 'dicts/get_precinct' => 'dicts#get_precinct', as: :get_precinct

    get 'dicts/search_plants'    => 'dicts#search_plants',    as: :search_plants
    get 'dicts/search_agri_pack' => 'dicts#search_agri_pack', as: :search_agri_pack
    get 'dicts/search_precinct'  => 'dicts#search_precinct',  as: :search_precinct
    get 'dicts/search_precinct_by_code' => 'dicts#search_precinct_by_code', as: :search_precinct_by_code

    # out od domain
    post "files/upload_shape"  => 'files_upload#upload_shape', as: :upload_shape

    get 'parcels_by_geo'       => 'parcels#by_geo', as: :parcels
  end

  # home
  root :to => 'home#index'
  get  "help"          => 'home#help'
  get  "home/index"
  get  "home/wizard"
  post "home/wizard_completed"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
