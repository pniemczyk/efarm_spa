require 'handlebars_assets/config'
HandlebarsAssets::Config.path_prefix        = 'app/templates'
HandlebarsAssets::Config.haml_options       = { remove_whitespace: true, ugly: true }
HandlebarsAssets::Config.options            = { data: true }
HandlebarsAssets::Config.template_namespace = 'tpl'