module CustomAssets
  CSS = %w{
    ace_theme.css
    ace_theme_ie.css
    base.css
  }
  JS = %w{
    accounts.js
    core.js
    markdown.js
    modules.js
    page_account.js
    page_home.js
    spa.js
    templates.js
    themes-base.js
    themes.js
    for_map.js
  }
  ALL = CSS + JS
end
