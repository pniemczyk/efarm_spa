# config valid only for Capistrano 3.1
lock '3.1.0'
application = 'efarm'

set :application, application
set :repo_url,    'git@bitbucket.org:pniemczyk/efarm_spa.git'
set :deploy_to,   "/opt/www/#{application}"
set :branch,      ENV["REVISION"] || ENV["BRANCH_NAME"] || "master"
set :use_sudo,    false

stage = :acceptance

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Default deploy_to directory is /var/www/my_app
# set :deploy_to, '/var/www/my_app'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :info

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml config/application.yml config/newrelic.yml}

# Default value for linked_dirs is []
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# set(:config_files, %w{
#   application.yml
#   database.yml
#   newrelic.yml
# })

  # nginx.conf
  # log_rotation
  # monit
  # unicorn.rb
  # unicorn_init.sh

# set :executable_config_files, %w{unicorn_init.sh}
set :keep_releases, 5

set :rvm_ruby_version,  '2.1.1'
set :rvm_type,           :user
set :rvm_ruby_string,   "2.1.1@#{application}"

namespace :unicorn do
  %w{restart reload}.each do |command|
    desc "#{command} unicorn"
    task command.to_sym do
      on roles(:app) do
        within shared_path do
          execute :service, "unicorn_#{application}", "#{command}"
        end
      end
    end
  end
end

namespace :nginx do
  %w{restart reload}.each do |command|
    desc "#{command} nginx"
    task command.to_sym do
      on roles(:app) do
        within shared_path do
          execute :sudo, 'service', "nginx", "#{command}"
        end
      end
    end
  end
end

namespace :deploy do

  desc "Create database"
  task :create_db do
    on primary :db do
      within release_path do
        execute :rake, "db:create"
      end
    end
  end

  # before 'deploy:setup', 'rvm:install_rvm'
  # before 'deploy:setup', 'rvm:install_ruby'
  after  'deploy:starting', 'deploy:cleanup'
  before 'deploy:migrate', 'deploy:create_db'
  before 'deploy:assets:precompile', 'deploy:migrate'

  before :production, :warn do
    info 'Deploying to production, hold tight!'
    stage = :production
  end

  after  :finishing, 'deploy:cleanup'

  desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:web), in: :sequence, wait: 5 do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end
  before :deploy, "deploy:check_revision"

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      Rake::Task["unicorn:restart"].invoke
      # Rake::Task["nginx:restart"].invoke
    end
  end

  after :publishing, :restart

  desc "Place release tag into Git and push it to server."
  task :push_deploy_tag do
    on roles(:app) do
      user         = `git config --get user.name`
      email        = `git config --get user.email`
      revision     = `git rev-parse origin/master`
      release_name = release_path.to_s.split('/')[-1]

      puts `git tag #{stage}_#{release_name} #{revision} -m "Deployed by #{user} <#{email}>"`
      puts `git push --tags`
    end
  end
  after :restart, 'deploy:push_deploy_tag'
  # task :cleanup_deploy_tag do
  #   on roles(:app) do
  #     count = fetch(:keep_releases, 5).to_i
  #     if count >= releases.length
  #       logger.important "no old release tags to clean up"
  #     else
  #       logger.info "keeping #{count} of #{releases.length} release tags"

  #       tags = (releases - releases.last(count)).map { |release| "#{stage}_#{release}" }

  #       tags.each do |tag|
  #         `git tag -d #{tag}`
  #         `git push origin :refs/tags/#{tag}`
  #       end
  #     end
  #   end
  # end
  # before :cleanup, "deploy:cleanup_deploy_tag"

  # after :restart, :clear_cache do
  #   on roles(:web), in: :groups, limit: 3, wait: 10 do
  #     within release_path do
  #       execute :rake, 'cache:clear'
  #     end
  #   end
  # end
end
