set :stage,     :acceptance
set :rails_env, :acceptance
fetch(:default_env).merge!(rails_env: :acceptance)

domain = 'acc.efarm.pl'
user   = 'deploy'

role :app, "#{user}@#{domain}"
role :web, "#{user}@#{domain}"
role :db,  "#{user}@#{domain}", primary: true

server domain, user: user, roles: %w{db web app}