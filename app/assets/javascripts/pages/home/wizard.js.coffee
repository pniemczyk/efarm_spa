class window.App.Controllers.ConfigurationWizard
  constructor: (@currentYear)->
    @$el = $('#baseConfigurationWizard')
    @$form = $('#wizardSetupFarm')
    @_buildSeasons()
    @init()

  seasons: []
  _buildSeasons: ->
    (
      @seasons.push
        value: @currentYear-i
        name: "#{@currentYear-i}/#{@currentYear-i+1}"
        selected: i is 0
    ) for i in [-2..2]
    source   = $("#seasonElem").html()
    template = Handlebars.compile(source)
    $('.js-wizard-seasons').html(template(seasons: @seasons))

  _changeEvent: (e, data) =>
    if data? && data.step is 3 && data.direction is 'next'
      e.preventDefault() unless @$form.valid()

  _finishedEvent: (e, data) => @$form.submit()

  _seasonClickEvent: (e) =>
    @$el.find('.js-wizard-seasons li').removeClass('selected')
    $this = $(e.currentTarget)
    $this.addClass('selected')
    $('#season').val($this.data('year'))

  _inviteFriends: -> App.commands.execute('InviteFriends-showModal')

  _validateFormDefaultConfig:
    errorElement: 'span'
    errorClass: 'help-inline'
    focusInvalid: false
    rules:
      name:
        required: true
      short_name:
        required: true
    messages:
      name:
        required: 'wypełnienie jest wymagane'
      short_name:
        required: 'wypełnienie jest wymagane'
    invalidHandler: (e ,validator) ->
    highlight: (e) -> $(e).closest('.control-group').removeClass('info').addClass('error')
    success: (e) ->
      $(e).closest('.control-group').removeClass('error').addClass('info')
      $(e).remove()
    errorPlacement: (error, element) -> error.insertAfter(element)
    invalidHandler: (form) ->

  init: ->
    @$el.find('.js-wizard-seasons li').on('click', @_seasonClickEvent)
    @$el.find('#wizardBtnInviteFriend').on('click', @_inviteFriends)
    @$el.wizard()
    @$el.on('change', @_changeEvent)
    @$el.on('finished', @_finishedEvent)
    @$form.validate(@_validateFormDefaultConfig)
    @$el.fadeIn()