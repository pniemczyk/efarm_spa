﻿class window.App.Controllers.Account
  loginFormName = '#loginForm'
  signupFormName = '#singupForm'
  forgotPasswordFormName = '#forgotForm'
  resetPasswordFormName = '#resetPasswordForm'

  constructor: ->
    @bindEvents()
    @showStartMessage()
    @registerFormValidations()

  forgotSubmitDisabled: false
  singupSubmitDisabled: false
  resetPasswordSubmitDisabled: false

  showStartMessage: ->
    if window.App.Message?
      messageClasses = "gritter-#{window.App.Message.kind}";
      @message("Informacja", window.App.Message.text, messageClasses, 5000)

  showBox: (id) ->
    $('.widget-box.visible').removeClass('visible')
    $("##{id}").addClass('visible')

  bindEvents: ->
    $('.back-to-login-link').click(@showLoginBox)
    $('.user-signup-link').click(@showSingupBox)
    $('.forgot-password-link').click(@showForgotBox)

    $('#loginSubmitBtn').click(@loginSubmit)
    $('#forgotSubmitBtn').click(@forgotSubmit)
    $('#singupSubmitBtn').click(@singupSubmit)

    $('#resetPasswordSubmitBtn').click(@resetPasswordSubmit)

  registerFormValidations: ->
    $(loginFormName).validate($.extend(@_validationBaseConfig(), @_loginValidationConfig()))
    $(signupFormName).validate($.extend(@_validationBaseConfig(), @_registrationValidationConfig()))
    $(forgotPasswordFormName).validate($.extend(@_validationBaseConfig(), @_forgotPasswordValidationConfig()))
    $(resetPasswordFormName).validate($.extend(@_validationBaseConfig(), @_resetPasswordValidationConfig()))

  loginSubmit: (e) =>
    loginForm = $(loginFormName)
    if loginForm.valid()
      $('#loginSubmitBtn > i').attr('class', @spinnerClasses)
      loginForm.submit()

  # registration
  singupSubmit: (e) =>
    e.preventDefault()
    return if @singupSubmitDisabled
    signupForm = $(signupFormName)

    if(signupForm.valid())
      @singupSubmitDisabled = true
      $('#singupSubmitBtn > i').attr('class', @spinnerClasses)
      data = signupForm.serialize()
      url = signupForm.attr("action")
      $.post(url, data).done(@singupSubmitDone).fail(@singupSubmitFail)

  singupSubmitDone: (data) =>
    @singupSubmitDisabled = false
    $('#singupSubmitBtn > i').attr('class', @singupButtonClasses)

    if data.status is 'OK'
      @showBox(@loginBoxId)
      @message("Informacja", data.message, 'gritter-info') if data.message?
    else
      @message(data.message, data.errors.toString(), 'gritter-error')

  singupSubmitFail: (xhr, textStatus, errorThrown) =>
    @singupSubmitDisabled = false
    $('#singupSubmitBtn > i').attr('class', @singupButtonClasses)
    @message("Błąd", "Błąd podczas połączenia z serwerem, spróbuj później.", "gritter-error")

  #forgot password
  forgotSubmit: (e) =>
    e.preventDefault()
    return if @forgotSubmitDisabled
    forgotPasswordForm = $(forgotPasswordFormName)

    if(forgotPasswordForm.valid())
      @forgotSubmitDisabled = true
      $('#forgotSubmitBtn > i').attr('class', @spinnerClasses)
      data = forgotPasswordForm.serialize()
      url = forgotPasswordForm.attr("action")
      $.post(url, data).done(@forgotSubmitDone).fail(@forgotSubmitFail)

  forgotSubmitDone: (data) =>
    @forgotSubmitDisabled = false
    $('#forgotSubmitBtn > i').attr('class', @forgotButtonClasses)

    if data.status is 'OK'
      @showBox(@loginBoxId)
      @message("Informacja", data.message, 'gritter-info')
    else
      @message(data.message, data.errors.toString(), 'gritter-error')

  forgotSubmitFail: (xhr, textStatus, errorThrown) =>
    @forgotSubmitDisabled = false
    $('#forgotSubmitBtn > i').attr('class', @forgotButtonClasses)
    @message("Błąd", "Błąd podczas połączenia z serwerem, spróbuj później.", "gritter-error")

  # reset password
  resetPasswordSubmit: (e) =>
    resetPasswordForm = $(resetPasswordFormName)
    if resetPasswordForm.valid()
      $('#resetPasswordSubmitBtn > i').attr('class', @spinnerClasses)
      resetPasswordForm.submit()

  showLoginBox: (e) =>
    e.preventDefault()
    @showBox(@loginBoxId)
  showSingupBox: (e) =>
    e.preventDefault()
    @showBox(@signupBoxId)
  showForgotBox: (e) =>
    e.preventDefault()
    @showBox(@forgotBoxId)

  loginBoxId:  'login-box'
  signupBoxId: 'signup-box'
  forgotBoxId: 'forgot-box'
  spinnerClasses: 'icon-spinner icon-spin bigger-125'
  forgotButtonClasses: 'icon-lightbulb'
  singupButtonClasses: 'icon-arrow-right icon-on-right'

  message: (title, msg, classes='gritter-info', center=false, time=5000) ->
    classes = "#{classes} gritter-center" if center is true
    $.gritter.add({ title: title, text: msg, class_name: classes, time: time})

  # validators configs

  _messages:
    email: 'Nieprawidłowy format adresu email.'
    required: 'Pole jest wymagane'
    minlength: "Pole musi mieć minimum {0} znaków"
    equalTo: 'Powtórz hasło'
    acceptTerms: 'Zaakceptuj regulamin'

  _validationBaseConfig: ->
    highlight: (e) -> $(e).closest('.js-control-group').addClass('error')
    success: (e) ->
      $(e).closest('.js-control-group').removeClass('error')
      $(e).remove()

    errorPlacement: (error, element) ->
      if (element.is(":checkbox") )
        error.appendTo(element.next())
      else
        error.insertAfter(element)

  _loginValidationConfig: ->
    rules:
      "email":
        required: true
        email: true
      "password":
        required: true
    messages:
      "email":
        required: @_messages['required']
        email: @_messages['email']
      "password":
        required: @_messages['required']

  _forgotPasswordValidationConfig: ->
    rules:
      "forgotPassword[email]":
        required: true
        email: true
    messages:
      "forgotPassword[email]":
        required: @_messages['required']
        email: @_messages['email']

  _registrationValidationConfig: ->
    rules:
      "registration[email]":
        required: true
        email: true
      "registration[password]":
        required: true
        minlength: 6
      "registration[password_confirmation]":
        equalTo: '#registrationPassword'
      "registration[accept_terms]":
        required: true
    messages:
      "registration[email]":
        required: @_messages['required']
        email: @_messages['email']
      "registration[password]":
        required: @_messages['required']
        minlength: @_messages['minlength']
      "registration[password_confirmation]":
        equalTo: @_messages['equalTo']
      "registration[accept_terms]":
        required: @_messages['acceptTerms']

  _resetPasswordValidationConfig: ->
    rules:
      "resetPassword[password]":
        required: true
        minlength: 6
      "resetPassword[password_confirmation]":
        equalTo: '#password'
    messages:
      "resetPassword[password]":
        required: @_messages['required']
        minlength: @_messages['minlength']
      "resetPassword[password_confirmation]":
        equalTo: @_messages['equalTo']