﻿window["ace"] = {}  unless "ace" of window
jQuery ($) ->

  #at some places we try to use 'tap' event instead of 'click' if jquery mobile plugin is available
  window["ace"].click_event = (if $.fn.tap then "tap" else "click")

jQuery ($) ->

  #ace.click_event defined in ace-elements.js
  ace.handle_side_menu jQuery
  ace.enable_search_ahead jQuery
  ace.general_things jQuery #and settings
  ace.widget_boxes jQuery
  ace.widget_reload_handler jQuery #this is for demo only, you can remove and have your own function, please see examples/widget.html


###
//make sidebar scrollbar when it is fixed and some parts of it is out of view
//>> you should include jquery-ui and slimscroll javascript files in your file
//>> you can call this function when sidebar is clicked to be fixed
$('.nav-list').slimScroll({
height: '400px',
distance:0,
size : '6px'
});
###
ace.handle_side_menu = ($) ->
  $("#menu-toggler").on ace.click_event, ->
    $("#sidebar").toggleClass "display"
    $(this).toggleClass "display"
    false


  #mini
  $minimized = $("#sidebar").hasClass("menu-min")
  $("#sidebar-collapse").on ace.click_event, ->
    $minimized = $("#sidebar").hasClass("menu-min")
    # ace.settings.sidebar_collapsed not $minimized #@ ace-extra.js

  touch = "ontouchend" of document

  #opening submenu
  $(".nav-list").on ace.click_event, (e) ->

    #check to see if we have clicked on an element which is inside a .dropdown-toggle element?!
    #if so, it means we should toggle a submenu
    link_element = $(e.target).closest("a")
    return  if not link_element or link_element.length is 0 #if not clicked inside a link element
    $minimized = $("#sidebar").hasClass("menu-min")
    unless link_element.hasClass("dropdown-toggle") #it doesn't have a submenu return
      #just one thing before we return
      #if sidebar is collapsed(minimized) and we click on a first level menu item
      #and the click is on the icon, not on the menu text then let's cancel event and cancel navigation
      #Good for touch devices, that when the icon is tapped to see the menu text, navigation is cancelled
      #navigation is only done when menu text is tapped
      if $minimized and ace.click_event is "tap" and link_element.get(0).parentNode.parentNode is this #.nav-list
#i.e. only level-1 links
        text = link_element.find(".menu-text").get(0)
        #not clicking on the text or its children
        return false  if e.target isnt text and not $.contains(text, e.target)
      return

    #
    sub = link_element.next().get(0)

    #if we are opening this submenu, close all other submenus except the ".active" one
    unless $(sub).is(":visible") #if not open and visible, let's open it and make it visible
      parent_ul = $(sub.parentNode).closest("ul")
      return  if $minimized and parent_ul.hasClass("nav-list")
      parent_ul.find("> .open > .submenu").each ->

        #close all other open submenus except for the active one
        $(this).slideUp(200).parent().removeClass "open"  if this isnt sub and not $(@parentNode).hasClass("active")


    #uncomment the following line to close all submenus on deeper levels when closing a submenu
    #$(this).find('.open > .submenu').slideUp(0).parent().removeClass('open');
    else


    #uncomment the following line to close all submenus on deeper levels when closing a submenu
    #$(sub).find('.open > .submenu').slideUp(0).parent().removeClass('open');
    return false  if $minimized and $(sub.parentNode.parentNode).hasClass("nav-list")
    $(sub).slideToggle(200).parent().toggleClass "open"
    false


ace.general_things = ($) ->
  $(".ace-nav [class*=\"icon-animated-\"]").closest("a").on "click", ->
    icon = $(this).find("[class*=\"icon-animated-\"]").eq(0)
    $match = icon.attr("class").match(/icon\-animated\-([\d\w]+)/)
    icon.removeClass $match[0]
    $(this).off "click"

  $(".nav-list .badge[title],.nav-list .label[title]").tooltip placement: "right"

  #simple settings
  $("#ace-settings-btn").on ace.click_event, ->
    $(this).toggleClass "open"
    $("#ace-settings-box").toggleClass "open"

  # #@ ace-extra.js
  # $("#ace-settings-navbar").on("click", ->
  #   ace.settings.navbar_fixed @checked
  # ).each ->
  #   @checked = ace.settings.is("navbar", "fixed")

  # #@ ace-extra.js
  # $("#ace-settings-sidebar").on("click", ->
  #   ace.settings.sidebar_fixed @checked
  # ).each ->
  #   @checked = ace.settings.is("sidebar", "fixed")

  # #@ ace-extra.js
  # $("#ace-settings-breadcrumbs").on("click", ->
  #   ace.settings.breadcrumbs_fixed @checked
  # ).each ->
  #   @checked = ace.settings.is("breadcrumbs", "fixed")

  # #@ ace-extra.js
  # $("#ace-settings-add-container").on("click", ->
  #   ace.settings.main_container_fixed @checked
  # ).each ->
  #   @checked = ace.settings.is("main-container", "fixed")


  #Switching to RTL (right to left) Mode
  $("#ace-settings-rtl").removeAttr("checked").on "click", ->
    ace.switch_direction jQuery

  $("#btn-scroll-up").on ace.click_event, ->
    duration = Math.min(400, Math.max(100, parseInt($("html").scrollTop() / 3)))
    $("html,body").animate
      scrollTop: 0
    , duration
    false

  try
    $("#skin-colorpicker").ace_colorpicker()
  $("#skin-colorpicker").on "change", ->
    skin_class = $(this).find("option:selected").data("skin")
    body = $(document.body)
    body.removeClass "skin-1 skin-2 skin-3"
    body.addClass skin_class  unless skin_class is "default"
    if skin_class is "skin-1"
      $(".ace-nav > li.grey").addClass "dark"
    else
      $(".ace-nav > li.grey").removeClass "dark"
    if skin_class is "skin-2"
      $(".ace-nav > li").addClass "no-border margin-1"
      $(".ace-nav > li:not(:last-child)").addClass("light-pink").find("> a > [class*=\"icon-\"]").addClass("pink").end().eq(0).find(".badge").addClass "badge-warning"
    else
      $(".ace-nav > li").removeClass "no-border margin-1"
      $(".ace-nav > li:not(:last-child)").removeClass("light-pink").find("> a > [class*=\"icon-\"]").removeClass("pink").end().eq(0).find(".badge").removeClass "badge-warning"
    if skin_class is "skin-3"
      $(".ace-nav > li.grey").addClass("red").find(".badge").addClass "badge-yellow"
    else
      $(".ace-nav > li.grey").removeClass("red").find(".badge").removeClass "badge-yellow"


ace.widget_boxes = ($) ->
  $(document).on "hide.bs.collapse show.bs.collapse", (ev) ->
    hidden_id = ev.target.getAttribute("id")
    $("[href*=\"#" + hidden_id + "\"]").find("[class*=\"icon-\"]").each ->
      $icon = $(this)
      $match = undefined
      $icon_down = null
      $icon_up = null
      if $icon_down = $icon.attr("data-icon-show")
        $icon_up = $icon.attr("data-icon-hide")
      else if $match = $icon.attr("class").match(/icon\-(.*)\-(up|down)/)
        $icon_down = "icon-" + $match[1] + "-down"
        $icon_up = "icon-" + $match[1] + "-up"
      if $icon_down
        if ev.type is "show"
          $icon.removeClass($icon_down).addClass $icon_up
        else
          $icon.removeClass($icon_up).addClass $icon_down
        false #ignore other icons that match, one is enough


  $(document).on "click.ace.widget", "[data-action]", (ev) ->
    ev.preventDefault()
    $this = $(this)
    $action = $this.data("action")
    $box = $this.closest(".widget-box")
    return  if $box.hasClass("ui-sortable-helper")
    if $action is "collapse"
      event_name = (if $box.hasClass("collapsed") then "show" else "hide")
      event_complete_name = (if event_name is "show" then "shown" else "hidden")
      event = undefined
      $box.trigger event = $.Event(event_name + ".ace.widget")
      return  if event.isDefaultPrevented()
      $body = $box.find(".widget-body")
      $icon = $this.find("[class*=icon-]").eq(0)
      $match = $icon.attr("class").match(/icon\-(.*)\-(up|down)/)
      $icon_down = "icon-" + $match[1] + "-down"
      $icon_up = "icon-" + $match[1] + "-up"
      $body_inner = $body.find(".widget-body-inner")
      if $body_inner.length is 0
        $body = $body.wrapInner("<div class=\"widget-body-inner\"></div>").find(":first-child").eq(0)
      else
        $body = $body_inner.eq(0)
      expandSpeed = 300
      collapseSpeed = 200
      if event_name is "show"
        $icon.addClass($icon_up).removeClass $icon_down  if $icon
        $box.removeClass "collapsed"
        $body.slideUp 0, ->
          $body.slideDown expandSpeed, ->
            $box.trigger event = $.Event(event_complete_name + ".ace.widget")


      else
        $icon.addClass($icon_down).removeClass $icon_up  if $icon
        $body.slideUp collapseSpeed, ->
          $box.addClass "collapsed"
          $box.trigger event = $.Event(event_complete_name + ".ace.widget")

    else if $action is "close"
      event = undefined
      $box.trigger event = $.Event("close.ace.widget")
      return  if event.isDefaultPrevented()
      closeSpeed = parseInt($this.data("close-speed")) or 300
      $box.hide closeSpeed, ->
        $box.trigger event = $.Event("closed.ace.widget")
        $box.remove()

    else if $action is "reload"
      event = undefined
      $box.trigger event = $.Event("reload.ace.widget")
      return $this.blur()  if event.isDefaultPrevented()
      $remove = false
      if $box.css("position") is "static"
        $remove = true
        $box.addClass "position-relative"
      $box.append "<div class=\"widget-box-overlay\"><i class=\"icon-spinner icon-spin icon-2x white\"></i></div>"
      $box.one "reloaded.ace.widget", ->
        $box.find(".widget-box-overlay").remove()
        $box.removeClass "position-relative"  if $remove

    else if $action is "settings"
      event = $.Event("settings.ace.widget")
      $box.trigger event


ace.widget_reload_handler = ($) ->

  #***default action for reload in this demo
  #you should remove this and add your own handler for each specific .widget-box
  #when data is finished loading or processing is done you can call $box.trigger('reloaded.ace.widget')
  $(document).on "reload.ace.widget", ".widget-box", (ev) ->
    $box = $(this)

    #trigger the reloaded event after 1-2 seconds
    setTimeout (->
      $box.trigger "reloaded.ace.widget"
    ), parseInt(Math.random() * 1000 + 1000)



#you may want to do something like this:
###
$('#my-widget-box').on('reload.ace.widget', function(){
//load new data
//when finished trigger "reloaded"
$(this).trigger('reloaded.ace.widget');
});
###

#search box's dropdown autocomplete
ace.enable_search_ahead = ($) ->
  ace.variable_US_STATES = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
  try
    $("#nav-search-input").typeahead
      source: ace.variable_US_STATES
      updater: (item) ->
        $("#nav-search-input").focus()
        item


ace.switch_direction = ($) ->

  #toggle pull-right class on dropdown-menu

  #swap pull-left & pull-right
  swap_classes = (class1, class2) ->
    $body.find("." + class1).removeClass(class1).addClass("tmp-rtl-" + class1).end().find("." + class2).removeClass(class2).addClass(class1).end().find(".tmp-rtl-" + class1).removeClass("tmp-rtl-" + class1).addClass class2
  swap_styles = (style1, style2, elements) ->
    elements.each ->
      e = $(this)
      tmp = e.css(style2)
      e.css style2, e.css(style1)
      e.css style1, tmp

  $body = $(document.body)
  $body.toggleClass("rtl").find(".dropdown-menu:not(.datepicker-dropdown,.colorpicker)").toggleClass("pull-right").end().find(".pull-right:not(.dropdown-menu,blockquote,.profile-skills .pull-right)").removeClass("pull-right").addClass("tmp-rtl-pull-right").end().find(".pull-left:not(.dropdown-submenu,.profile-skills .pull-left)").removeClass("pull-left").addClass("pull-right").end().find(".tmp-rtl-pull-right").removeClass("tmp-rtl-pull-right").addClass("pull-left").end().find(".chosen-container").toggleClass("chosen-rtl").end()
  swap_classes "align-left", "align-right"
  swap_classes "no-padding-left", "no-padding-right"
  swap_classes "arrowed", "arrowed-right"
  swap_classes "arrowed-in", "arrowed-in-right"
  swap_classes "messagebar-item-left", "messagebar-item-right" #for inbox page

  #redraw the traffic pie chart on homepage with a different parameter
  placeholder = $("#piechart-placeholder")
  if placeholder.size() > 0
    pos = (if $(document.body).hasClass("rtl") then "nw" else "ne") #draw on north-west or north-east?
    placeholder.data("draw").call placeholder.get(0), placeholder, placeholder.data("chart"), pos