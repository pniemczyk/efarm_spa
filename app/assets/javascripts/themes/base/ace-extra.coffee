# To store configuration

# window["ace"] = {}  unless "ace" of window
# ace.config =
#   cookie_expiry: 604800 #1 week duration for saved settings
#   storage_method: 2 #2 means use cookies, 1 means localStorage, 0 means localStorage if available otherwise cookies

# ace.settings =
#   is: (item, status) ->

#     #such as ace.settings.is('navbar', 'fixed')
#     ace.data.get("settings", item + "-" + status) is 1

#   exists: (item, status) ->
#     ace.data.get("settings", item + "-" + status) isnt null

#   set: (item, status) ->
#     ace.data.set "settings", item + "-" + status, 1

#   unset: (item, status) ->
#     ace.data.set "settings", item + "-" + status, -1

#   remove: (item, status) ->
#     ace.data.remove "settings", item + "-" + status

#   navbar_fixed: (fix) ->
#     fix = fix or false
#     ace.settings.sidebar_fixed false  if not fix and ace.settings.is("sidebar", "fixed")
#     navbar = document.getElementById("navbar")
#     if fix
#       ace.addClass navbar, "navbar-fixed-top"  unless ace.hasClass(navbar, "navbar-fixed-top")
#       ace.addClass document.body, "navbar-fixed"  unless ace.hasClass(document.body, "navbar-fixed")
#       ace.settings.set "navbar", "fixed"
#     else
#       ace.removeClass navbar, "navbar-fixed-top"
#       ace.removeClass document.body, "navbar-fixed"
#       ace.settings.unset "navbar", "fixed"
#     document.getElementById("ace-settings-navbar").checked = fix

#   breadcrumbs_fixed: (fix) ->
#     fix = fix or false
#     ace.settings.sidebar_fixed true  if fix and not ace.settings.is("sidebar", "fixed")
#     breadcrumbs = document.getElementById("breadcrumbs")
#     if fix
#       ace.addClass breadcrumbs, "breadcrumbs-fixed"  unless ace.hasClass(breadcrumbs, "breadcrumbs-fixed")
#       ace.addClass document.body, "breadcrumbs-fixed"  unless ace.hasClass(document.body, "breadcrumbs-fixed")
#       ace.settings.set "breadcrumbs", "fixed"
#     else
#       ace.removeClass breadcrumbs, "breadcrumbs-fixed"
#       ace.removeClass document.body, "breadcrumbs-fixed"
#       ace.settings.unset "breadcrumbs", "fixed"
#     document.getElementById("ace-settings-breadcrumbs").checked = fix

#   sidebar_fixed: (fix) ->
#     fix = fix or false
#     ace.settings.breadcrumbs_fixed false  if not fix and ace.settings.is("breadcrumbs", "fixed")
#     ace.settings.navbar_fixed true  if fix and not ace.settings.is("navbar", "fixed")
#     sidebar = document.getElementById("sidebar")
#     if fix
#       ace.addClass sidebar, "sidebar-fixed"  unless ace.hasClass(sidebar, "sidebar-fixed")
#       ace.settings.set "sidebar", "fixed"
#     else
#       ace.removeClass sidebar, "sidebar-fixed"
#       ace.settings.unset "sidebar", "fixed"
#     document.getElementById("ace-settings-sidebar").checked = fix

#   main_container_fixed: (inside) ->
#     inside = inside or false
#     main_container = document.getElementById("main-container")
#     navbar_container = document.getElementById("navbar-container")
#     if inside
#       ace.addClass main_container, "container"  unless ace.hasClass(main_container, "container")
#       ace.addClass navbar_container, "container"  unless ace.hasClass(navbar_container, "container")
#       ace.settings.set "main-container", "fixed"
#     else
#       ace.removeClass main_container, "container"
#       ace.removeClass navbar_container, "container"
#       ace.settings.unset "main-container", "fixed"
#     document.getElementById("ace-settings-add-container").checked = inside
#     if navigator.userAgent.match(/webkit/i)

#       #webkit has a problem redrawing and moving around the sidebar background in realtime
#       #so we do this, to force redraw
#       #there will be no problems with webkit if the ".container" class is statically put inside HTML code.
#       sidebar = document.getElementById("sidebar")
#       ace.toggleClass sidebar, "menu-min"
#       setTimeout (->
#         ace.toggleClass sidebar, "menu-min"
#       ), 0

#   sidebar_collapsed: (collpase) ->
#     collpase = collpase or false
#     sidebar = document.getElementById("sidebar")
#     icon = document.getElementById("sidebar-collapse").querySelector("[class*=\"icon-\"]")
#     $icon1 = icon.getAttribute("data-icon1") #the icon for expanded state
#     $icon2 = icon.getAttribute("data-icon2") #the icon for collapsed state
#     if collpase
#       ace.addClass sidebar, "menu-min"
#       ace.removeClass icon, $icon1
#       ace.addClass icon, $icon2
#       ace.settings.set "sidebar", "collapsed"
#     else
#       ace.removeClass sidebar, "menu-min"
#       ace.removeClass icon, $icon2
#       ace.addClass icon, $icon1
#       ace.settings.unset "sidebar", "collapsed"


# ###
# select_skin : function(skin) {
# }
# ###

# #check the status of something
# ace.settings.check = (item, val) ->
#   return  unless ace.settings.exists(item, val) #no such setting specified
#   status = ace.settings.is(item, val) #is breadcrumbs-fixed? or is sidebar-collapsed? etc
#   mustHaveClass =
#     "navbar-fixed": "navbar-fixed-top"
#     "sidebar-fixed": "sidebar-fixed"
#     "breadcrumbs-fixed": "breadcrumbs-fixed"
#     "sidebar-collapsed": "menu-min"
#     "main-container-fixed": "container"


#   #if an element doesn't have a specified class, but saved settings say it should, then add it
#   #for example, sidebar isn't .fixed, but user fixed it on a previous page
#   #or if an element has a specified class, but saved settings say it shouldn't, then remove it
#   #for example, sidebar by default is minimized (.menu-min hard coded), but user expanded it and now shouldn't have 'menu-min' class
#   target = document.getElementById(item) ##navbar, #sidebar, #breadcrumbs
#   ace.settings[item.replace("-", "_") + "_" + val] status  unless status is ace.hasClass(target, mustHaveClass[item + "-" + val]) #call the relevant function to mage the changes


# #save/retrieve data using localStorage or cookie
# #method == 1, use localStorage
# #method == 2, use cookies
# #method not specified, use localStorage if available, otherwise cookies
# ace.data_storage = (method, undefined_) ->
#   prefix = "ace."
#   storage = null
#   type = 0
#   if (method is 1 or method is `undefined`) and "localStorage" of window and window["localStorage"] isnt null
#     storage = ace.storage
#     type = 1
#   else if not storage? and (method is 2 or method is `undefined`) and "cookie" of document and document["cookie"] isnt null
#     storage = ace.cookie
#     type = 2

#   #var data = {}
#   @set = (namespace, key, value, undefined_) ->
#     return  unless storage
#     if value is `undefined` #no namespace here?
#       value = key
#       key = namespace
#       if value?
#         if type is 1
#           storage.set prefix + key, value
#         else storage.set prefix + key, value, ace.config.cookie_expiry  if type is 2
#     else
#       if type is 1 #localStorage
#         unless value?
#           storage.remove prefix + namespace + "." + key
#         else
#           storage.set prefix + namespace + "." + key, value
#       else if type is 2 #cookie
#         val = storage.get(prefix + namespace)
#         tmp = (if val then JSON.parse(val) else {})
#         unless value?
#           delete tmp[key] #remove

#           if ace.sizeof(tmp) is 0 #no other elements in this cookie, so delete it
#             storage.remove prefix + namespace
#             return
#         else
#           tmp[key] = value
#         storage.set prefix + namespace, JSON.stringify(tmp), ace.config.cookie_expiry

#   @get = (namespace, key, undefined_) ->
#     return null  unless storage
#     if key is `undefined` #no namespace here?
#       key = namespace
#       storage.get prefix + key
#     else
#       if type is 1 #localStorage
#         storage.get prefix + namespace + "." + key
#       else if type is 2 #cookie
#         val = storage.get(prefix + namespace)
#         tmp = (if val then JSON.parse(val) else {})
#         (if key of tmp then tmp[key] else null)

#   @remove = (namespace, key, undefined_) ->
#     return  unless storage
#     if key is `undefined`
#       key = namespace
#       @set key, null
#     else
#       @set namespace, key, null


# #cookie storage
# ace.cookie =

#   # The following functions are from Cookie.js class in TinyMCE, Moxiecode, used under LGPL.

#   ###
#   Get a cookie.
#   ###
#   get: (name) ->
#     cookie = document.cookie
#     e = undefined
#     p = name + "="
#     b = undefined
#     return  unless cookie
#     b = cookie.indexOf("; " + p)
#     if b is -1
#       b = cookie.indexOf(p)
#       return null  unless b is 0
#     else
#       b += 2
#     e = cookie.indexOf(";", b)
#     e = cookie.length  if e is -1
#     decodeURIComponent cookie.substring(b + p.length, e)


#   ###
#   Set a cookie.

#   The 'expires' arg can be either a JS Date() object set to the expiration date (back-compat)
#   or the number of seconds until expiration
#   ###
#   set: (name, value, expires, path, domain, secure) ->
#     d = new Date()
#     if typeof (expires) is "object" and expires.toGMTString
#       expires = expires.toGMTString()
#     else if parseInt(expires, 10)
#       d.setTime d.getTime() + (parseInt(expires, 10) * 1000) # time must be in miliseconds
#       expires = d.toGMTString()
#     else
#       expires = ""
#     document.cookie = name + "=" + encodeURIComponent(value) + ((if (expires) then "; expires=" + expires else "")) + ((if (path) then "; path=" + path else "")) + ((if (domain) then "; domain=" + domain else "")) + ((if (secure) then "; secure" else ""))


#   ###
#   Remove a cookie.

#   This is done by setting it to an empty value and setting the expiration time in the past.
#   ###
#   remove: (name, path) ->
#     @set name, "", -1000, path


# #local storage
# ace.storage =
#   get: (key) ->
#     window["localStorage"].getItem key

#   set: (key, value) ->
#     window["localStorage"].setItem key, value

#   remove: (key) ->
#     window["localStorage"].removeItem key


# #count the number of properties in an object
# #useful for getting the number of elements in an associative array
# ace.sizeof = (obj) ->
#   size = 0
#   for key of obj
#     size++  if obj.hasOwnProperty(key)
#   size


# #because jQuery may not be loaded at this stage, we use our own toggleClass
# ace.hasClass = (elem, className) ->
#   (" " + elem.className + " ").indexOf(" " + className + " ") > -1

# ace.addClass = (elem, className) ->
#   unless ace.hasClass(elem, className)
#     currentClass = elem.className
#     elem.className = currentClass + ((if currentClass.length then " " else "")) + className

# ace.removeClass = (elem, className) ->
#   ace.replaceClass elem, className

# ace.replaceClass = (elem, className, newClass) ->
#   classToRemove = new RegExp(("(^|\\s)" + className + "(\\s|$)"), "i")
#   elem.className = elem.className.replace(classToRemove, (match, p1, p2) ->
#     (if newClass then (p1 + newClass + p2) else " ")
#   ).replace(/^\s+|\s+$/g, "")

# ace.toggleClass = (elem, className) ->
#   if ace.hasClass(elem, className)
#     ace.removeClass elem, className
#   else
#     ace.addClass elem, className


# #data_storage instance used inside ace.settings etc
# ace.data = new ace.data_storage(ace.config.storage_method)