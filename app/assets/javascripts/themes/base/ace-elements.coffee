﻿jQuery ->
  window["ace"] = {}  unless "ace" of window
  window["ace"].click_event = (if $.fn.tap then "tap" else "click")


#at some places we try to use 'tap' event instead of 'click' if jquery mobile plugin is available
(($, undefined_) ->
  multiplible = "multiple" of document.createElement("INPUT")
  hasFileList = "FileList" of window #file list enabled in modern browsers
  hasFileReader = "FileReader" of window
  Ace_File_Input = (element, settings) ->
    self = this
    @settings = $.extend({}, $.fn.ace_file_input.defaults, settings)
    @$element = $(element)
    @element = element
    @disabled = false
    @can_reset = true
    @$element.on "change.ace_inner_call", (e, ace_inner_call) ->
      return  if ace_inner_call is true #this change event is called from above drop event
      handle_on_change.call self

    @$element.wrap "<div class=\"ace-file-input\" />"
    @apply_settings()

  Ace_File_Input.error =
    FILE_LOAD_FAILED: 1
    IMAGE_LOAD_FAILED: 2
    THUMBNAIL_FAILED: 3

  Ace_File_Input::apply_settings = ->
    self = this
    remove_btn = !!@settings.icon_remove
    @multi = @$element.attr("multiple") and multiplible
    @well_style = @settings.style is "well"
    if @well_style
      @$element.parent().addClass "ace-file-multiple"
    else
      @$element.parent().removeClass "ace-file-multiple"
    @$element.parent().find(":not(input[type=file])").remove() #remove all except our input, good for when changing settings
    @$element.after "<label class=\"file-label\" data-title=\"" + @settings.btn_choose + "\"><span class=\"file-name\" data-title=\"" + @settings.no_file + "\">" + ((if @settings.no_icon then "<i class=\"" + @settings.no_icon + "\"></i>" else "")) + "</span></label>" + ((if remove_btn then "<a class=\"remove\" href=\"#\"><i class=\"" + @settings.icon_remove + "\"></i></a>" else ""))
    @$label = @$element.next()
    @$label.on "click", -> #firefox mobile doesn't allow 'tap'!
      self.$element.click()  if not @disabled and not self.element.disabled and not self.$element.attr("readonly")

    if remove_btn
      @$label.next("a").on ace.click_event, ->
        return false  unless self.can_reset
        ret = true
        ret = self.settings.before_remove.call(self.element)  if self.settings.before_remove
        return false  unless ret
        self.reset_input()

    enable_drop_functionality.call this  if @settings.droppable and hasFileList

  Ace_File_Input::show_file_list = ($files) ->
    files = (if typeof $files is "undefined" then @$element.data("ace_input_files") else $files)
    return  if not files or files.length is 0

    #////////////////////////////////////////////////////////////////
    if @well_style
      @$label.find(".file-name").remove()
      @$label.addClass "hide-placeholder"  unless @settings.btn_change
    @$label.attr("data-title", @settings.btn_change).addClass "selected"
    i = 0

    while i < files.length
      filename = (if typeof files[i] is "string" then files[i] else $.trim(files[i].name))
      index = filename.lastIndexOf("\\") + 1
      index = filename.lastIndexOf("/") + 1  if index is 0
      filename = filename.substr(index)
      fileIcon = "icon-file"
      if (/\.(jpe?g|png|gif|svg|bmp|tiff?)$/i).test(filename)
        fileIcon = "icon-picture"
      else if (/\.(mpe?g|flv|mov|avi|swf|mp4|mkv|webm|wmv|3gp)$/i).test(filename)
        fileIcon = "icon-film"
      else fileIcon = "icon-music"  if (/\.(mp3|ogg|wav|wma|amr|aac)$/i).test(filename)
      if @well_style
        @$label.append "<span class=\"file-name\" data-title=\"" + filename + "\"><i class=\"" + fileIcon + "\"></i></span>"
        type = $.trim(files[i].type)
        can_preview = hasFileReader and @settings.thumbnail and ((type.length > 0 and type.match("image")) or (type.length is 0 and fileIcon is "icon-picture")) #the second one is for Android's default browser which gives an empty text for file.type
        if can_preview
          self = this
          $.when(preview_image.call(this, files[i])).fail (result) ->

            #called on failure to load preview
            self.settings.preview_error.call self, filename, result.code  if self.settings.preview_error

      i++
    true

  Ace_File_Input::reset_input = ->
    @$label.attr(
      "data-title": @settings.btn_choose
      class: "file-label"
    ).find(".file-name:first").attr(
      "data-title": @settings.no_file
      class: "file-name"
    ).find("[class*=\"icon-\"]").attr("class", @settings.no_icon).prev("img").remove()
    @$label.find("[class*=\"icon-\"]").remove()  unless @settings.no_icon
    @$label.find(".file-name").not(":first").remove()
    if @$element.data("ace_input_files")
      @$element.removeData "ace_input_files"
      @$element.removeData "ace_input_method"
    @reset_input_field()
    false

  Ace_File_Input::reset_input_field = ->

    #http://stackoverflow.com/questions/1043957/clearing-input-type-file-using-jquery/13351234#13351234
    @$element.wrap("<form>").closest("form").get(0).reset()
    @$element.unwrap()

  Ace_File_Input::enable_reset = (can_reset) ->
    @can_reset = can_reset

  Ace_File_Input::disable = ->
    @disabled = true
    @$element.attr("disabled", "disabled").addClass "disabled"

  Ace_File_Input::enable = ->
    @disabled = false
    @$element.removeAttr("disabled").removeClass "disabled"

  Ace_File_Input::files = ->
    $(this).data("ace_input_files") or null

  Ace_File_Input::method = ->
    $(this).data("ace_input_method") or ""

  Ace_File_Input::update_settings = (new_settings) ->
    @settings = $.extend({}, @settings, new_settings)
    @apply_settings()

  enable_drop_functionality = ->
    self = this
    dropbox = @element.parentNode
    $(dropbox).on("dragenter", (e) ->
      e.preventDefault()
      e.stopPropagation()
    ).on("dragover", (e) ->
      e.preventDefault()
      e.stopPropagation()
    ).on "drop", (e) ->
      e.preventDefault()
      e.stopPropagation()
      dt = e.originalEvent.dataTransfer
      files = dt.files
      if not self.multi and files.length > 1 #single file upload, but dragged multiple files
        tmpfiles = []
        tmpfiles.push files[0]
        files = tmpfiles #keep only first file
      ret = true
      ret = self.settings.before_change.call(self.element, files, true)  if self.settings.before_change #true means files have been dropped
      return false  if not ret or ret.length is 0

      #user can return a modified File Array as result
      files = ret  if ret instanceof Array or (hasFileList and ret instanceof FileList)
      self.$element.data "ace_input_files", files #save files data to be used later by user
      self.$element.data "ace_input_method", "drop"
      self.show_file_list files
      self.$element.triggerHandler "change", [true] #true means inner_call
      true


  handle_on_change = ->
    ret = true
    ret = @settings.before_change.call(@element, @element.files or [@element.value], false)  if @settings.before_change #make it an array
#false means files have been selected, not dropped
    if not ret or ret.length is 0
      @reset_input_field()  unless @$element.data("ace_input_files") #if nothing selected before, reset because of the newly unacceptable (ret=false||length=0) selection
      return false

    #user can return a modified File Array as result
    #for old IE, etc
    files = (if not hasFileList then null else ((if (ret instanceof Array or ret instanceof FileList) then ret else @element.files)))
    @$element.data "ace_input_method", "select"
    if files and files.length > 0 #html5
      @$element.data "ace_input_files", files
    else
      name = $.trim(@element.value)
      if name and name.length > 0
        files = []
        files.push name
        @$element.data "ace_input_files", files
    return false  if not files or files.length is 0
    @show_file_list files
    true

  preview_image = (file) ->
    self = this
    $span = self.$label.find(".file-name:last") #it should be out of onload, otherwise all onloads may target the same span because of delays
    deferred = new $.Deferred
    reader = new FileReader()
    reader.onload = (e) ->
      $span.prepend "<img class='middle' style='display:none;' />"
      img = $span.find("img:last").get(0)

      #if image loaded successfully

      #if making thumbnail fails

      #/////////////////
      $(img).one("load", ->
        size = 50
        if self.settings.thumbnail is "large"
          size = 150
        else size = $span.width()  if self.settings.thumbnail is "fit"
        $span.addClass (if size > 50 then "large" else "")
        thumb = get_thumbnail(img, size, file.type)
        unless thumb?
          $(this).remove()
          deferred.reject code: Ace_File_Input.error["THUMBNAIL_FAILED"]
          return
        w = thumb.w
        h = thumb.h
        w = h = size  if self.settings.thumbnail is "small"
        $(img).css(
          "background-image": "url(" + thumb.src + ")"
          width: w
          height: h
        ).data("thumb", thumb.src).attr(src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNgYGBgAAAABQABh6FO1AAAAABJRU5ErkJggg==").show()
        deferred.resolve()
      ).one "error", ->

        #for example when a file has image extenstion, but format is something else
        $span.find("img").remove()
        deferred.reject code: Ace_File_Input.error["IMAGE_LOAD_FAILED"]

      img.src = e.target.result

    reader.onerror = (e) ->
      deferred.reject code: Ace_File_Input.error["FILE_LOAD_FAILED"]

    reader.readAsDataURL file
    deferred.promise()

  get_thumbnail = (img, size, type) ->
    w = img.width
    h = img.height
    if w > size or h > size
      if w > h
        h = parseInt(size / w * h)
        w = size
      else
        w = parseInt(size / h * w)
        h = size
    dataURL = undefined
    try
      canvas = document.createElement("canvas")
      canvas.width = w
      canvas.height = h
      context = canvas.getContext("2d")
      context.drawImage img, 0, 0, img.width, img.height, 0, 0, w, h
      dataURL = canvas.toDataURL() #type == 'image/jpeg' ? type : 'image/png', 10
    catch e
      dataURL = null

    #there was only one image that failed in firefox completely randomly! so let's double check it
    dataURL = null  unless /^data\:image\/(png|jpe?g|gif);base64,[0-9A-Za-z\+\/\=]+$/.test(dataURL)
    return null  unless dataURL
    src: dataURL
    w: w
    h: h


  #/////////////////////////////////////////
  $.fn.ace_file_input = (option, value) ->
    retval = undefined
    $set = @each(->
      $this = $(this)
      data = $this.data("ace_file_input")
      options = typeof option is "object" and option
      $this.data "ace_file_input", (data = new Ace_File_Input(this, options))  unless data
      retval = data[option](value)  if typeof option is "string"
    )
    (if (retval is `undefined`) then $set else retval)

  $.fn.ace_file_input.defaults =
    style: false
    no_file: "No File ..."
    no_icon: "icon-upload-alt"
    btn_choose: "Choose"
    btn_change: "Change"
    icon_remove: "icon-remove"
    droppable: false
    thumbnail: false #large, fit, small

    #callbacks
    before_change: null
    before_remove: null
    preview_error: null
) window.jQuery
(($, undefined_) ->
  $.fn.ace_spinner = (options) ->

    #when min is negative, the input maxlength does not account for the extra minus sign
    @each ->
      icon_up = options.icon_up or "icon-chevron-up"
      icon_down = options.icon_down or "icon-chevron-down"
      on_sides = options.on_sides or false
      btn_up_class = options.btn_up_class or ""
      btn_down_class = options.btn_down_class or ""
      max = options.max or 999
      max = ("" + max).length
      $(this).addClass("spinner-input form-control").wrap "<div class=\"ace-spinner\">"
      $parent_div = $(this).closest(".ace-spinner").spinner(options).wrapInner("<div class='input-group'></div>")
      if on_sides
        $(this).before("<div class=\"spinner-buttons input-group-btn\">\t\t\t\t\t\t\t<button type=\"button\" class=\"btn spinner-down btn-xs " + btn_down_class + "\">\t\t\t\t\t\t\t\t<i class=\"" + icon_down + "\"></i>\t\t\t\t\t\t\t</button>\t\t\t\t\t\t</div>").after "<div class=\"spinner-buttons input-group-btn\">\t\t\t\t\t\t\t<button type=\"button\" class=\"btn spinner-up btn-xs " + btn_up_class + "\">\t\t\t\t\t\t\t\t<i class=\"" + icon_up + "\"></i>\t\t\t\t\t\t\t</button>\t\t\t\t\t\t</div>"
        $parent_div.addClass "touch-spinner"
        $parent_div.css "width", (max * 20 + 40) + "px"
      else
        $(this).after "<div class=\"spinner-buttons input-group-btn\">\t\t\t\t\t\t\t<button type=\"button\" class=\"btn spinner-up btn-xs " + btn_up_class + "\">\t\t\t\t\t\t\t\t<i class=\"" + icon_up + "\"></i>\t\t\t\t\t\t\t</button>\t\t\t\t\t\t\t<button type=\"button\" class=\"btn spinner-down btn-xs " + btn_down_class + "\">\t\t\t\t\t\t\t\t<i class=\"" + icon_down + "\"></i>\t\t\t\t\t\t\t</button>\t\t\t\t\t\t</div>"
        if "ontouchend" of document or options.touch_spinner
          $parent_div.addClass "touch-spinner"
          $parent_div.css "width", (max * 20 + 40) + "px"
        else
          $(this).next().addClass "btn-group-vertical"
          $parent_div.css "width", (max * 20 + 10) + "px"
      $(this).on "mousewheel DOMMouseScroll", (event) ->
        delta = (if event.originalEvent.detail < 0 or event.originalEvent.wheelDelta > 0 then 1 else -1)
        $parent_div.spinner "step", delta > 0 #accepts true or false as second param
        $parent_div.spinner "triggerChangedEvent"
        false

      that = $(this)
      $parent_div.on "changed", ->
        that.trigger "change" #trigger the input's change event


    this
) window.jQuery
(($, undefined_) ->
  $.fn.ace_wizard = (options) ->
    @each ->
      $this = $(this)
      $this.wizard()
      buttons = $this.siblings(".wizard-actions").eq(0)
      $wizard = $this.data("wizard")
      $wizard.$prevBtn.remove()
      $wizard.$nextBtn.remove()
      $wizard.$prevBtn = buttons.find(".btn-prev").eq(0).on(ace.click_event, ->
        $this.wizard "previous"
      ).attr("disabled", "disabled")
      $wizard.$nextBtn = buttons.find(".btn-next").eq(0).on(ace.click_event, ->
        $this.wizard "next"
      ).removeAttr("disabled")
      $wizard.nextText = $wizard.$nextBtn.text()

    this
) window.jQuery
(($, undefined_) ->
  $.fn.ace_colorpicker = (options) ->
    settings = $.extend(
      pull_right: false
      caret: true
    , options)
    @each ->
      $that = $(this)
      colors = ""
      color = ""
      $(this).hide().find("option").each(->
        $class = "colorpick-btn"
        if @selected
          $class += " selected"
          color = @value
        colors += "<li><a class=\"" + $class + "\" href=\"#\" style=\"background-color:" + @value + ";\" data-color=\"" + @value + "\"></a></li>"
      ).end().on("change.ace_inner_call", ->
        $(this).next().find(".btn-colorpicker").css "background-color", @value
      ).after("<div class=\"dropdown dropdown-colorpicker\"><a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\"><span class=\"btn-colorpicker\" style=\"background-color:" + color + "\"></span></a><ul class=\"dropdown-menu" + ((if settings.caret then " dropdown-caret" else "")) + ((if settings.pull_right then " pull-right" else "")) + "\">" + colors + "</ul></div>").next().find(".dropdown-menu").on ace.click_event, (e) ->
        a = $(e.target)
        return false  unless a.is(".colorpick-btn")
        a.closest("ul").find(".selected").removeClass "selected"
        a.addClass "selected"
        color = a.data("color")
        $that.val(color).change()
        e.preventDefault()
        true #if false, dropdown won't hide!


    this
) window.jQuery
(($, undefined_) ->
  $.fn.ace_tree = (options) ->
    $options =
      "open-icon": "icon-folder-open"
      "close-icon": "icon-folder-close"
      selectable: true
      "selected-icon": "icon-ok"
      "unselected-icon": "tree-dot"

    $options = $.extend({}, $options, options)
    @each ->
      $this = $(this)
      $this.html "<div class = \"tree-folder\" style=\"display:none;\">\t\t\t\t<div class=\"tree-folder-header\">\t\t\t\t\t<i class=\"" + $options["close-icon"] + "\"></i>\t\t\t\t\t<div class=\"tree-folder-name\"></div>\t\t\t\t</div>\t\t\t\t<div class=\"tree-folder-content\"></div>\t\t\t\t<div class=\"tree-loader\" style=\"display:none\"></div>\t\t\t</div>\t\t\t<div class=\"tree-item\" style=\"display:none;\">\t\t\t\t" + ((if not $options["unselected-icon"]? then "" else "<i class=\"" + $options["unselected-icon"] + "\"></i>")) + "\t\t\t\t<div class=\"tree-item-name\"></div>\t\t\t</div>"
      $this.addClass (if $options["selectable"] is true then "tree-selectable" else "tree-unselectable")
      $this.tree $options

    this
) window.jQuery
(($, undefined_) ->
  $.fn.ace_wysiwyg = ($options, undefined_) ->
    options = $.extend(
      speech_button: true
      wysiwyg: {}
    , $options)
    color_values = ["#ac725e", "#d06b64", "#f83a22", "#fa573c", "#ff7537", "#ffad46", "#42d692", "#16a765", "#7bd148", "#b3dc6c", "#fbe983", "#fad165", "#92e1c0", "#9fe1e7", "#9fc6e7", "#4986e7", "#9a9cff", "#b99aff", "#c2c2c2", "#cabdbf", "#cca6ac", "#f691b2", "#cd74e6", "#a47ae2", "#444444"]
    button_defaults =
      font:
        values: ["Arial", "Courier", "Comic Sans MS", "Helvetica", "Open Sans", "Tahoma", "Verdana"]
        icon: "icon-font"
        title: "Font"

      fontSize:
        values:
          5: "Huge"
          3: "Normal"
          1: "Small"

        icon: "icon-text-height"
        title: "Font Size"

      bold:
        icon: "icon-bold"
        title: "Bold (Ctrl/Cmd+B)"

      italic:
        icon: "icon-italic"
        title: "Italic (Ctrl/Cmd+I)"

      strikethrough:
        icon: "icon-strikethrough"
        title: "Strikethrough"

      underline:
        icon: "icon-underline"
        title: "Underline"

      insertunorderedlist:
        icon: "icon-list-ul"
        title: "Bullet list"

      insertorderedlist:
        icon: "icon-list-ol"
        title: "Number list"

      outdent:
        icon: "icon-indent-left"
        title: "Reduce indent (Shift+Tab)"

      indent:
        icon: "icon-indent-right"
        title: "Indent (Tab)"

      justifyleft:
        icon: "icon-align-left"
        title: "Align Left (Ctrl/Cmd+L)"

      justifycenter:
        icon: "icon-align-center"
        title: "Center (Ctrl/Cmd+E)"

      justifyright:
        icon: "icon-align-right"
        title: "Align Right (Ctrl/Cmd+R)"

      justifyfull:
        icon: "icon-align-justify"
        title: "Justify (Ctrl/Cmd+J)"

      createLink:
        icon: "icon-link"
        title: "Hyperlink"
        button_text: "Add"
        placeholder: "URL"
        button_class: "btn-primary"

      unlink:
        icon: "icon-unlink"
        title: "Remove Hyperlink"

      insertImage:
        icon: "icon-picture"
        title: "Insert picture"
        button_text: "<i class=\"icon-file\"></i> Choose Image &hellip;"
        placeholder: "Image URL"
        button_insert: "Insert"
        button_class: "btn-success"
        button_insert_class: "btn-primary"
        choose_file: true #show the choose file button?

      foreColor:
        values: color_values
        title: "Change Color"

      backColor:
        values: color_values
        title: "Change Background Color"

      undo:
        icon: "icon-undo"
        title: "Undo (Ctrl/Cmd+Z)"

      redo:
        icon: "icon-repeat"
        title: "Redo (Ctrl/Cmd+Y)"

      viewSource:
        icon: "icon-code"
        title: "View Source"

    toolbar_buttons = options.toolbar or ["font", null, "fontSize", null, "bold", "italic", "strikethrough", "underline", null, "insertunorderedlist", "insertorderedlist", "outdent", "indent", null, "justifyleft", "justifycenter", "justifyright", "justifyfull", null, "createLink", "unlink", null, "insertImage", null, "foreColor", null, "undo", "redo", null, "viewSource"]
    @each ->
      toolbar = " <div class=\"wysiwyg-toolbar btn-toolbar center\"> <div class=\"btn-group\"> "
      for tb of toolbar_buttons
        if toolbar_buttons.hasOwnProperty(tb)
          button = toolbar_buttons[tb]
          if button is null
            toolbar += " </div> <div class=\"btn-group\"> "
            continue
          if typeof button is "string" and button of button_defaults
            button = button_defaults[button]
            button.name = toolbar_buttons[tb]
          else if typeof button is "object" and button.name of button_defaults
            button = $.extend(button_defaults[button.name], button)
          else
            continue
          className = (if "className" of button then button.className else "")
          switch button.name
            when "font"
              toolbar += " <a class=\"btn btn-sm " + className + " dropdown-toggle\" data-toggle=\"dropdown\" title=\"" + button.title + "\"><i class=\"" + button.icon + "\"></i><i class=\"icon-angle-down icon-on-right\"></i></a> "
              toolbar += " <ul class=\"dropdown-menu dropdown-light\">"
              for font of button.values
                toolbar += " <li><a data-edit=\"fontName " + button.values[font] + "\" style=\"font-family:'" + button.values[font] + "'\">" + button.values[font] + "</a></li> "  if button.values.hasOwnProperty(font)
              toolbar += " </ul>"
            when "fontSize"
              toolbar += " <a class=\"btn btn-sm " + className + " dropdown-toggle\" data-toggle=\"dropdown\" title=\"" + button.title + "\"><i class=\"" + button.icon + "\"></i>&nbsp;<i class=\"icon-angle-down icon-on-right\"></i></a> "
              toolbar += " <ul class=\"dropdown-menu dropdown-light\"> "
              for size of button.values
                toolbar += " <li><a data-edit=\"fontSize " + size + "\"><font size=\"" + size + "\">" + button.values[size] + "</font></a></li> "  if button.values.hasOwnProperty(size)
              toolbar += " </ul> "
            when "createLink"
              toolbar += " <div class=\"inline position-relative\"> <a class=\"btn btn-sm " + className + " dropdown-toggle\" data-toggle=\"dropdown\" title=\"" + button.title + "\"><i class=\"" + button.icon + "\"></i></a> "
              toolbar += " <div class=\"dropdown-menu dropdown-caret pull-right\">\t\t\t\t\t\t\t<div class=\"input-group\">\t\t\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"" + button.placeholder + "\" type=\"text\" data-edit=\"" + button.name + "\" />\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">\t\t\t\t\t\t\t\t\t<button class=\"btn btn-sm " + button.button_class + "\" type=\"button\">" + button.button_text + "</button>\t\t\t\t\t\t\t\t</span>\t\t\t\t\t\t\t</div>\t\t\t\t\t\t</div> </div>"
            when "insertImage"
              toolbar += " <div class=\"inline position-relative\"> <a class=\"btn btn-sm " + className + " dropdown-toggle\" data-toggle=\"dropdown\" title=\"" + button.title + "\"><i class=\"" + button.icon + "\"></i></a> "
              toolbar += " <div class=\"dropdown-menu dropdown-caret pull-right\">\t\t\t\t\t\t\t<div class=\"input-group\">\t\t\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"" + button.placeholder + "\" type=\"text\" data-edit=\"" + button.name + "\" />\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">\t\t\t\t\t\t\t\t\t<button class=\"btn btn-sm " + button.button_insert_class + "\" type=\"button\">" + button.button_insert + "</button>\t\t\t\t\t\t\t\t</span>\t\t\t\t\t\t\t</div>"
              toolbar += "<div class=\"space-2\"></div>\t\t\t\t\t\t\t <div class=\"center\">\t\t\t\t\t\t\t\t<button class=\"btn btn-sm " + button.button_class + " wysiwyg-choose-file\" type=\"button\">" + button.button_text + "</button>\t\t\t\t\t\t\t\t<input type=\"file\" data-edit=\"" + button.name + "\" />\t\t\t\t\t\t\t  </div>"  if button.choose_file and "FileReader" of window
              toolbar += " </div> </div>"
            when "foreColor", "backColor"
              toolbar += " <select class=\"hide wysiwyg_colorpicker\" title=\"" + button.title + "\"> "
              for color of button.values
                toolbar += " <option value=\"" + button.values[color] + "\">" + button.values[color] + "</option> "
              toolbar += " </select> "
              toolbar += " <input style=\"display:none;\" disabled class=\"hide\" type=\"text\" data-edit=\"" + button.name + "\" /> "
            when "viewSource"
              toolbar += " <a class=\"btn btn-sm " + className + "\" data-view=\"source\" title=\"" + button.title + "\"><i class=\"" + button.icon + "\"></i></a> "
            else
              toolbar += " <a class=\"btn btn-sm " + className + "\" data-edit=\"" + button.name + "\" title=\"" + button.title + "\"><i class=\"" + button.icon + "\"></i></a> "
      toolbar += " </div> </div> "

      #if we have a function to decide where to put the toolbar, then call that
      if options.toolbar_place
        toolbar = options.toolbar_place.call(this, toolbar)

      #otherwise put it just before our DIV
      else
        toolbar = $(this).before(toolbar).prev()
      toolbar.find("a[title]").tooltip
        animation: false
        container: "body"

      toolbar.find(".dropdown-menu input:not([type=file])").on(ace.click_event, ->
        false
      ).on("change", ->
        $(this).closest(".dropdown-menu").siblings(".dropdown-toggle").dropdown "toggle"
      ).on "keydown", (e) ->
        if e.which is 27
          @value = ""
          $(this).change()

      toolbar.find("input[type=file]").prev().on ace.click_event, (e) ->
        $(this).next().click()

      toolbar.find(".wysiwyg_colorpicker").each ->
        $(this).ace_colorpicker(pull_right: true).change(->
          $(this).nextAll("input").eq(0).val(@value).change()
        ).next().find(".btn-colorpicker").tooltip
          title: @title
          animation: false
          container: "body"


      speech_input = undefined
      if options.speech_button and "onwebkitspeechchange" of (speech_input = document.createElement("input"))
        editorOffset = $(this).offset()
        toolbar.append speech_input
        $(speech_input).attr(
          type: "text"
          "data-edit": "inserttext"
          "x-webkit-speech": ""
        ).addClass("wysiwyg-speech-input").css(position: "absolute").offset
          top: editorOffset.top
          left: editorOffset.left + $(this).innerWidth() - 35

      else
        speech_input = null

      #view source
      self = $(this)
      view_source = false
      toolbar.find("a[data-view=source]").on "click", (e) ->
        e.preventDefault()
        unless view_source
          $("<textarea />").css(
            width: self.outerWidth()
            height: self.outerHeight()
          ).val(self.html()).insertAfter self
          self.hide()
          $(this).addClass "active"
        else
          textarea = self.next()
          self.html(textarea.val()).show()
          textarea.remove()
          $(this).removeClass "active"
        view_source = not view_source

      $options = $.extend({},
        activeToolbarClass: "active"
        toolbarSelector: toolbar
      , options.wysiwyg or {})
      $(this).wysiwyg $options

    this
) window.jQuery