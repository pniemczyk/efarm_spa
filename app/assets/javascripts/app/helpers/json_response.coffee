class window.App.Helpers.JsonResponse
  data: {}
  valid:   false
  success: false
  fail:    true
  constructor: (data) ->
    if data.json_response?
      @[prop] = data.json_response[prop] for prop of data.json_response
      @valid   = true
      @success = @status is 'OK'
      @fail    = !@success
      @type    = (@status || 'undefined').toLowerCase()
    else
      @data = data