﻿class window.App.Helpers.Typeahead
  constructor: (@$el, @url, options) ->
    @list = []
    @objects = {}
    @selectedItem = null
    if options?
      @source       = options.source       if options.source?
      @matcher      = options.matcher      if typeof options.matcher      is 'function'
      @decorator    = options.decorator    if typeof options.decorator    is 'function'
      @selectItem   = options.selectItem   if typeof options.selectItem   is 'function'


  init: -> @$el.typeahead(that: @, source: @source, updater: @updater)
  matcher: (item) ->
    return unless _.isString(item)
    true if item.toLowerCase().indexOf(@query.trim().toLowerCase()) isnt -1

  select: (item) ->
    @selectedItem = item
    @selectItem(item)

  selectItem: (item) ->
  decorator:  (obj) -> obj

  source: (query, process) ->
    return unless $.trim(query).length > 2
    self = @options.that
    opts =
      data:
        q:$.trim(query)
      method: 'GET'
      url: self.url

    window.App.transaction.request(opts).success (data) ->
      results = data.json_response.data || []
      self.list = _.map(results, (i) -> self.decorator(i))
      self.objects = results
      process(self.list)
    self.list

  updater: (item) ->
    self = @options.that
    self.select(_.find(self.objects, (i) -> self.decorator(i) is item))
    item