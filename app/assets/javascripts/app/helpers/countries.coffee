class window.App.Helpers.Countries
  name: 'Countries'
  constructor: (opts={}) ->
    @_initializationError('reqres is not configured') unless opts.reqres
    @_reqres = opts.reqres
    @_registerEvents()

  _registerEvents: ->
    @_reqres.setHandler("#{@name}-getAll", @getAll, @)
    @_reqres.setHandler("{@name}-getName", @getName, @)

  getName: (code) -> all[code.toUpperCase()]
  getAll: -> @all
  all:
    CZ: 'Czechy'
    DE: 'Niemcy'
    LT: 'Litwa'
    PL: 'Polska'
    RU: 'Federacja Rosyjska'
    BY: 'Białoruś'
    UA: 'Ukraina'
    SK: 'Słowacja'