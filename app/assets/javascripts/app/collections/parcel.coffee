class window.App.Collections.Parcel extends window.App.Collections.Base
  initialize: (models=[], opts={}) ->
    @field_card = opts.field_card

  lastCalculations:
    sumArea: null
    sumAreaUsed: null

  sumArea: ->
    result = _.reduce(@models, (memo, obj) ->
      memo + obj.get('area_in_ha')
    , 0)
    @lastCalculations.sumArea = Math.round(result * 1000)/1000

  sumAreaUsed: ->
    result = _.reduce(@models, (memo, obj) ->
      memo + obj.get('area_used')
    , 0)
    @lastCalculations.sumAreaUsed = Math.round(result * 1000)/1000

