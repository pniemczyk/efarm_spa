class window.App.Collections.FieldCard extends window.App.Collections.Base
  getById:   (id) -> _.find(@models, (m) -> m.get('id') == ~~id)
  getByOlId: (id) -> _.find(@models, (m) -> if m.getFeature() then m.getFeature().olId() == id else false)

  getNextNewFieldId: ->
    min = _.min(_.map(@models, (m) -> (m.get('id'))))
    if min > 0 then -1 else (min - 1)