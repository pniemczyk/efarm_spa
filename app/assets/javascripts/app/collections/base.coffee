class window.App.Collections.Base extends Backbone.Collection
  initialize: (models=[], opts={}) ->
    @url = opts.url
    @pagingUrl = opts.pagingUrl
    @_init()

  _init: ->

  page: 1
  size: 10
  total: 0