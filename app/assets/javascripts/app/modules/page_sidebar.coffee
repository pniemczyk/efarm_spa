﻿class window.App.Modules.PageSidebar extends window.App.Modules.BaseViewModule
  @name: 'PageSidebar'
  @require: ->
    name: 'PageSidebar'
    libs: ['commands', 'reqres']
    config: 'PageSidebar'

  _initialize: ->
    data = @_config.data || {}
    data.mini = @_config.minimized
    @_model = new _Model(data)
    @_template = @_getTemplate(@_templateName)
    @_view = new _View(model: @_model, template: @_template, module: @)

  remove: -> @_view.remove()

  _registerEvents: ->
    @_commands.setHandler("#{@name}-update", @_view.update, @_view)
    @_commands.setHandler("#{@name}-addShortcut", @_model.addShortcut, @_model)
    @_commands.setHandler("#{@name}-addItem", @_model.addItem, @_model)

  _templateName: 'page/sidebar'

  class _Model extends Backbone.Model
    defaults:
      mini: true
      shortcuts: []
      items:[]

    addShortcut: (shortcut, silent=false) ->
      all = @get('shortcuts')
      return if all.lenght == 4
      all.push shortcut
      @set('shortcuts', all)
      @trigger('change') unless silent

    addItem: (item, silent=false) ->
      all = @get('items')
      all.push item
      @set('items', all)
      @trigger('change') unless silent

  class _View extends Backbone.View
    initialize: (opts={}) ->
      @_template = opts.template
      @_module   = opts.module
      @model.on("change", @render, @)
      @render()

    el: '#sidebar'

    attributes:
      class: 'sidebar fixed'

    update: -> @render()

    _isMinimized: -> @$el.hasClass("menu-min")

    events:
      "click #sidebar-collapse, tap #sidebar-collapse": "collapseEvent"
      "click .nav-list, tap .nav-list": "openingSubmenuEvent"

    openingSubmenuEvent: (e)=>
      link_element = $(e.target).closest("a")
      return  if not link_element or link_element.length is 0

      unless link_element.hasClass("dropdown-toggle")
        if @_isMinimized() and @eventClickName is "tap" and e.target is e.currentTarget
          text = link_element.find(".menu-text").get(0)
          return false  if e.target isnt text and not $.contains(text, e.target)
        return
      sub = link_element.next().get(0)

      unless $(sub).is(":visible")
        parent_ul = $(sub.parentNode).closest("ul")
        return  if @_isMinimized() and parent_ul.hasClass("nav-list")
        parent_ul.find("> .open > .submenu").each ->
          $(this).slideUp(200).parent().removeClass "open"  if this isnt sub and not $(@parentNode).hasClass("active")
      else
      return false  if @_isMinimized() and $(sub.parentNode.parentNode).hasClass("nav-list")
      $(sub).slideToggle(200).parent().toggleClass "open"
      false

    collapseEvent: (e)=>
      @$el.toggleClass "menu-min"
      @_module._commands.execute('Settings-set', 'ui_sidebar_minimized', @_isMinimized())
      $(e.currentTarget).find("[class*=\"icon-\"]:eq(0)").toggleClass "icon-angle-double-right"
      @$el.find(".open > .submenu").removeClass "open"  if @_isMinimized()

    _initScripts: ->
      @$el.find('[data-rel="tooltip"]').tooltip(animation: true)
      @_togglerMenuEvent()

    _togglerMenuEvent: ->
      togglerMenu = $("#menu-toggler")
      togglerMenu.on @eventClickName, =>
        @$el.toggleClass "display"
        togglerMenu.toggleClass "display"

    render: ->
      @$el.empty()
      @$el.html(@_template(@model.toJSON()))
      if @model.get('mini') then @$el.addClass('menu-min') else @$el.removeClass('menu-min')
      @_initScripts()
      @