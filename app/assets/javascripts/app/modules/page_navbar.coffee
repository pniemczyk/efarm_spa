class window.App.Modules.PageNavbar extends window.App.Modules.BaseViewModule
  @name: 'PageNavbar'
  @require: ->
    name: 'PageNavbar'
    libs: ['commands', 'reqres']

  routes:
    'farms/change/:id': 'changeFarm'
    'seasons/change/:id': 'changeSeason'

  changeFarm: (id) =>
    @_commands.execute('Settings-set', 'current_farm_id', parseInt(id))
    @_commands.execute('farms-changed')

  changeSeason: (id) =>
    @_commands.execute('Settings-set', 'current_season_id', parseInt(id))
    @_commands.execute('seasons-changed')

  elNaviId: 'topNavbarNavi'
  elInfoId: 'topNavbarInfo'

  seasonConfig:
    rootElId: 'topNavbarNavi'
    elId: 'naviSeason'
    css: "light-green"
    icon: "icon-calendar"
    url: (id)-> "#season/#{id}"
    changeUrl: (id)-> "#seasons/change/#{id}"

  farmConfig:
    rootElId: 'topNavbarNavi'
    elId: 'naviFarm'
    css: "light-green"
    icon: "icon-bullseye"
    url: (id)-> "#farms/#{id}"
    changeUrl: (id)-> "#farms/change/#{id}"

  _registerEvents: ->
    @_commands.setHandler("seasons-changed", @_seasonUpdate, @)
    @_commands.setHandler("farms-changed", @_farmUpdate, @)

  _templateItem: 'page/navbar/item'
  _templateInfoItem: 'page/navbar/info_item'

  _initialize: ->
    itemsLimit=100 #TODO: make actions without pagination
    farmsUrl = "#{@_getUrl('FarmsUrl')}?size=#{itemsLimit}&page=1"
    seasonsUrl = "#{@_getUrl('SeasonsUrl')}?size=#{itemsLimit}&page=1"
    fieldsUrl = @_getUrl('FieldsUrl')
    @_farmCollection = @_newFarmCollection(farmsUrl)
    @_seasonsCollection = @_newSeasonCollection(seasonsUrl)
    @_itemTemplate = @_getTemplate(@_templateItem)
    @_itemInfoTemplate = @_getTemplate(@_templateInfoItem)
    @_farmUpdate()
    @_seasonUpdate()

  _seasonUpdate: (opts={}) ->
    @_seasonsCollection.fetch({async:false})
    @_updateSeasonsItem(@_seasonsCollection)

  _newFarmCollection:   (url) -> new (@_getCollection('Farm'))([], url: url)
  _newSeasonCollection: (url) -> new (@_getCollection('Season'))([], url: url)

  _farmUpdate: ->
    @_farmCollection.fetch({async:false})
    @_updateFarmsItem(@_farmCollection)

  _updateItem: (title, config={}, collection) ->
    json =
      css: config.css
      title: "#{title}: #{collection.current.shortname || collection.current.name}"
      icon: config.icon
      elId: config.elId
    if collection.items.length is 1
      json.url = config.url(collection.current.id)
    else
      json.url = null
      json.items = []
      (
        unless item.id is collection.current.id
          json.items.push
            title: item.shortname || item.name
            url: config.changeUrl(item.id)
      ) for item in collection.items
    $root = $("##{config.rootElId}")
    $el = $root.find("##{config.elId}")
    if $el.length
      $el.replaceWith(@_itemTemplate(json))
    else
      $root.append @_itemTemplate(json)

  _updateFarmsItem: (collection) ->
    currentFarmId = @_reqres.request('Settings-get', 'current_farm_id')
    items = collection.toJSON()
    current = _.find(items, (item) -> item.id is currentFarmId )

    unless current?
      current = items[0]
      @_commands.execute('Settings-set', 'current_farm_id', current.id)
    @_updateItem('Gospodarstwo', @farmConfig, {current: current, items: items})

  _updateSeasonsItem: (collection) ->
    currentSeasonId = @_reqres.request('Settings-get', 'current_season_id')
    items = collection.toJSON()
    current = _.find(items, (item) -> item.id is currentSeasonId )
    unless current?
      current = items[0]
      @_commands.execute('Settings-set', 'current_season_id', current.id)

    @_updateItem('Sezon', @seasonConfig, {current: current, items: items})