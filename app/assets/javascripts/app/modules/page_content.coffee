﻿class window.App.Modules.PageContent extends window.App.Modules.BaseViewModule
  @name: 'PageContent'
  @require: ->
    name: 'PageContent'
    libs: ['commands', 'reqres']

  _initialize: ->
    @_template = @_getTemplate(@_templateName)
    @_view = new _View(template: @_template)

  _registerEvents: -> @_commands.setHandler("#{@name}-setView", @_view.set, @_view)

  _templateName: 'page/content'
  class _View extends Backbone.View
    initialize: (opts={}) -> @_template = opts.template

    el: '.page-content'
    _currentView: null
    _renderTitle: (title, subtitle=null, components=null) -> @_template(title: title, subtitle: subtitle, components: components)

    set: (view) ->
      @$el.removeClass(@_currentView.pageContentCssClass) if @_currentView && @_currentView.pageContentCssClass
      @_currentView.remove() if @_currentView
      delete @_currentView
      @_currentView = view
      @render()

    render: ->
      return @ unless @_currentView
      @$el.empty()
      @$el.append(@_renderTitle(@_currentView.title, @_currentView.subtitle, @_currentView.components)) if @_currentView.title
      @$el.append(@_currentView.render().el)
      @$el.addClass(@_currentView.pageContentCssClass) if @_currentView.pageContentCssClass
      @_currentView.afterRender() if typeof @_currentView.afterRender is 'function'
      @