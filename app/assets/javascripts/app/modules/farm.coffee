﻿class window.App.Modules.Farm extends window.App.Modules.BaseViewModule
  @name: 'Farm'
  @require: ->
    name: 'Farm'
    libs: ['commands', 'reqres', 'transaction']

  routes:
    'farms':'showFarmList'
    'farms/p/:page':'showFarmListPage'
    'farms/:id':'showFarm'
    'farms/create':'createFarm'

  _initialize: ->
    @_url = @_getUrl('FarmsUrl')
    @_seasonsUrl = @_getUrl('SeasonsUrl')

  sidebar:
    icon: "icon-bullseye"
    title: "Gospodarstwa"
    subitems: [
        { url: "#farms", title: "Lista" }
        { url: "#farms/create", title: "Dodaj nowe" }
      ]

  showFarmList: => @_show({action: 'farms', page:1})
  showFarmListPage: (page) => @_show({action: 'farms', page:page})
  showFarm: (id)=> @_show({action: 'details', id:id})
  createFarm: => @_show({action: 'create'})

  _show: (opts={})->
    @_showLoader()
    @_build(opts)
    @_showView(@_view)
    @_breadcrumb(opts)

  _showEditFarmButton: -> @_showBreadcrumbButtons([{icon:'edit', label:'Edytuj', cb: -> $('#btnFarmEdit').click() }])

  _breadcrumb: (opts={})->
    switch opts.action
      when "farms"
        @_showBreadcrumb([ { title: "Gospodarstwa"} ])
        @_showBreadcrumbButtons([{icon:'plus', label:'Dodaj gospodarstwo', cb: -> window.location.hash = "#farms/create" } ])
      when "create"
        @_showBreadcrumb([ { url: '#farms' , title: "Gospodarstwa"},  {title: "Nowe"}])
        @_showBreadcrumbButtons([
          {type:'default', icon:'undo', label:'Wyczyść', cb: -> $('#btnResetCreateFarm').click() }
          {type:'success', icon:'check', label:'Zapisz', cb: -> $('#btnSaveCreateFarm').click() }
        ])
      when 'details'
        @_showBreadcrumb([ { url: '#farms' , title: "Gospodarstwa"},  {title: "Szczegóły"}])
        @_showEditFarmButton()
      else []

  _build: (opts={})->
    switch opts.action
      when "farms" then @_buildFarmsAction(opts.page)
      when "create" then @_buildCreateAction()
      when 'details' then @_buildDetailsAction(opts.id)
      else null

  _buildFarmsAction: (page)->
    @_clearView()
    url = "#{@_url}?page=#{page}"

    @_collection = @_newCollection(url)
    @_collection.fetch()

    @_farmsTemplate = @_getTemplate(@_templateIndexName) unless @_farmsTemplate
    @_view = new _FarmsView(collection: @_collection, template: @_farmsTemplate, module: @)

  _buildDetailsAction: (id) ->
    @_clearView()
    url = "#{@_url}/#{id}"

    countries = @countries()
    @seasonsCollection().fetch()

    model = @_newModel(url)
    model.fetch()
    @_farmDetailsTemplate = @_getTemplate(@_templateDetailsName) unless @_farmDetailsTemplate
    @_view = new _FarmDetailsView(model: model, template: @_farmDetailsTemplate, seasonsCollection: @_seasonsCollection, countries: countries, module: @)

  _buildCreateAction: ->
    @_clearView()

    countries = @countries()
    @seasonsCollection().fetch()

    model =  @_newModel(@_url)
    @_farmTemplate = @_getTemplate(@_templateCreateName) unless @_farmTemplate
    @_view = new _FarmView(model: model, template: @_farmTemplate, seasonsCollection: @_seasonsCollection, countries: countries, module: @)

  countries: -> @_countries ||  @_countries = @_reqres.request('Countries-getAll')
  seasonsCollection: -> @_seasonsCollection || @_seasonsCollection = @_newSeasonCollection(@_seasonsUrl)

  _clearView: ->
    return unless @_view
    @_view.remove()
    delete @_view

  _newModel:      (url) -> new (@_getModel('Farm'))({}, url: url)
  _newCollection: (url) -> new (@_getCollection('Farm'))([], url: url, pagingUrl: '#farms/p/')
  _newSeasonCollection: (url) -> new (@_getCollection('Season'))([], url: url)

  _templateIndexName:   'farm/index'
  _templateCreateName:  'farm/form'
  _templateDetailsName: 'farm/details'

  class _FarmForm extends Backbone.View
    tagName: 'div'
    initialize: (opts={}) ->
      @_countries = opts.countries
      @_module = opts.module
      @_template = opts.template
      @_seasonsCollection = opts.seasonsCollection
      @_baseUrl = @model.url
      @model.on('change', @modelChange, @)

    _validateConfig:
      rules:
        name:
          required: true
        shortname:
          required: true

      messages:
        name:
          required: 'wypełnienie jest wymagane'
        shortname:
          required: 'wypełnienie jest wymagane'

    remove: ->
      $("##{@formId}").data('validator', null)
      $("##{@formId}").unbind('validate')
      super

    _getFirstYearOfSeasonById: (id) ->
      _.find(@_seasonsCollection.models, (i) -> parseInt(i.attrId()) is parseInt(id)).attrFirstYear()

    render: ->
      json = @model.toJSON()
      json.countries = @_countriesForTemplate()
      json.seasons = @_seasonsForTemplate()

      @$el.html @_template(json)
      @

    setValidateForm: -> @_module.validateFormInit(@formId, @_validateConfig)
    afterRender: ->  @setValidateForm()

    _countriesForTemplate: ->
      list = []
      (list.push {name: @_countries[country_code], code: country_code, selected: country_code is @model.get('country_code')}) for country_code of @_countries
      list

    _seasonsForTemplate: ->
      currentYear = new Date().getFullYear()
      _.map(
        @_seasonsCollection.toJSON(),
        (season) -> {name: season.name, id: season.id, selected: season.first_year is currentYear}
      )

  class _FarmDetailsView extends _FarmForm
    title: 'Szczegóły gospodarstwa'
    formId: 'farmForm'

    events:
      'click #btnFarmEdit'   : 'editFarm'
      'click #btnFarmCancel' : 'cancelEditFarm'
      'click #btnFarmSave'   : 'saveFarm'

    editFarm: (e) ->
      e.preventDefault()
      @editMode()

    editMode: ->
      $('.js-show-mode').addClass('hidden')
      $('.js-edit-mode').removeClass('hidden')
      @_module._showBreadcrumbButtons([
        {type:'error', icon:'times', label:'Anuluj', cb: -> $('#btnFarmCancel').click() }
        {type:'success', icon:'check', label:'Zapisz', cb: -> $('#btnFarmSave').click() }
      ])

    cancelEditFarm: (e) ->
      e.preventDefault()
      @hideEditable()

    hideEditable: ->
      $('.js-edit-mode').addClass('hidden')
      $('.js-show-mode').removeClass('hidden')
      @_module._showEditFarmButton()

    saveFarm:(e) ->
      e.preventDefault()
      if $("##{@formId}").valid()
        data = @_module._formFields(@formId)
        seasonId = data.season
        data.country = @_countries[data.country_code]
        delete data['season']
        seasonFirstYear = @_getFirstYearOfSeasonById(seasonId)
        @model.set(data, {silent: true})
        @model.url = "#{@_baseUrl}?season=#{seasonFirstYear}"
        @model.save([], method:'PUT').done((data)=>
          #TODO
          @hideEditable()
          @_module._commands.execute('farms-changed')
          @render()
        )

  class _FarmView extends _FarmForm
    title: 'Nowe gospodarstwo'
    formId: 'farmForm'

    events:
      'click #btnSaveCreateFarm' : 'saveFarm'
      'click #btnResetCreateFarm': 'resetForm'

    resetForm: (e)->
      $("##{@formId}").validate().resetForm()
      $("##{@formId}")[0].reset()

    saveFarm: (e) ->
      e.preventDefault()
      if $("##{@formId}").valid()
        data = @_module._formFields(@formId)
        seasonId = data.season
        data.country = @_countries[data.country_code]
        delete data['season']
        seasonFirstYear = @_getFirstYearOfSeasonById(seasonId)
        @model.set(data, {silent: true})
        @model.url = "#{@_baseUrl}?season=#{seasonFirstYear}"
        @model.save([], method:'POST').done((data) =>
          #TODO
          window.location.hash = '#farms'
          @_module._commands.execute('farms-changed')
        )

  class _FarmsView extends Backbone.View
    title: 'Gospodarstwa'
    # components: '<a href="#farms/create" class="btn btn-primary"><i class="icon-plus bigger-100"></i>Dodaj gospodarstwo</a>'
    tagName: 'div'
    events:
      'click #farms tbody tr':'showFarm'

    showFarm: (e) ->
      target = $(e.target)
      target = $(e.target).closest('tr') unless target.is('tr')
      id = target.data('id')
      window.location.hash = "#farms/#{id}"

    initialize: (opts={}) ->
      @_module = opts.module
      @_template = opts.template
      @collection.on('change', @render, @)

    render: ->
      data =
        items: @collection.toJSON()
        pagination: @_module._getPagingHtml(@collection)
      @$el.html @_template(data)
      @