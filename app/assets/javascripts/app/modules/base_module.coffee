﻿class window.App.Modules.BaseModule
  @name: 'BaseModule'
  @require: ->
    name: @name
    libs: []
    config: null

  constructor: (opts={}, require={}) ->
    throw new Error('require must have module name') unless require.name
    @name = require.name
    if require.config
      @_error("config is not present") unless opts.config
      @_config = opts.config
    (
      @_error("#{lib} is not present") unless opts[lib]
      @["_#{lib}"] = opts[lib]
    ) for lib in require.libs if require.libs
    @_initialize() if @_initialize
    @_registerEvents() if @_registerEvents

  _getUrl: (name) ->
    @_error("reqres is not configured", "#_getUrl") unless @_reqres
    @_reqres.request('Url-get', name)

  _getModel: (name) ->
    @_error("reqres is not configured", "#_getModel") unless @_reqres
    @_reqres.request('Model-get', name)

  _getCollection: (name, opts={}) ->
    @_error("reqres is not configured", "#_getCollection") unless @_reqres
    @_reqres.request('Collection-get', name, opts)

  _error: (message, type='initialization') -> throw new Error("#{@name} #{type} ERROR: #{message}")

  _message: (title, msg, type='info', time=2000) ->
    $.gritter.add({ title: title, text: msg, class_name: "gritter-#{type} gritter-center", time: time})

  _validateFormDefaultConfig:
    errorElement: 'div'
    errorClass: 'help-inline'
    focusInvalid: false
    rules: {}
    messages: {}
    invalidHandler: (e ,validator) ->
    highlight: (e) -> $(e).closest('.form-group').removeClass('has-info').addClass('has-error')
    success: (e) ->
      $(e).closest('.form-group').removeClass('has-error').addClass('has-info')
      $(e).remove()
    errorPlacement: (error, element) ->
      if element.is(":checkbox") or element.is(":radio")
        controls = element.closest("div[class*=\"col-\"]")
        if controls.find(":checkbox,:radio").length > 1
          controls.append error
        else
          error.insertAfter element.nextAll(".lbl:eq(0)").eq(0)
      else if element.is(".select2")
        error.insertAfter element.siblings("[class*=\"select2-container\"]:eq(0)")
      else if element.is(".chosen-select")
        error.insertAfter element.siblings("[class*=\"chosen-container\"]:eq(0)")
      else
        error.insertAfter element.parent()
    submitHandler: (form) ->
    invalidHandler: (form) ->

  validateFormInit: (formId, config={})->
    configuration = $.extend(@_validateFormDefaultConfig, config)
    $("##{formId}").validate(configuration)