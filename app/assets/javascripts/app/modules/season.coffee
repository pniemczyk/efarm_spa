﻿class window.App.Modules.Season extends window.App.Modules.BaseViewModule
  @name: 'Season'
  @require: ->
    name: 'Season'
    libs: ['commands', 'reqres']

  routes:
    'season':'showSeason'
    'season/set/:id':'setSeason'

  _initialize: ->
    @_url = @_getUrl('SeasonUrl')

  sidebar:
    icon: "icon-calendar "
    title: "Sezony"
    url: "#season"

  showSeason: =>
    @_showLoader()
    unless @_availableSeasons
      @_availableSeasons = new _SeasonCollection([], url: @_url)
      @_availableSeasons.fetch()
    @_model = new _Model({}, url: @_url)
    @_model.fetch()
    @_template = @_getTemplate(@_templateName) unless @_template
    @_view = new _View(model: @_model, collection: @_availableSeasons, template: @_template, module: @)
    @_showView(@_view)
    @_showBreadcrumb(@_breadcrumb)

  _templateName: 'season/index'
  _breadcrumb: [ { title: "Sezony"} ]

  setSeason: =>

  class _SeasonModel extends Backbone.Model
    defaults:
      FirstYear: 0
      Name: ''

  class _SeasonCollection extends Backbone.Collection
    model: _SeasonModel
    initialize: (models=[], opts={}) -> @url = opts.url
    parse: (response) -> response.Data

  class _Model extends Backbone.Model
    initialize: (model={}, opts={}) -> @url = opts.url
    defaults:
      SelectedSeasons: []
      ActiveSeasons: []
      CurrentSeason: ''

  class _View extends Backbone.View
    title: 'Sezony'
    initialize: (opts={}) ->
      @_module = opts.module
      @_template = opts.template
      @model.on('change', @render, @)

    render: ->
      data =
        Seasons: @collection.toJSON()
      @$el.html @_template(data)
      @
