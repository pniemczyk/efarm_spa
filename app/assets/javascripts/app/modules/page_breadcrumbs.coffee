﻿class window.App.Modules.PageBreadcrumb extends window.App.Modules.BaseViewModule
  @name: 'PageBreadcrumb'
  @require: ->
    name: 'PageBreadcrumb'
    libs: ['commands', 'reqres']

  _initialize: ->
    @_model = new _Model()
    @_template = @_getTemplate(@_templateName) unless @_template
    @_view = new _View(model: @_model, template: @_template)
    @_buttonTemplate = @_getTemplate(@_templateButtonName) unless @_buttonTemplate
    @_buttonsCollection = new _ButtonsCollection()
    @_buttonsView = new _ButtonsView(collection: @_buttonsCollection, template: @_buttonTemplate)

  _registerEvents: ->
    @_commands.setHandler("#{@name}-set", @set, @)
    @_commands.setHandler("#{@name}-setButtons", @setButtons, @)
    @_commands.setHandler("#{@name}-releaseButtons", @releaseButtons, @)

  remove: -> @_view.remove()

  set: (data) ->
    @releaseButtons()
    @_model.set('items', data)
    @_model.trigger('change')

  setButtons: (data) ->
    @_buttonsCollection.reset(data || [])

  releaseButtons: ->
    @_buttonsCollection.reset([])

  _templateName: 'page/breadcrumb'
  _templateButtonName: 'page/breadcrumb_button'

  class _Model extends Backbone.Model
    defaults:
      base_url: "#home"
      items:[]

  class _ButtonModel extends Backbone.Model
    defaults:
      icon: null
      label: null
      type: 'primary'
      cb: null

  class _ButtonsCollection extends Backbone.Collection
    model: _ButtonModel

  class _ButtonView extends Backbone.View
    initialize: (opts={})->
      @_template = opts.template
      @render()

    events:
      'click': 'onClick'

    onClick: (e) ->
      e.preventDefault()
      cb = @model.get('cb')
      cb() if typeof cb is 'function'

    render: ->
      @setElement($(@_template(@model.toJSON())))
      @

  class _ButtonsView  extends Backbone.View
    el: '#breadcrumbs ul.nav-buttons li.btn-group'
    initialize: (opts={})->
      @_template = opts.template
      @collection.on("reset", @render, @)
      @render()

    renderOne: (model)->
      @$el.append(new _ButtonView(model: model, template: @_template).el)

    render: ->
      @$el.empty()
      _.each(@collection.models, (model) => @renderOne(model))
      @

  class _View extends Backbone.View
    el: '#breadcrumbs ul.breadcrumb'
    initialize: (opts={})->
      @_template = opts.template
      @model.on("change", @render, @)
      @render()

    render: ->
      @$el.html(@_template(@model.toJSON()))
      @