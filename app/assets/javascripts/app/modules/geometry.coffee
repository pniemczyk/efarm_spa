class window.App.Modules.Geometry extends window.App.Modules.BaseModule
  @name: 'Geometry'
  @require: ->
    name: 'Geometry'
    libs: ['commands', 'reqres']

  _initialize: ->
    @_toolkit = new GeometryToolkit()
    @_assignLocalsFromToolkit()

  _registerEvents: ->
    @_reqres.setHandler("Geometry-projections",   (() => @_projections),   @)
    @_reqres.setHandler("Geometry-jstsParser",    (() => @_jstsParser),    @)
    @_reqres.setHandler("Geometry-jstsValidator", (() => @_toolkit.jstsValidator()), @)

    @_reqres.setHandler("Geometry-buildFeature", @buildFeature, @)
    @_reqres.setHandler("Geometry-buildLonLat",  @buildLonLat,  @)

    @_reqres.setHandler("Geometry-featureFromGeoJson",      @featureFromGeoJson,      @)
    @_reqres.setHandler("Geometry-featureFromWKT",          @featureFromWKT,          @)
    @_reqres.setHandler("Geometry-featureFromPoints",       @featureFromPoints,       @)
    @_reqres.setHandler("Geometry-featureFromEsriGeometry", @featureFromEsriGeometry, @)

    @_reqres.setHandler("Geometry-getCommonArea", @getCommonArea)
    @_reqres.setHandler("Geometry-getAreaInfoFromMeters", @handlerGetAreaInfoFromMeters, @)

  buildFeature: (featureObj, currentProjection=null) ->
    opts =
      toolkit: @_toolkit
      projection: @_detectProjection(currentProjection)
    new Feature(featureObj, opts)

  handlerGetAreaInfoFromMeters: (val) -> if val? then @_toolkit.areaInfoFromMeters(val) else null

  buildLonLat: (lonLatObj, currentProjection=null) ->
    opts =
      toolkit: @_toolkit
      projection: @_detectProjection(currentProjection)
    new LonLat(lonLatObj, opts)

  featureFromEsriGeometry: (esriGeometry, opts={}) ->
    opts = opts || {}
    if opts.type is 'Polygon'
      projection = esriGeometry.spatialReference.wkid unless opts.projection
      @featureFromPoints(esriGeometry.rings, opts)
    else if opts.type is 'MultiPolygon'
      console.log "NOT IMPLEMENTED YET :D"  #TODO
    else
      throw new Error("featureFromEsriGeometry cannot convert shape #{type}")

  featureFromWKT: (wkt, opts={})-> # WKT -> Well Known Text #TODO

  featureFromGeoJson: (geoJson, opts={}) ->
    return @_nullFeature() unless geoJson
    projection = @_detectProjection(opts.projection)

    try
      jstsGeometry  = @_jstsGeoJsonReader.read(geoJson)
      openLayersGeo = @_jstsParser.write(jstsGeometry)
      olFeature = new OpenLayers.Feature.Vector(openLayersGeo)
      @_transformGeometry.execute(olFeature.geometry, projection, @_projections.proj900913)

      feature = @buildFeature(olFeature)
      feature.simplify(opts) if opts.simplifyShape
      feature.tryFix() if opts.fixShape
      feature
    catch e
      console.log e
      null

  featureFromPoints: (pointsArray, opts={}) ->
    opts = opts || {}
    projection = @_detectProjection(opts.projection)
    if opts.type is 'Polygon'
      feature = @_getOlPolygonFromPoints(pointsArray)
      @_transformGeometry.execute(feature.geometry, projection, @_projections.proj900913)
      @buildFeature(feature)

  getCommonArea: (mainFeature, features=[]) =>
    unless mainFeature.isValid()
      return _.map(features, (feature) => {feature: feature, areaInfo: null})

    mainFeatureGeometry = @_jstsParser.read(mainFeature.geometry())

    _.map(features, (feature) =>
      featureGeometry = @_jstsParser.read(feature.geometry())

      res = {feature: feature}

      if mainFeatureGeometry.intersects(featureGeometry)
        intersectionFeatureGeometry = @_jstsParser.write(mainFeatureGeometry.intersection(featureGeometry))
        intersectionFeatureOl = new OpenLayers.Feature.Vector(intersectionFeatureGeometry)

        areaInfo = @buildFeature(intersectionFeatureOl).getAreaInfo()
        res['areaInfo'] = if areaInfo.haRound > 0 then areaInfo else null
      else
        res['areaInfo'] = null

      res
    )

  _nullFeature: ->
    @buildFeature(new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Collection()))

  _getOlPolygonFromPoints: (pointsArray) ->
    rings = _.map(pointsArray, (points) ->
      p = _.map(points, (p) -> new OpenLayers.Geometry.Point(p[0], p[1]))
      new OpenLayers.Geometry.LinearRing(p))

    polygon = new OpenLayers.Geometry.Polygon(rings)
    new OpenLayers.Feature.Vector(polygon)

  _detectProjection: (projection) ->
    return @_projections.proj900913 unless projection
    return @_projections['proj' + projection.toString().replace('EPSG:','')] if typeof projection is 'string'  || typeof projection is 'number'
    # TODO Log to NewRelic of unsupported projection
    log("unsupported projection #{projection}")
    projection

  _assignLocalsFromToolkit: () ->
    @_projections       = @_toolkit.projections
    @_jstsParser        = @_toolkit.jstsParser
    @_jstsGeoJsonReader = @_toolkit.jstsGeoJsonReader
    @_transformGeometry = @_toolkit.transformGeometry
    @_simplifier        = @_toolkit.simplifier

  class Feature
    CLASS_NAME: 'Geometry.Feature'
    constructor: (@feature, opts={}) ->
      @toolkit = opts.toolkit
      @_assignLocalsFromToolkit(@toolkit)
      @currentProjection  = opts.projection || @_projections.proj900913

    getOl:    -> @feature

    #TODO: Find better way to get labal id
    _htmlElemsIds: ->
      gId = "##{@geometry().id}"
      arr = gId.split('_')
      number = parseInt(arr[arr.length - 1]) + 1
      [gId, "#OpenLayers_Feature_Vector_#{number}_label", "#OpenLayers_Feature_Vector_#{number}_outline"]

    fadeOut:  (delay=3000, fn=null) ->
      ids = @_htmlElemsIds()
      f = @feature

      for id in ids
        $(id).stop().fadeOut delay, ->
          if (id == ids[ids.length - 1])
            fn() if typeof fn is 'function'
            if f and f.layer
              f.layer.removeFeatures([f])

    fadeIn:   (delay=3000, fn=null) ->
      if @layer() then @layer().addFeatures([@feature])

      ids = @_htmlElemsIds()

      for id in ids
        $(id).stop().fadeIn delay, ->
          if (id == ids[ids.length - 1])
            fn() if typeof fn is 'function'

    firstOlPoint: ->
      components = @_components()
      return unless components?
      return unless components.length
      components[0].components[0].clone()

    geometry: -> if @feature then @feature.geometry
    olId:     -> if @feature then @feature.id
    id:       -> if @feature then @feature.attributes.id
    getCurrentProjection: -> @currentProjection

    isPoint:           -> @shapeType() is "Point"
    isPolygon:         -> @shapeType() is "Polygon"
    isMultiPolygon:    -> @shapeType() is "MultiPolygon"
    isLineString:      -> @shapeType() is 'LineString'
    isMultiLineString: -> @shapeType() is 'MultiLineString'
    isConcreteGeoType: -> not (@shapeType() is 'GeometryCollection' || @shapeType() is 'Collection')
    shapeType:         -> @_transformGeometry.shapeType(@geometry())

    isSame: (feature) -> feature && @olId() == feature.olId()
    isDrawn:          -> @feature.layer? && @geometry()
    hasEmptyGeo:      -> not @isConcreteGeoType() || not @geometry()?
    layer:            -> @feature.layer || @attributes().layer
    attributes:       -> @feature.attributes
    isValid:          -> @_validationErrors().length == 0
    setName: (name)   ->
      @addToAttributes({name: name})
      @layer().drawFeature(@feature) if @isDrawn()

    addToAttributes: (opts={}) ->
      return unless opts?
      $.extend(@attributes(), opts)

    clear: -> if @isDrawn() then @_updateGeoOnLayer(new OpenLayers.Geometry.Collection())

    destroy: ->
      @feature.layer.removeFeatures([@feature]) if @isDrawn()
      @feature.destroy()
      null

    getTransformedFeature: (toProjection) ->
      toProjection   = @_projections['proj' + toProjection.toString().replace('EPSG:','')] if typeof toProjection is 'string'  || typeof toProjection is 'number'
      feature = @feature.clone()
      @_transformGeometry.execute(feature.geometry, @currentProjection, toProjection)
      feature

    transform: (toProjection) ->
      @_transformGeometry.execute(@feature.geometry, @currentProjection, toProjection)
      @currentProjection = @_projections['proj' + toProjection.toString().replace('EPSG:','')] if typeof toProjection is 'string'  || typeof toProjection is 'number'

    getAreaInfo: ->
      m  = if @_isTypeSupportingAreaInfo() then @geometry().getGeodesicArea(@currentProjection) else 0
      @toolkit.areaInfoFromMeters(m)

    toGeoJson: ->
      if (@geometry() && @isConcreteGeoType())
        @_jstsGeoJsonWriter.write(@_jstsParser.read(@geometry()))
      else
        null

    shapeInfo: ->
      info = (@attributes() || {})
      info.projection = @_currentProjectionCode()

      {
        info: info,
        geoJson: @toGeoJson()
      }

    toParcel: -> @_parcelFromGeo(@geometry())
    toBoundsParcel: -> unless @hasEmptyGeo() then @_parcelFromGeo(@geometry().getBounds().toGeometry())

    union: (unionFeature) ->
      return unionFeature.destroy() unless unionFeature.isValid()
      return unionFeature.destroy() unless @isValid()

      return @_updateGeoOnLayer(unionFeature.geometry()) unless @geometry()

      unionFeatureGeo = @_jstsParser.read(unionFeature.geometry().clone())
      selfGeo         = @_jstsParser.read(@geometry())
      newGeo          = @_jstsParser.write(selfGeo.union(unionFeatureGeo))

      unionFeature.destroy()

      @_updateGeoOnLayer(newGeo)

    exclude: (exclusionFeature) ->
      return exclusionFeature.destroy() unless exclusionFeature.isValid()
      return exclusionFeature.destroy() unless @isValid()

      exclusionFeatureGeo = @_jstsParser.read(exclusionFeature.geometry().clone())
      selfGeo        = @_jstsParser.read(@geometry())
      newGeo         = @_jstsParser.write(selfGeo.difference(exclusionFeatureGeo))

      exclusionFeature.destroy()

      unless newGeo.components == [] #user removed everything, the shape is empty
        @_updateGeoOnLayer(newGeo)

    unionItselfOrRestore: (geoToRestore) ->
      unless @isValid()
        @_updateGeoOnLayer(geoToRestore)
        return {unioned: false}

      clonesComponents = _.map(@_components(), (c) => @_jstsParser.read(c.clone()))
      headClone = _.find(clonesComponents, (c) => _.find(clonesComponents, (cc) -> c != cc && c.intersects(cc)))
      if headClone
        _.each(clonesComponents, (c) -> if c != headClone then headClone = headClone.union(c))
        @_updateGeoOnLayer(@_jstsParser.write(headClone))
      {unioned: true}

    simplify: (opts={}) ->
      if @isPolygon()
        _.each(@_components(), (p) => @_simplifyPolygon(p, opts))

      if @isMultiPolygon()
        _.each(@_components(), (c) => _.each(c.components, (p) => @_simplifyPolygon(p, opts)))

      @_updateGeoOnLayer(@geometry().clone())

    tryFix: ->
      errors = @_validationErrors()
      return if errors.length == 0
      # TODO

    _parcelFromGeo: (geometry) ->
      geometry = geometry.clone()
      @_transformGeometry.execute(geometry, @currentProjection, @_projections.proj2180)

      jstsGeometry = @_jstsParser.read(geometry)
      esriGeometry = @_geoJsonConverter.toEsri(@_jstsGeoJsonWriter.write(jstsGeometry))
      esriGeometry.spatialReference.wkid = 2180

      parcel =
        mapExtent: geometry.getBounds().clone().transform(@currentProjection, @_projections.proj2180).toBBOX()
        esriGeometry: esriGeometry
        tolerance: 0

    _isTypeSupportingAreaInfo: -> @isPolygon() || @isMultiPolygon()

    _isBetweenPoints: (point, nextPoint, maybePoint) ->
      isBetweenX = (point.x <= maybePoint.x && nextPoint.x >= maybePoint.x) || (point.x >= maybePoint.x && nextPoint.x <= maybePoint.x)
      return false unless isBetweenX
      (point.y <= maybePoint.y && nextPoint.y >= maybePoint.y) || (point.y >= maybePoint.y && nextPoint.y <= maybePoint.y)

    _simplifyPolygon: (p, opts) =>
      p.components = @_simplifier.getSimplified(p.components, tolerance: opts.tolerance)

    _validationErrors: ->
      validate = (geo) =>
        jstsGeometry = @_jstsParser.read(geo)
        validator = @_jstsValidator()
        validator.checkValid(jstsGeometry)
        validator.validErr

      if @isMultiPolygon()
        _.compact(_.map(@_components(), (c) => validate(c)))
      else
        _.compact([validate(@geometry())])

    _updateGeoOnLayer: (newGeo) ->
      if @isDrawn()
        layer = @layer() # to keep the reference
        layer.removeFeatures([@feature])
        @feature.geometry.destroy()
        @feature.geometry = newGeo
        layer.addFeatures([@feature])
        layer.refresh()
      else
        @feature.geometry.destroy() if @geometry()
        @feature.geometry = newGeo

    _components: -> @geometry().components

    _currentProjectionCode: -> _.last(@currentProjection.toString().split(':'))

    _assignLocalsFromToolkit: (toolkit) ->
      @_projections       = toolkit.projections
      @_jstsParser        = toolkit.jstsParser
      @_geoJsonConverter  = toolkit.geoJsonConverter
      @_jstsGeoJsonWriter = toolkit.jstsGeoJsonWriter
      @_transformGeometry = toolkit.transformGeometry
      @_simplifier        = toolkit.simplifier
      @_toolkit           = toolkit

    _jstsValidator: -> @_toolkit.jstsValidator()

    toJSON: ->
      $.extend(@toGeoJson(), {attributes: @attributes})

  class LonLat
    CLASS_NAME: 'Geometry.LonLat'
    # defaultProjection proj900913
    constructor: (lonLat, opts={}) ->
      @_assignLocalsFromToolkit(opts.toolkit)
      @currentProjection = opts.projection || @_projections.proj900913

      @lonLat = if _.isArray(lonLat)
        new OpenLayers.LonLat(lonLat[0], lonLat[1])
      else if _.isObject(lonLat) && lonLat.CLASS_NAME is "OpenLayers.LonLat"
        lonLat
      else if _.isObject(lonLat) && lonLat.CLASS_NAME is 'Geometry.LonLat'
        lonLat.get()
      else
        throw new Error("Wrong LonLat for Geometry.LonLat")

      unless @currentProjection is @_projections.proj900913
        @lonLat = @lonLat.transform(@currentProjection, @_projections.proj900913) unless @currentProjection is @_projections.proj900913
        @currentProjection = @_projections.proj900913

    getOl: -> @lonLat
    getCurrentProjection: -> @currentProjection

    toPoint: -> @_point = new OpenLayers.Geometry.Point(@lonLat.lon, @lonLat.lat)

    toParcel:->
      point = @toPoint().clone()
      point.transform(@currentProjection, @_projections.proj2180)
      point.calculateBounds()

      parcel =
        mapExtent: point.bounds.toBBOX()
        esriGeometry: {x: point.x, y: point.y, spatialReference: {wkid: 2180}}
        tolerance: 0

    toBoundsParcel: -> @toParcel()

    getTransformedLonLat: (toProjection) ->
      toProjection   = @_projections['proj' + toProjection.toString().replace('EPSG:','')] if typeof toProjection is 'string'  || typeof toProjection is 'number'
      lonLat = @lonLat.clone()
      lonLat.transform(@currentProjection, toProjection)
      lonLat

    transform: (toProjection) ->
      toProjection   = @_projections['proj' + toProjection.toString().replace('EPSG:','')] if typeof toProjection is 'string'  || typeof toProjection is 'number'
      @lonLat.transform(@currentProjection, toProjection)

    _assignLocalsFromToolkit: (toolkit) ->
      @_projections = toolkit.projections

  class GeometryToolkit
    constructor: () ->
      @projections       = new Projections()
      @jstsParser        = new jsts.io.OpenLayersParser() # reads/writes OpenLayers and Jsts Geometry
      @jstsWktReader     = new jsts.io.WKTReader()
      @jstsGeoJsonReader = new jsts.io.GeoJSONReader()
      @jstsGeoJsonWriter = new jsts.io.GeoJSONWriter()
      @simplifier        = window.shapesSimplifier
      @geoJsonConverter  = window.geoJsonConverter() # geoJson -> ESRI
      @esriConverter     = window.esriConverter()    # ESRI -> geoJson
      @transformGeometry = new TransformGeometry(@projections)

    jstsValidator: -> new jsts.operation.valid.IsValidOp() # checks whether geo is valid

    areaInfoFromMeters: (m) ->
      ha = m * 0.0001
      areaInfo =
        m: m
        ha: ha
        haRound: Math.round(ha * 1000) / 1000

    class Projections
      Proj4js.defs["EPSG:2180"] = "+proj=tmerc +lat_0=0 +lon_0=19 +k=0.9993 +x_0=500000 +y_0=-5300000 +ellps=GRS80 +units=m +no_defs"
      getProjection: (name) -> @["proj#{name}"] if @["proj#{name}"]
      getBbox: (name) -> @["bbox#{name}"] if @["bbox#{name}"]
      proj900913: new OpenLayers.Projection("EPSG:900913")
      proj4326:   new OpenLayers.Projection("EPSG:4326")
      proj3785:   new OpenLayers.Projection("EPSG:3785")
      proj2180:   new OpenLayers.Projection("EPSG:2180") # default projection when querying for parcels, and when reading data from .shp files
      bbox4326:   new OpenLayers.Bounds(43.6055107169651, 9.30453661238444, 58.1319431387457, 40.2372735881515)
      bbox3785:   new OpenLayers.Bounds(1035776.27775801, 5404595.6361576, 4479192.80674266, 7995085.89223208)
      bbox2180:   new OpenLayers.Bounds(144907.1658, 140544.7241, 877004.0070, 910679.6817)

    class TransformGeometry
      constructor: (@_projections) ->
      execute: (geometry, fromProjection, toProjection) ->
        fromProjection   = @_projections['proj' + fromProjection.toString().replace('EPSG:','')] if typeof fromProjection is 'string'  || typeof fromProjection is 'number'
        toProjection     = @_projections['proj' + toProjection.toString().replace('EPSG:','')]   if typeof toProjection is 'string'  || typeof toProjection is 'number'

        switch @shapeType(geometry)
          when "Point"        then @_transformPoint(geometry, fromProjection, toProjection)
          when "Polygon"      then @_transformPolygon(geometry, fromProjection, toProjection)
          when "MultiPolygon" then @_transformMultiPolygon(geometry, fromProjection, toProjection)
          else null

      shapeType: (geometry)-> if geometry then geometry.CLASS_NAME.replace('OpenLayers.Geometry.','') else ''

      _transformPolygon: (polygonGeometry, fromProjection, toProjection) ->
        _.each(polygonGeometry.components, (linearString) -> _.each(_.initial(linearString.components), (point) -> point.transform(fromProjection, toProjection)))

      _transformMultiPolygon: (multipolygonGeometry, fromProjection, toProjection) ->
        _.each(multipolygonGeometry.components, (polygon) => @_transformPolygon(polygon, fromProjection, toProjection))

      _transformPoint: (pointGeometry, fromProjection, toProjection) ->
        pointGeometry.transform(fromProjection, toProjection)