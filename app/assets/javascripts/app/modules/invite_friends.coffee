﻿class window.App.Modules.InviteFriends extends window.App.Modules.BaseViewModule
  @name: 'InviteFriends'
  @require: ->
    name: 'InviteFriends'
    libs: ['commands', 'reqres', 'transaction']

  routes:
    'inviteFriends':'showFriendsInvitationModal'

  _initialize: -> @_url = @_getUrl('InviteFriendsUrl')
  _registerEvents: -> @_commands.setHandler("#{@name}-showModal", @showModal, @)

  showFriendsInvitationModal: => @showModal()

  el: '#inviteFriendsModal'
  elSend: '#sendInvitation'
  elTags: '#inviteFriendsEmailContainer'

  showModal: ->
    template = @_getTemplate('page/invite_friends')
    $('body').append(template())
    $(@elTags).tagit(
      placeholderText: 'Dodaj adresy email'
      caseSensitive: false
      preprocessTag: (value) -> value.toLowerCase().replace(/[^a-z0-9_.@]/g, '')
      beforeTagAdded: (e, ui) => @_isEmail(ui.tagLabel)
    )
    $(@el).modal()
    $(@el).on('hide.bs.modal', @_clearView)
    $(@el).on('shown.bs.modal', -> $('.tagit-new input').first().focus())
    $(@elSend).on('click', @sendClick)


  sendClick: (e) =>
    emails = $(@elTags).tagit('assignedTags')
    @_sendInvitationFor(emails)
    $(@el).modal('hide')

  _clearView: ->
    $('#inviteFriendsModal').remove()

  _sendInvitationFor: (emails) ->
    opts =
      transactionName: 'InviteFriends'
      method: 'POST'
      data:
        emails: emails
      url: @_url
    @_transaction.request(opts).fail( =>
      @info(title: 'Zaproszenie znajomych', text: 'Nie udało się wysłać zaproszenia. Prosze spróbować później.', type: 'warning', center: true)
    )

  _isEmail: (email) ->
    regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/
    regex.test(email)