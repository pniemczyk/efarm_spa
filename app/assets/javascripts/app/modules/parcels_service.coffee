class window.App.Modules.ParcelsService extends window.App.Modules.BaseModule
  @name: 'ParcelsService'
  @require: ->
    name: 'ParcelsService'
    config: 'ParcelsService'
    libs: ['commands', 'reqres', 'transaction']

  _registerEvents: -> @_reqres.setHandler("Parcels-readParcels", @_readParcels, @)

  _initialize: ->
    @_url = @_getUrl('ParcelsUrl')
    @Model = @_getModel('Parcel')

  _newModel:  (parcel) -> new (@_getModel('Parcel'))({}, parcel: parcel)
  _readParcels: (data) ->
    return [] unless data
    result = null
    opts =
      transactionName: 'parcels_by_geo'
      method: 'GET'
      data:
        data: JSON.stringify(data)
      url: @_url
      contentType: 'application/json'
      async: false

    @_transaction.request(opts).done((res) => result = res['json_response']['data'])
    _.map(result, (p) => @_newModel(p))