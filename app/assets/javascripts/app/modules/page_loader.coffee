﻿class window.App.Modules.PageLoader extends window.App.Modules.BaseViewModule
  @name: 'PageLoader'

  @require: ->
    name: 'PageLoader'
    libs: ['commands', 'reqres']

  _registerEvents: -> @_commands.setHandler("#{@name}-show", @show, @)

  show: (message=null) ->
    @_template = @_getTemplate(@_templateName) unless @_template
    @_view = new _View(template: @_template, message: message) unless @_view
    @_showView(@_view)

  _templateName: 'page/loader'

  class _View extends Backbone.View
    tagName: 'div'
    className: 'page-loader'

    initialize: (opts={}) ->
      @_template = opts.template
      @_message = opts.message

    render: ->
      @$el.html(@_template(message: @_message))
      @