class window.App.Modules.WeatherService extends window.App.Modules.BaseModule
  @name: 'WeatherService'
  @require: ->
    name: 'WeatherService'
    libs: ['reqres']

  _registerEvents: -> @_reqres.setHandler("Weather-getUrls", @getUrls, @)
  _coampsUrl: (lon, lat, date) -> "http://www.meteo.pl/php/mgram_search.php?NALL=#{lat}&EALL=#{lon}&lang=#{@lang}&fdate=#{date}"
  _umUrl: (lon, lat, date) -> "http://www.meteo.pl/um/php/mgram_search.php?NALL=#{lat}&EALL=#{lon}&lang=#{@lang}&fdate=#{date}"

  _initialize: -> @lang = 'pl'

  getUrls: (date, lonLat, projection=900913) ->
    lonLat = @_reqres.request('Geometry-buildLonLat', lonLat, projection)
    lonLat = lonLat.getTransformedLonLat(4326)
    date = date.toISOString().replace(/-/g, '').substr(0,8) + '00'
    coamps: @_coampsUrl(lonLat.lon, lonLat.lat, date)
    um: @_umUrl(lonLat.lon, lonLat.lat, date)
