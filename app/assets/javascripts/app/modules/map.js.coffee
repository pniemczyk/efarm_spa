class window.App.Modules.Map extends window.App.Modules.BaseModule
  @name: 'Map'
  @require: ->
    name: 'Map'
    config: 'Map'
    libs: ['reqres', 'commands']

  options:
    defaultMapLayer: ['googlehybrid', 'osm', 'ortofoto', 'dzkat', 'parcels', 'previews', 'fields']
  _listenForLonLat: false
  _currentField: null
  _mapLayers: []

  _registerEvents: ->
    @_reqres.setHandler('Map-init',              @handlerInitMap, @)
    @_reqres.setHandler('Map-addFieldShape',     @handlerAddFieldShape, @)
    @_reqres.setHandler('Map-addParcelShape',    @handlerAddParcelShape, @)
    @_reqres.setHandler('Map-addPreviewShape',   @handlerAddPreviewShape, @)
    @_reqres.setHandler('Map-multiAddFields',    @handlerAddMultiFields, @)
    @_reqres.setHandler('Map-getLayerShapeById', @handlerGetLayerShapeById, @)
    @_reqres.setHandler('Map-getZoom',           @handlerGetZoom, @ )
    @_reqres.setHandler('Map-selectField',   @handlerSelectField, @)
    @_reqres.setHandler('Map-removeSelectedShape',      @handlerRemoveSelectedField, @)
    @_reqres.setHandler('Map-unionToSelectedShape',     @handlerUnionToSelectedShape, @)
    @_reqres.setHandler('Map-exclusionToSelectedShape', @handlerExclusionToSelectedShape, @)
    @_reqres.setHandler('Map-addShapeFromGeoJson', @addShapeFromGeoJson, @)
    @_reqres.setHandler('Map-unselectSelectedShape', @handlerUnselectSelectedShape, @)
    @_reqres.setHandler('Map-clearSelectedShape', @handlerClearSelectedShape, @)

    @_commands.setHandler('Map-zoomTo', @handlerZoomTo, @)
    @_commands.setHandler('Map-panTo',  @handlerPanTo,  @)
    @_commands.setHandler('Map-setCenter',   @handlerSetCenter, @)
    @_commands.setHandler('Map-setHandMode', @handlerSetHandMode, @)
    @_commands.setHandler('Map-startDrawingShape',          @handlerStartDrawingShape, @)
    @_commands.setHandler('Map-startDrawingUnionShape',     @handlerStartDrawingUnionShape, @)
    @_commands.setHandler('Map-startDrawingExclusionShape', @handlerStartDrawingExlusionShape, @)
    @_commands.setHandler('Map-setListenForLonLat', @handlerSetListenForLonLat, @)
    @_commands.setHandler('Map-setLayerVisibility', @handlerSetLayerVisibility, @)
    @_commands.setHandler('Map-showMeOnMap',        @handlerSetCenterByGeoLocation, @)
    @_commands.setHandler('Map-clearParcelsLayer',  @handlerClearParcelsLayer,  @)
    @_commands.setHandler('Map-clearPreviewsLayer', @handlerClearPreviewsLayer, @)
    @_commands.setHandler('Map-refreshMap', @handlerRefreshMap, @)

  _initialize: ->
    OpenLayers.IMAGE_RELOAD_ATTEMPTS = 3;
    OpenLayers.ImgPath = "/assets/openlayers/"
    OpenLayers.Util.onImageLoadErrorColor = 'transparent';
    @projections = @_reqres.request('Geometry-projections')
    @options = $.extend({}, @options, @_config)
    @_registerShapeEvents()

  handlerInitMap: (@mapOpts={})->
    @destroy() if @map
    @mapOpts.projections = @projections
    @mapFactory = new OSMapFactory(@mapOpts)
    @map = @mapFactory.create()
    @_createLayersStyles()
    @_addAllDefinedMapLayers()
    @_initShapeControls()
    @_initJstsComponents()
    @map.zoomToMaxExtent()
    @map.events.register('click', @map, @getLonLatFromClick)
    @_initDevTools()
    @mapOpts.afterLoaded() if typeof @mapOpts.afterLoaded is 'function'
    @

  handlerAddFieldShape: (feature, opts={})  ->
    @_addNewFeature(feature, opts)
    @_selectFieldByModifyControl(feature)

  handlerAddParcelShape: (feature, opts={})  ->
    @_addNewFeature(feature, $.extend(true, opts, {layer: 'parcels'}))

  handlerAddPreviewShape: (feature, opts={}) ->
    @_addNewFeature(feature, $.extend(true, opts, {layer: 'previews'}))

  handlerAddMultiFields: (features, opts={}) ->
    @_multiAddFeature(features, opts || {})

  handlerClearSelectedShape: ->
    return unless @_currentField
    @_currentField.clear()
    @handlerUnselectSelectedShape()

  handlerRefreshMap: ->
    if @map.baseLayer and (@map.baseLayer.type == 'hybrid' || @map.baseLayer.type == 'satellite')
      @map.updateSize()

  handlerRemoveSelectedField: -> @_removeSelectedFeature()

  handlerSetListenForLonLat: (state) ->
    state == !!state
    @_listenForLonLat = state
    @_setNoneControls() if state

  handlerZoomTo: (zoom) -> @map.zoomTo(zoom)
  handlerPanTo: (lonLat, projection=4326) -> # TODO pass our LonLat
    properLonLat = @_reqres.request('Geometry-buildLonLat', lonLat, projection).getOl()
    @map.panTo(properLonLat)

  handlerStartDrawingShape: (@_drawnShapeName) -> @_setControlsWhenDrawingField()
  handlerStartDrawingUnionShape:    -> @_setControlsWhenDrawingUnion()
  handlerStartDrawingExlusionShape: -> @_setControlsWhenDrawingExclusion()
  handlerSetHandMode: -> @_setNoneControls()

  handlerGetZoom:            -> @map.getZoom()
  handlerClearParcelsLayer:  -> @_parcelsLayer.destroyFeatures()
  handlerClearPreviewsLayer: -> @_previewLayer.destroyFeatures()

  handlerSetCenter: (lonLat, zoom) ->
    @map.setCenter(lonLat, zoom, false, true)

  handlerUnionToSelectedShape: (feature)  ->
    @_unionWithCurrentField(feature)

  handlerExclusionToSelectedShape: (feature) ->
    @_exclusionWithCurrentField(feature)

  handlerSelectField: (field)   ->
    fieldFeature = @handlerGetLayerShapeByField('fields', field)
    if fieldFeature
      @_selectFieldByModifyControl(fieldFeature)
      @_currentField = fieldFeature
      @map.setCenter(fieldFeature.geometry().getBounds().getCenterLonLat(), 16) if fieldFeature.geometry().getBounds()

  handlerUnselectSelectedShape: ->
    @_destroyFieldModifyControl()
    @_currentField = null

  handlerGetLayerShapeByField: (layerName, field) ->
    layer = @_getLayerByName(layerName)
    if layer
      layer = layer.olLayer
      if field.persisted()
        f = _.find(layer.features, (f) => @_buildFeatureFromOl(f).id() == field.id)
      else
        f = _.find(layer.features, (f) => @_buildFeatureFromOl(f).olId() == field.olId())

      if f then @_buildFeatureFromOl(f)

  handlerSetLayerVisibility: (layerName, show) ->
    layer = @_getLayerByName(layerName)
    return unless layer
    return layer.olLayer.setVisibility(show) if layer.isOverlay

    if layer.isOsm
      @_googleLayer.setVisibility(!show)
      @_osmLayer.setVisibility(show)
      if show then @map.setBaseLayer(@_osmLayer) else @map.setBaseLayer(@_googleLayer)
    else
      @_googleLayer.setVisibility(show)
      @_osmLayer.setVisibility(!show)
      if show then @map.setBaseLayer(@_googleLayer) else @map.setBaseLayer(@_osmLayer)

  handlerSetCenterByGeoLocation: ->
    @_reqres.request('SearchLocation-getCurrentLocation').done((pos) =>
      lonLat = @_reqres.request('Geometry-buildLonLat', [pos.coords.longitude, pos.coords.latitude], 4326).getTransformedLonLat(3785)
      @handlerSetCenter(lonLat, 16)
    )

  destroy: ->
    _.map(@map.layer, (l) => (l.destroyFeatures(); @map.removeLayer(l); l.destroy()))
    _.map(_.compact(@map.controls), (c) => (c.deactivate(); @map.removeControl(c); c.destroy()))
    @_fieldModifyCtrl.destroy() if @_fieldModifyCtrl?
    @_fieldModifyCtrl = null
    @map.destroy()
    @map = null
    @_mapLayers = []
    @_currentField = null
    @_listenForLonLat = false

  # Internal use methods

  _initJstsComponents: () ->
    @_jstsParser = @_reqres.request('Geometry-jstsParser') #new jsts.io.OpenLayersParser() # reads/writes OpenLayers and Jsts Geometry
    @_jstsValidator = @_reqres.request('Geometry-jstsValidator') #new jsts.operation.valid.IsValidOp() # checks whether geo is valid

  _initShapeControls: ->
    @_highlighCtrl = new OpenLayers.Control.SelectFeature([@_parcelsLayer, @_previewLayer, @_fieldsLayer],
      # hover: true
      highlightOnly: true
      renderIntent: "temporary"
    )

    @_selectCtrl = new OpenLayers.Control.SelectFeature([@_fieldsLayer], clickout: true)

    @_fieldsDrawCtrl = new OpenLayers.Control.DrawFeature(@_fieldsLayer, OpenLayers.Handler.Polygon)
    @_fieldsDrawCtrl.events.register('featureadded', '', @_featureAddedHandler);

    @_fieldsExclusionCtrl = new OpenLayers.Control.DrawFeature(@_fieldsLayer, OpenLayers.Handler.Polygon)
    @_fieldsExclusionCtrl.events.register('featureadded', '', @_exclusionFeatureAddHandler);

    @_fieldsUnionCtrl = new OpenLayers.Control.DrawFeature(@_fieldsLayer, OpenLayers.Handler.Polygon)
    @_fieldsUnionCtrl.events.register('featureadded', '', @_unionFeatureAddHandler);

    @map.addControl(@_highlighCtrl)
    @map.addControl(@_selectCtrl)
    @map.addControl(@_fieldsDrawCtrl)
    @map.addControl(@_fieldsExclusionCtrl)
    @map.addControl(@_fieldsUnionCtrl)

  _initDevTools: ->
    window._dev = {} unless window._dev
    window._dev.mm = @
    window._dev.map = @map
    window._dev.parcelsLayer = @_parcelsLayer
    window._dev.previewLayer = @_previewLayer
    window._dev.fieldsLayer  = @_fieldsLayer

  _featureAddedHandler: (data) => @_selectFieldByModifyControl(@_addNewFeature(@_buildFeatureFromOl(data.feature), {name: @_drawnShapeName}))
  _onFieldShapeModifyStartHandler: (olFeature) => @_geoBeforeModification = @_buildFeatureFromOl(olFeature).geometry().clone()

  _onFieldShapeModifyHandler: (feature) =>
    modifiedFeature = @_buildFeatureFromOl(feature)
    unless @_currentField || (not @_currentField.isSame(modifiedFeature))
      modifiedFeature.destroy()
      return

    res = @_currentField.unionItselfOrRestore(@_geoBeforeModification)
    @_geoBeforeModification = @_currentField.geometry().clone()
    @_selectFieldByModifyControl(@_currentField)
    @_commands.execute('Map-FeatureUpdated', @_currentField) if res.unioned

  _exclusionFeatureAddHandler: (data) =>
    @_exclusionWithCurrentField(@_buildFeatureFromOl(data.feature))

  _unionFeatureAddHandler: (data) =>
      @_unionWithCurrentField(@_buildFeatureFromOl(data.feature))

  _exclusionWithCurrentField: (exclusionFeature) ->
    unless @_currentField
      exclusionFeature.destroy()
      @_setControlsWhenExclusionDrawn()
      return

    @_currentField.exclude(exclusionFeature)
    @_setControlsWhenExclusionDrawn()
    @_selectFieldByModifyControl(@_currentField)
    @_commands.execute('Map-FeatureUpdated', @_currentField)

  _unionWithCurrentField: (unionFeature) ->
    unless @_currentField
      unionFeature.destroy()
      @_setControlsWhenUnionDrawn()
      return

    @_currentField.union(unionFeature)
    @_setControlsWhenUnionDrawn()
    @_selectFieldByModifyControl(@_currentField)
    @_commands.execute('Map-FeatureUpdated', @_currentField)

  _fieldsLayerFeatureSelectedHandler: (data) =>
    feature = @_buildFeatureFromOl(data.feature)
    @_createModifyControlForField(feature)
    unless feature.isSame(@_currentField)
      @_currentField = feature
      @_commands.execute('Map-fieldSelected', feature)

  _parcelsLayerFeatureSelected: (data) =>
    @_commands.execute('Map-ParcelOnMapSelected', @_buildFeatureFromOl(data.feature))

  _previewLayerFeatureSelected: (data) =>
    @_commands.execute('Map-PreviewOnMapSelected', @_buildFeatureFromOl(data.feature))

  _buildFeatureFromOl: (olFeature, projection=null) -> @_reqres.request('Geometry-buildFeature', olFeature, projection)

  _addNewFeature: (feature, opts={}) ->
    opts = opts || {}
    unless feature.isValid()
      return feature.destroy() unless opts.allowInvalid

    layer = @_getLayerByName(opts.layer)
    layer = if layer then layer.olLayer else @_fieldsLayer

    unless _.find(layer.features, (f) => f.id == feature.olId())
      layer.addFeatures([feature.getOl()])

    feature.addToAttributes({name: opts.name, layer: layer})
    layer.redraw()

    if layer == @_fieldsLayer
      @_currentField = feature
      @_setControlsWhenFieldDrawn()
      @_commands.execute('Map-drawShapeDone', feature)

    feature

  _multiAddFeature: (features, opts={}) ->
    layer = @_getLayerByName(opts.layer)
    layer = if layer then layer.olLayer else @_fieldsLayer
    anythingAdded = false

    _.each(features, (feature) =>
      unless feature.isValid()
        return feature.destroy() unless opts.allowInvalid

      unless _.find(layer.features, (f) => f.id == feature.olId())
        layer.addFeatures([feature.getOl()])
        feature.addToAttributes({layer: layer})
        anythingAdded = true
    )

    if anythingAdded
      @_setControlsWhenFieldDrawn()
      layer.redraw()
      @map.zoomToExtent(_.last(features).geometry().getBounds())
      @map.zoomTo(@map.getZoom() - 2)

  _removeSelectedFeature: ->
    return false unless @_currentField
    @_destroyFieldModifyControl()
    @_currentField.destroy()
    @_currentField = null
    @_setControlsWhenFieldRemoved()
    true

  _createModifyControlForField: (feature) ->
    @_destroyFieldModifyControl()

    @_fieldModifyCtrl = new OpenLayers.Control.ModifyFeature(feature.layer(), {
      standalone: true
      clickout: true
      mode: OpenLayers.Control.ModifyFeature.RESHAPE
      createVertices: true
      onModification: @_onFieldShapeModifyHandler
      onModificationStart: @_onFieldShapeModifyStartHandler
    })

    @map.addControl(@_fieldModifyCtrl)
    @_fieldModifyCtrl.activate()
    @_fieldModifyCtrl.selectFeature(feature.getOl())

  _featureUnselectedHandler: (data) => @_destroyFieldModifyControl()

  _destroyFieldModifyControl: () ->
    if @_fieldModifyCtrl
      @_fieldModifyCtrl.deactivate()
      @map.removeControl(@_fieldModifyCtrl)
      @_fieldModifyCtrl.destroy()
      @_fieldModifyCtrl = null

  _selectFieldByModifyControl: (feature) ->
    @_createModifyControlForField(feature) unless @_fieldModifyCtrl

    @_fieldModifyCtrl.deactivate()
    @_fieldModifyCtrl.activate()
    @_fieldModifyCtrl.selectFeature(feature.getOl())
    feature.layer().refresh()

  _shapeChangeData: (id, beforeActionGeo, afterActionGeo) ->
    new ShapeChangeData(id, beforeActionGeo.clone(), afterActionGeo.clone())

  _getLayerByName: (name) ->
    _.find(@_mapLayers, (l) -> l.name == name)

  addShapeFromWKT: (wkt, opts={}) -> @_reqres.request('Geometry-featureFromWKT', wkt)
  addShapeFromGeoJson: (geoJson, opts={}) ->
    opts = opts || {}

    feature = @_reqres.request('Geometry-featureFromGeoJson', geoJson, opts)
    return unless feature
    if @_addNewFeature(feature, opts)
      @map.zoomToExtent(feature.geometry().getBounds()) if opts.panToShape

  getLonLatFromClick: (e) =>
    return unless @_listenForLonLat
    @_commands.execute('Map-lonLatFromClick', @_reqres.request('Geometry-buildLonLat', @map.getLonLatFromViewPortPx(e.xy)))
    @_listenForLonLat = false
    @_setControlsWhenFieldDrawn()

  # Shape Controls states
  _setNoneControls: ->
    @_highlighCtrl.deactivate()
    @_selectCtrl.deactivate()
    @_fieldsDrawCtrl.deactivate()
    @_fieldsExclusionCtrl.deactivate()
    @_fieldsUnionCtrl.deactivate()

  _setControlsWhenFieldRemoved: ->
    @_highlighCtrl.activate()
    @_selectCtrl.activate()
    @_fieldsDrawCtrl.deactivate()
    @_fieldsExclusionCtrl.deactivate()
    @_fieldsUnionCtrl.deactivate()

  _setControlsWhenDrawingField: ->
    @_highlighCtrl.deactivate()
    @_selectCtrl.deactivate()
    @_fieldsDrawCtrl.activate()
    @_fieldsExclusionCtrl.deactivate()
    @_fieldsUnionCtrl.deactivate()

  _setControlsWhenFieldDrawn: ->
    @_highlighCtrl.activate()
    @_selectCtrl.activate()
    @_fieldsDrawCtrl.deactivate()
    @_fieldsExclusionCtrl.deactivate()
    @_fieldsUnionCtrl.deactivate()

  _setControlsWhenDrawingExclusion: ->
    @_highlighCtrl.deactivate()
    @_selectCtrl.deactivate()
    @_fieldsDrawCtrl.deactivate()
    @_fieldsExclusionCtrl.activate()
    @_fieldsUnionCtrl.deactivate()

  _setControlsWhenExclusionDrawn: ->
    @_highlighCtrl.activate()
    @_selectCtrl.activate()
    @_fieldsDrawCtrl.deactivate()
    @_fieldsExclusionCtrl.deactivate()
    @_fieldsUnionCtrl.deactivate()

  _setControlsWhenDrawingUnion: ->
    @_highlighCtrl.deactivate()
    @_selectCtrl.deactivate()
    @_fieldsDrawCtrl.deactivate()
    @_fieldsExclusionCtrl.deactivate()
    @_fieldsUnionCtrl.activate()

  _setControlsWhenUnionDrawn: ->
    @_highlighCtrl.activate()
    @_selectCtrl.activate()
    @_fieldsDrawCtrl.deactivate()
    @_fieldsExclusionCtrl.deactivate()
    @_fieldsUnionCtrl.deactivate()

  #Shapes

  _registerShapeEvents: ->
    cmd = @_commands
    OpenLayers.Handler.Polygon::dblclick = (e) ->
      if (not @freehandMode(e))
        @finishGeometry()
        cmd.execute('Map-finishGeometry')
      false

  #Layers

  _baseMapLayers: ['GoogleHybrid', 'Osm', 'OrtotFoto', 'Dzkat', 'Parcels', 'Previews', 'Fields']

  _addAllDefinedMapLayers: ->
    (@["_addLayer#{layer}"]()) for layer in @_baseMapLayers
    _.each(@_mapLayers, (l) => @map.addLayer(l.olLayer))

  _addLayerOsm: ->
    @_osmLayer = new OpenLayers.Layer.OSM()
    @_mapLayers.push(olLayer: @_osmLayer, name: 'osm', isOverlay: false, isOsm: true)

  _addLayerGoogleHybrid: ->
    @_googleLayer = new OpenLayers.Layer.Google("Google Hybrid", type: google.maps.MapTypeId.SATELLITE, sphericalMercator: true, numZoomLevels: 20,
    MAX_ZOOM_LEVEL: 20, isOverlay: false)
    @_mapLayers.push(olLayer: @_googleLayer, name: 'googlehybrid')

  _WMSGeoPortalMinScale: 25000 #def = 13500
  _addLayerOrtotFoto: ->
    layer = new WMSLayerFactory("Geoportal Ortofoto", "http://sdi.geoportal.gov.pl/wms_orto/wmservice.aspx", layers: "ORTOFOTO", projections: @projections, layerOpts: {opacity: 1, visibility: false, maxScale:1, minScale:@_WMSGeoPortalMinScale})
    @_mapLayers.push(olLayer: layer.create(), name: 'ortofoto', isOverlay: true)

  _addLayerDzkat: ->
    layer = new WMSLayerFactory("Geoportal DZKAT", "http://sdi.geoportal.gov.pl/wms_dzkat/wmservice.aspx", layers: "Dzialki,Numery_dzialek", projections: @projections, format: 'image/svg+xml', layerOpts: { visibility: false, maxScale:1, minScale:@_WMSGeoPortalMinScale})
    @_mapLayers.push(olLayer: layer.create(), name: 'dzkat', isOverlay: true)

  _addLayerParcels: ->
    @_parcelsLayer = new OpenLayers.Layer.Vector('Działki', {styleMap: @_parcelsLayerStyle})
    @_parcelsLayer.events.on({featureselected: @_parcelsLayerFeatureSelected})
    @_mapLayers.push(olLayer: @_parcelsLayer, name: 'parcels', isOverlay: true)

  _addLayerPreviews: ->
    @_previewLayer = new OpenLayers.Layer.Vector('Podgląd', {styleMap: @_previewLayerStyle})
    @_previewLayer.events.on({featureselected: @_previewLayerFeatureSelected})
    @_mapLayers.push(olLayer: @_previewLayer, name: 'previews', isOverlay: true)

  _addLayerFields: ->
    @_fieldsLayer = new OpenLayers.Layer.Vector('Wszystkie Pola', {styleMap: @_fieldsLayerStyle})
    @_fieldsLayer.events.on({featureselected: @_fieldsLayerFeatureSelectedHandler, featureunselected: @_featureUnselectedHandler})
    @_mapLayers.push(olLayer: @_fieldsLayer, name: 'fields', isOverlay: true)

  _createLayersStyles: ->
    @_createParcelsLayerStyle()
    @_createPreviewLayerStyle()
    @_createFieldsLayerStyle()

  _createParcelsLayerStyle: ->
    style = $.extend(true, {}, OpenLayers.Feature.Vector.style['default']) # get a copy of the default style
    style.fillColor = "#7EBCF2"
    style.fillOpacity = 0.2
    style.strokeWidth = 2
    style.strokeColor = "#032645"
    style.label = "${getLabel}";

    @_parcelsLayerStyle = new OpenLayers.StyleMap({
      default: new OpenLayers.Style(style, {
        context: {
          getLabel: (olFeature) => @_buildFeatureFromOl(olFeature, 3785).attributes().name
        }
      })
    })

  _createPreviewLayerStyle: ->
    style = $.extend(true, {}, OpenLayers.Feature.Vector.style['default']) # get a copy of the default style
    style.fillColor = "#B50061"
    style.fillOpacity = 0.4
    style.strokeWidth = 2
    style.strokeColor = "#38021F"
    style.label = "${getLabel}";

    @_previewLayerStyle = new OpenLayers.StyleMap({
      default: new OpenLayers.Style(style, {
        context: {
          getLabel: (olFeature) => @_buildFeatureFromOl(olFeature, 3785).attributes().name
        }
      })
    })

  _createFieldsLayerStyle: ->
    style = $.extend(true, {}, OpenLayers.Feature.Vector.style['default']) # get a copy of the default style
    style.label = "${getLabel}";
    style.fillOpacity = 0.4
    style.strokeWidth = 3

    @_fieldsLayerStyle = new OpenLayers.StyleMap({
      default: new OpenLayers.Style(style, {
        context: {
          getLabel: (olFeature) =>
            feature = @_buildFeatureFromOl(olFeature, 3785)
            if feature.geometry() && (feature.isPolygon() || feature.isMultiPolygon())
              name = olFeature.attributes.name || ''
              haRound = feature.getAreaInfo().haRound

              return "#{name}  #{haRound} ha";
            else
              return ""
        }
      })
    })

  _readTestGeoJson: () ->
    # /tmp/shp/test/janusz.shp
    {
      type: "MultiPolygon",
      coordinates: [[[[307693.69021772954, 341398.95970168355], [309218.33114566, 341414.3774189323], [309728.82889456255, 341344.1411514658], [309903.52070677, 341052.9688996309], [307032.44145572814, 340657.1961940499], [307693.69021772954, 341398.95970168355]]]]
    }

  class OSMapFactory
    defaultOpts: ->
      opts =
        center: new OpenLayers.LonLat(0, 0)
        numZoomLevels: 6
        units: "m"
        controls:[
          new OpenLayers.Control.Navigation(zoomWheelEnabled: false)
          new OpenLayers.Control.PanZoomBar()
          new OpenLayers.Control.KeyboardDefaults()
        ]

    constructor: (opts={}) ->
      throw Error("map div is not defined") unless opts.div
      @div = opts.div
      @mapOpts = $.extend({}, @defaultOpts())
      @_setUpProjections(opts.projections)
      @mapOpts.center = @_lonLat(opts.center) if opts.center
      @defaultZoom = opts.zoom

    _lonLat: (lonlat)-> new OpenLayers.LonLat(lonlat[0], lonlat[1]).transform(@mapOpts.displayProjection, @mapOpts.projection)

    _setUpProjections: (projections) ->
      @mapOpts.displayProjection = projections.proj4326
      @mapOpts.projection        = projections.proj3785

    create: -> @map = new OpenLayers.Map(@div, @mapOpts) #unless @map

  class WMSLayerFactory
    constructor: (@name, @url, @opts={}) ->
      @WMSOpts = $.extend(@defaultWMSOpts, @opts)
      @projections = @opts.projections
      delete @WMSOpts.layerOpts
      delete @WMSOpts.projections
      @layerOpts = $.extend(@defaultLayerOpts(), @opts.layerOpts)

    defaultLayerOpts: ->
      displayInLayerSwitcher: true
      maxExtent: @projections.bbox3785
      opacity: 0.6
      isBaseLayer: false
      projection: @projections.proj3785
      # displayProjection: @projections.proj900913
      units: "m"
      # tileOptions:
      #   eventListeners:
      #     'loaderror': (e) -> log 'loaderror'

    defaultWMSOpts:
      service: "WMS"
      request: "GetMap"
      version: "1.3.0"
      CRS:     "EPSG:3785"
      format:  "image/png"
      TRANSPARENT: "true"
      styles: ""

    create: -> @layer = new OpenLayers.Layer.WMS(@name, @url,  @WMSOpts, @layerOpts)