﻿class window.App.Modules.Pagination extends window.App.Modules.BaseModule
  @name: 'Pagination'
  @require: ->
    name: 'Pagination'
    libs: ['reqres']

  _registerEvents: -> @_reqres.setHandler("#{@name}-getHtml", @getHtml, @)

  _getHtmlError: (msg)-> @_error("#{msg} is not defined", '#getHtml')

  _templateName: 'helpers/pagination'

  getHtml: (opts={}) ->
    @_getHtmlError('page') unless opts.page
    @_getHtmlError('size') unless opts.size
    @_getHtmlError('total') unless opts.total?
    @_getHtmlError('pagingUrl') unless opts.url

    @_template = @_reqres.request('TemplateProvider-get', @_templateName) unless @_template
    pages = Math.ceil(opts.total / opts.size)
    min = opts.page - 2
    min = 1 if min < 1
    max = min + 4
    max = pages if max > pages
    prev = if opts.page > 1 then opts.page - 1 else null
    next = if opts.page < pages then opts.page + 1 else null
    pages = []
    (
      active = if i is opts.page then true else false
      pages.push({active: active, page: i})
    ) for i in [min..max]
    from = (opts.page - 1)  * opts.size
    from = 1 if from is 0
    to = from + opts.size
    to = to - 1 if from is 1
    to = opts.total if to > opts.total
    model =
      prev: prev
      next: next
      from: from
      to: to
      total: opts.total
      page: opts.page
      pages: pages
      url: opts.pagingUrl
    @_template(model)