class window.App.Modules.FieldCard extends window.App.Modules.BaseViewModule
  @name: 'FieldCard'
  @require: ->
    name: 'FieldCard'
    libs: ['commands', 'reqres', 'router']

  routes:
    'fis/:fis_id/fields/:id':  'showField'
    'fis/:fis_id/fields/draft/:ol_id': 'showDraft'
    'fis/:fis_id/fields/new':  'newField'
    'fis/:fis_id/fields':      'showFields'

  breadcrumbsButtons:
    add:
      icon: 'plus'
      label:'Dodaj pole'
      cb: -> $('#breadcrumbFieldCardAddMapBtn').click()
    cancel:
      icon: 'thumbs-down'
      label:'Anuluj'
      type: 'error'
      cb: -> $('#breadcrumbFieldCardCancelBtn').click()

  showFields: (fis_id) =>
    @_fetchCollection(fis_id)
    @_initRootModel()
    @_showBreadcrumbButtons([@breadcrumbsButtons.add])
    @_show(@_rootModel)
    @_rootModel.setMode('none')
    @_rootModel.setAction('none')

  newField: (fis_id) =>
    @showFields(fis_id)
    @_rootModel.addNewField()

  showField: (fis_id, id) =>
    @showFields(fis_id)
    @_rootModel.selectField(@collection.getById(id), selectOnMap: true)

  showDraft: (fis_id, ol_id) => @showFields(fis_id)

  _show: (rootModel) ->
    @_view = new _View(rootModel: rootModel, afterRender: @_afterRender)
    @_showView(@_view)
    @notifyManager = new Notify($rootEl: @_view.$el, divId: 'mapNotify')

  _afterRender: => @_rootModel.moduleRendered()

  _registerEvents: ->
  _initialize: ->
    @_url = @_getUrl('FieldsUrl')
    @_commands.execute('PageSidebar-addItem', @_sidebar())
    @_initDevTools()

  getCollection: -> @collection

  notify: (opts={}) ->
    @notifyManager.show(opts)
    $("[data-fieldcard-tooltip=tooltip]").tooltip()

  notifyDestroy: (name)-> @notifyManager.destroy(name)

  _initRootModel: ->
    @_rootModel = new _RootModel(module: @, router: @_router, url: @_url)

  _fetchCollection: (fis_id) ->
    collectionUrl = @_url.replace(':FIS_ID', fis_id)
    @collection = @_newCollection(collectionUrl)
    @collection.fetch()

  _fisId: -> @_reqres.request('Settings-get', 'fis_id')

  _sidebar: -> {icon:  "icon-puzzle-piece", title: "Karty pól", url: @_sidebarUrl() }
  _sidebarUrl: -> "#fis/#{@_fisId()}/fields"

  _newModel:             -> new (@_getModel('FieldCard'))()
  _newCollection: (url) -> new (@_getCollection('FieldCard'))([], url: url)

  _initDevTools: ->
    window._dev = {} unless window._dev
    window._dev.fc = @

  class Notify
    divId: 'notify'
    template: (html, type, name='default') -> "<div id='#{@divId}' class='#{type}' data-name='#{name}'><div class='notify-close'></div><div class='notify-content'>#{html}</div></div>"
    currentNotify: null
    constructor: (opts={}) ->
      @$rootEl  = opts.$rootEl  if opts.$rootEl
      @divId    = opts.divId    if opts.divId
      @template = opts.template if opts.template

    show: (opts={}) ->
      @destroy() if @currentNotify?
      @currentNotify = new _NotifyItem(@_prepareParams(opts))
      @currentNotify.showIn(@$rootEl)

    destroy: (name) ->
      if @currentNotify?
        if name?
          @_destroy() if @currentNotify.name is name
        else
         @_destroy()

    _destroy: ->
      @currentNotify.destroy()
      delete @currentNotify

    _prepareParams: (opts={}) ->
      source = opts.html || opts.text || ''
      type   = opts.type || 'default'
      name   = opts.name || 'default'

      body: @template(source, type, name)
      time: opts.time
      cbfn: opts.cbfn
      $el:  @$rootEl
      name: name

    class _NotifyItem
      $body: null
      cbfn:  null
      time:  5000

      constructor: (opts={}) ->
        @$body = $(opts.body)
        @name  = opts.name || 'default'
        @cbfn  = opts.cbfn if typeof opts.cbfn is 'function'
        @time  = opts.time if opts.time?

      showIn: ($el) ->
        $el.append(@$body)
        @$body.find('.notify-close').click( => @destroy())
        @$body.show().delay(@time).fadeOut("slow", => @destroy()) if @time

      destroy: ->
        @cbfn() if @cbfn
        @$body.remove()
        delete @cbfn

  class _RootModel extends Backbone.Model
    _wasMapInitialized: false
    _moduleRendered: false

    initialize: (opts={}) ->
      @_url   = opts.url
      @module = opts.module
      @router = opts.router
      @set('currentModel', @newFieldCardModel())
      @module._commands.setHandler('Map-drawShapeDone',   @handlerDrawingActionDone, @)
      @module._commands.setHandler('Map-FeatureUpdated',  @handlerUpdatingShapeActionDone, @)
      @module._commands.setHandler('Map-lonLatFromClick', @handlerGetLonLatFromMapDone, @)
      @module._commands.setHandler('Map-fieldSelected',   @handlerFieldFeatureSelected, @)

      @_generateFeaturesForEveryModel()

      @swapState(@_startState())

    _fisId: -> @module._fisId()

    collection: -> @module.getCollection()

    newFieldCardModel: -> @module._newModel()
    currentModel: -> @get('currentModel')

    features: -> _.compact(_.map(@collection().models, (m) -> m.getFeature()))

    defaults:
      state:      'intro' #intro, map, list
      mode:       'none' #none, info, new, edit
      action:     'none'
      waitingFor: 'none'
      sidebar: false
      ortofotoLayer: false
      parcelsLayer: false
      current_model_id: -1 #to check model switching

    swapState: (newState) ->
      @module._commands.execute('Settings-set', 'last_map_state_on_fields', newState)
      @set('state', newState)
      @_afterSwapState()

    _afterSwapState: ->
      @_initMap()
      @refreshMap()

    _startState: -> if @shouldShowIntro() then 'map' else @module._reqres.request('Settings-get', 'last_map_state_on_fields') || @state()

    isMapState:   -> @state() == 'map'
    isListState:  -> @state() == 'list'
    isIntroState: -> @state() == 'intro'
    state: -> @get('state')

    canSwitchToListView: -> @collection().models.length > 0

    swapSidebar: (newState) ->
      @set('sidebar', newState)
      @refreshMap() is newState == false

    addNewField: ->
      @_navigateToNewField()
      newField = @newFieldCardModel()
      newField.set('id', @collection().getNextNewFieldId())
      @collection().add(newField)
      @set('currentModel', newField)
      @setMode('new')

    selectField: (field, opts={}) ->
      return unless field
      @_navigateToField(field)
      @module._reqres.request('Map-selectField', field) if opts.selectOnMap and @_wasMapInitialized
      @set('currentModel', field)
      @setAction('drawn')
      @setMode('edit')

    cancelActionByBreadcrumb: ->
      @cancelAction()
      @setAction('none')
      @setMode('none')

    cancelAction: ->
      switch @get('action')
        when 'none'      then @cancelMode()
        when 'drawing'   then @cancelActionDrawing()
        when 'union'     then @cancelActionUnion()
        when 'exclusion' then @cancelActionExclusion()
        when 'drawn'     then @cancelMode()
        else null
      @cancelWaiting()

    cancelActionDrawing: ->
      @module._commands.execute('Map-setHandMode', '')
      @_deleteCurrentModel()
      @set('action', 'none')

    cancelActionUnion: ->
      @module._commands.execute('Map-setHandMode', '')
      @set('action', 'drawn')

    cancelActionExclusion: ->
      @module._commands.execute('Map-setHandMode', '')
      @set('action', 'drawn')

    cancelMode: ->
      @_navigateToFields()
      @_unselectFieldOnMap()
      switch @get('mode')
        when 'new'
          @swapSidebar(false)
          @setMode('none')
        when 'edit' then @setMode('info')
        when 'info'
          @swapSidebar(false)
          @setMode('none')
      @set('action', 'none')

    cancelWaiting: ->
      @module._commands.execute('Map-setListenForLonLat', false)
      $('#map').css('cursor','default')
      @set('waitingFor', 'none')

    addParcelIntoCollection:    (parcelModel) -> @currentModel().parcels.add(parcelModel)
    removeParcelFromCollection: (parcelModel) -> @currentModel().parcels.remove(parcelModel)

    _calculateGrowingAreaForParcels: (parcels) ->
      # When all pacels are added auto
      currentFeature = @currentModel().getFeature()
      parcelsFeatures = _.map(parcels, (p) => p.getFeature())
      result = @module._reqres.request('Geometry-getCommonArea', currentFeature, parcelsFeatures)
      _.each(result, (r, idx)=>
        if r.areaInfo
          parcels[idx].set({area_used: r.areaInfo.haRound, display_area_used: r.areaInfo.haRound })
        else
          parcels[idx].destroyFeature()
          parcels[idx] = null
      )

    _unselectFieldOnMap: -> @module._reqres.request('Map-unselectSelectedShape')

    moduleRendered: ->
      @_moduleRendered = true
      @_initMap()

    handlerDrawingActionDone: (feature)->
      @currentModel().updateFeature(feature)
      $('#map').css('cursor','wait')
      @notify(text: @msg.loadingParcels)
      setTimeout (=>
        @onFeatureCreated(feature)
      ), 300
      @_navigateToField(@currentModel())
      @setAction('drawn')
      @setMode('edit')

    onFeatureCreated: (feature) ->
      @currentModel().createFeature(feature)
      unless @currentModel().parcels.length
        @updateParcelsByFeature(feature)
      @setAction('drawn')
      $('#map').css('cursor','default')

    updateParcelsByFeature: (feature) ->
      @notify(text: @msg.waitForParcels)
      parcels = @requestReadParcels(feature)
      parcels.forEach((m)=> @requestAddParcelShape(m.get('feature'), {name: m.get('number')}))
      @_calculateGrowingAreaForParcels(parcels)
      parcels = _.compact(parcels)
      setTimeout((=> @cmdFadeOutParcels(parcels)),1200)
      @currentModel().parcels.models = []
      @currentModel().parcels.add(parcels)

    handlerUpdatingShapeActionDone: (feature)->
      if @currentModel().getFeature().hasEmptyGeo() then @clearShape()
      @currentModel().updateFeature(feature)

    handlerGetLonLatFromMapDone: (lonLat) ->
      return if @get('waitingFor') is 'none'
      $('#map').css('cursor','wait') #.delay(300).queue(=> @onWaitingFor(lonLat))
      @notify(text: @msg.loading, type: 'loading', time: 0, name: 'loading-lonlat')
      setTimeout (=>
        @onWaitingFor(lonLat)
      ), 300

    handlerFieldFeatureSelected: (feature) ->
      field = @collection().getById(feature.id())
      field = @collection().getByOlId(feature.olId()) unless field
      @selectField(field)

    notify:        (opts) -> @module.notify(opts)
    notifyDestroy: (name) -> @module.notifyDestroy(name)

    _initMap: =>
      if (not @_wasMapInitialized) && @_moduleRendered && (@isMapState() || @isIntroState())
        @module._reqres.request('Map-init', {div: "map", center: [19.0258159, 52.0977181], zoom: 10, afterLoaded: @_afterMapLoaded})

    _afterMapLoaded: =>
      @_wasMapInitialized = true
      @module._reqres.request('Map-multiAddFields', @features(), allowInvalid: true)
      @selectField(@currentModel(), selectOnMap: true) if @currentModel() and @currentModel().persisted()

    refreshMap: -> if @_wasMapInitialized && @isMapState() then @module._commands.execute('Map-refreshMap')

    _navigateToField: (field) ->
      if field.persisted()
        @router.silentNavigate(@_fieldUrl(field.id))
      else
        @_navigateToDraft(field)

    _navigateToFields:        -> @router.silentNavigate(@_fieldsUrl())
    _navigateToDraft: (field) -> @router.silentNavigate(@_fieldDraftUrl(field.id))
    _navigateToNewField:      -> @router.silentNavigate(@_newFieldUrl())

    _saveUpdateServerUrl:  -> @_url.replace(':FIS_ID', @_fisId())
    _deleteServerUrl: (id) -> "#{@_url.replace(':FIS_ID', @_fisId())}/#{id}"

    _fieldsUrl:          -> @_fullUrl()
    _newFieldUrl:        -> @_fullUrl('new')
    _fieldUrl:      (id) -> @_fullUrl(id)
    _fieldDraftUrl: (id) -> @_fullUrl("draft/#{id}")
    _fullUrl:     (rest) -> if rest? then "#fis/#{@_fisId()}/fields/#{rest}" else "#fis/#{@_fisId()}/fields"

    #TODO notifications
    toggleWMS: (layerName) ->
      switch layerName
        when 'ortofoto'
          ortofotoLayer = not !!@get('ortofotoLayer')
          @module._commands.execute('Map-setLayerVisibility', 'ortofoto', ortofotoLayer)
          @notify(text: @msg.wmsZoomIsToSmall('zdjęcia lotnicze', 15)) if ortofotoLayer && @module._reqres.request('Map-getZoom') < 15
          @set('ortofotoLayer', ortofotoLayer)
        when 'parcels'
          parcelsLayer = not !!@get('parcelsLayer')
          @module._commands.execute('Map-setLayerVisibility', 'dzkat', parcelsLayer)
          @notify(text: @msg.wmsZoomIsToSmall('działki ewidencyjne', 16)) if parcelsLayer && @module._reqres.request('Map-getZoom') < 16
          @set('parcelsLayer', parcelsLayer)
        when 'osm'
          osmLayer = not !!@get('osmLayer')
          @module._commands.execute('Map-setLayerVisibility', 'osm', osmLayer)
          @set('osmLayer', osmLayer)

    readParcelFromLonLat: (lonLat) ->
      parcels = @requestReadParcels(lonLat)
      if parcels.length then parcels[0] else null

    onWaitingFor: (obj) ->
      switch @get('waitingFor')
        when 'whatIsHere'   then @onWaitingForWhatIsHere(obj)
        when 'createParcel' then @onWaitingForCreateParcel(obj)
        when 'addParcel'    then @onWaitingForAddParcel(obj)
        when 'removeParcel' then @onWaitingForRemoveParcel(obj)
      @set('waitingFor', 'none')
      @notifyDestroy('loading-lonlat')

    onWhatIsHereClosed: => @module._commands.execute('Map-clearPreviewsLayer')

    onWaitingForCreateParcel: (lonLat) ->
      parcel = @readParcelFromLonLat(lonLat)
      @module._reqres.request('Map-addFieldShape', parcel.getFeature()) if parcel

      $('#map').css('cursor','default')
      @set('action', 'drawn')

    onWaitingForAddParcel: (lonLat) ->
      parcel = @readParcelFromLonLat(lonLat)
      @module._reqres.request('Map-unionToSelectedShape', parcel.getFeature()) if parcel

      $('#map').css('cursor','default')
      @set('action', 'drawn')

    onWaitingForRemoveParcel: (lonLat) ->
      parcel = @readParcelFromLonLat(lonLat)
      @module._reqres.request('Map-exclusionToSelectedShape', parcel.getFeature()) if parcel

      $('#map').css('cursor','default')
      @set('action', 'drawn')

    onWaitingForWhatIsHere: (lonLat) ->
      parcel = @readParcelFromLonLat(lonLat)
      if parcel
        @module._reqres.request('Map-addPreviewShape', parcel.getFeature(), {name: parcel.attrNumber()})
        @parcelNotifiInfo(parcel, lonLat: lonLat, cbfn:  @onWhatIsHereClosed)
      $('#map').css('cursor','default')

    parcelNotifiInfo: (parcel, opts={}) ->
      @notifyParcelInfoTmpl = @module._reqres.request('TemplateProvider-get', 'field_card/notify_parcel_info') unless @notifyParcelInfoTmpl
      if opts.lonLat
        lonLat = opts.lonLat
      else
        if parcel?
          feature = parcel.getFeature()
          if feature? && typeof feature is 'object'
            point  = feature.firstOlPoint()
            lonLat = @module._reqres.request('Geometry-buildLonLat', [point.x, point.y], 900913)
      if lonLat?
        WeatherUrls = @module._reqres.request("Weather-getUrls", new Date(), lonLat.getOl())# display in new window
        weather = [
          {
            url: WeatherUrls.coamps
            name: 'Pogoda: 13km/84h'
            height: 666
            width: 965
          },
          {
            url: WeatherUrls.um
            name: 'Pogoda: 4km/60h'
            height: 782
            width: 842
          }
        ]

      placeInfo   = if parcel?
        parcel.location()
      else
        if lonLat?
          display: @module._reqres.request("SearchLocation-whatPlaceIsHere", lonLat.getOl()).full_name

      data =
        location: placeInfo
        parcel: if parcel? then parcel.getJsonForInfo() else null
        weather: weather

      @notify(html: @notifyParcelInfoTmpl(data), type: 'info', cbfn: opts.cbfn, time: 0)

    msg:
      loading: 'Proszę czekać trwa ładowane danych'
      loadingParcels: 'Proszę czekać trwa próba pobrania działek ewidencyjnych'
      whatIsHereBegin: 'kliknij miejsce na mapie, by pokazać więcej informacji o nim'
      creatingParcelBegin: 'kliknij aby narysować pole na podstawie działki ewidencyjnej' # może być z linkiem do anulowania akcji
      addingParcelBegin: 'kliknij aby dorysować dziłkę ewidencyjną do pola'
      removingParcelBegin: 'kliknij aby odiąć działkę ewidencyjną od pola'
      drawingAction: 'kliknij na mapie aby rozpocząc rysowanie pola'
      unionAction:  'kliknij na mapie aby rospocząć rysowanie kształtu który zostanie dodany do pola.'
      exclusionAction: 'kliknij na mapie aby rozpocząć rysowanie kształtu który zostanie odjęty od pola'
      fieldSaveSuccess: 'Zapisano dane pola'
      fieldSaveFail: 'Nie udało się zapisać pola'
      fieldDeleteSuccess: 'Pole zostało usunięte'
      fieldDeleteFail: 'Nie udało się usunąć pola'
      wmsZoomIsToSmall: (mapName, zoom)-> "Aby zobaczyć #{mapName} na mapie musisz ją powiększyć. <a href='javascript:void(0)' data-zoom='#{zoom}' class='js-zoom-now'>Chcesz to zrobić teraz naciśnij tutaj</a>"
      waitForParcels: 'proszę czekać trwa próba załadowania działek ewidencyjnych'

    setWaitingFor: (name) ->
      return @cancelWaiting() if @get('waitingFor') is name
      @cancelWaiting() if @get('waitingFor') isnt 'none'
      switch name
        when 'whatIsHere'
          @notify(text: @msg.whatIsHereBegin)
          @module._commands.execute('Map-setListenForLonLat', true)
          $('#map').css('cursor','crosshair')
        when 'createParcel'
          @notify(text: @msg.creatingParcelBegin)
          @module._commands.execute('Map-setListenForLonLat', true)
          $('#map').css('cursor','crosshair')
        when 'addParcel'
          @notify(text: @msg.addingParcelBegin)
          @module._commands.execute('Map-setListenForLonLat', true)
          $('#map').css('cursor','crosshair')
        when 'removeParcel'
          @notify(text: @msg.removingParcelBegin)
          @module._commands.execute('Map-setListenForLonLat', true)
          $('#map').css('cursor','crosshair')

      @set('waitingFor', name)

    clearShape: ->
      @module._reqres.request('Map-clearSelectedShape')
      @currentModel().updateFeature(@currentModel().getFeature()) #update with cleared geo
      @currentModel().parcels.reset()

    setAction: (name) ->
      @cancelWaiting()
      switch name
        when 'drawing'   then @_inActionDrawing()
        when 'union'     then @_inActionUnion()
        when 'exclusion' then @_inActionExclusion()
      @set('action', name)

    _inActionDrawing: ->
      @notify(text: @msg.drawingAction)
      name = if @currentModel() then @currentModel().get('name') else "Pole #{@collection().length}"
      @module._commands.execute('Map-startDrawingShape', name)
    _inActionUnion: ->
      @notify(text: @msg.unionAction)
      @module._commands.execute('Map-startDrawingUnionShape')
    _inActionExclusion: ->
      @notify(text: @msg.exclusionAction)
      @module._commands.execute('Map-startDrawingExclusionShape')

    setMode: (name) ->
      @cancelWaiting()
      switch name
        when 'new'  then @_inNewMode()
        when 'info' then @_inInfoMode()
        when 'edit' then @_inEditMode()
        when 'none' then @_inNoneMode()
      @set('mode', name)

    _inNewMode: ->
      @swapSidebar(true)
      @module._showBreadcrumb([{url: @_fieldsUrl(), title: "Wszystkie Pola"}, {title: 'Nowe pole'}])
      @module._showBreadcrumbButtons([@module.breadcrumbsButtons.cancel])

    _inInfoMode: ->
      @swapSidebar(true)
      @module._showBreadcrumb([{title: "Wszystkie Pola"}])
      @module._showBreadcrumbButtons([@module.breadcrumbsButtons.cancel])

    _inEditMode: ->
      @swapSidebar(true)
      @module._showBreadcrumb([{url: @_fieldsUrl(), title: "Wszystkie Pola"}, {title: 'Edycja'}])
      @module._showBreadcrumbButtons([@module.breadcrumbsButtons.cancel])

    _inNoneMode: ->
      @swapSidebar(false)
      @module._showBreadcrumb([{title: "Wszystkie Pola"}])
      @module._showBreadcrumbButtons([@module.breadcrumbsButtons.add])

    requestAddParcelShape: (feature, opts={}) -> @module._reqres.request('Map-addParcelShape', feature, opts) if feature
    requestReadParcels: (obj) -> @module._reqres.request('Parcels-readParcels', obj.toBoundsParcel())

    cmdFadeOutParcels: (parcels)-> _.each(parcels, (p) -> p.featureFadeOut())

    commandZoomTo: (zoom) -> @module._commands.execute('Map-zoomTo', zoom)
    commandGoToLocationOnMap: (lonLat) ->
        @module._commands.execute('Map-panTo', lonLat)
        @commandZoomTo(15)

    commandShowMyLocationOnMap: ->
      #should init searching overlay with information to accept location request
      @module._commands.execute('Map-showMeOnMap')
    locationSearchItemTmpl:   (elem) -> "<li data-lonlat=\"#{elem.lonLat}\"><i class='#{elem.type}'></i> #{elem.name}</li>"
    requestLocationSearchResults: (location) ->
      location = $.trim(location)
      return null unless location.length
      @module._reqres.request("SearchLocation-search", location)

    shouldShowIntro: -> @collection().length == 0

    _generateFeaturesForEveryModel: ->
      _.each(@collection().models,
        (m) =>
          feature = @module._reqres.request('Geometry-featureFromGeoJson', m.get('geo_json'))
          if feature
            m.createFeature(feature)
            feature.addToAttributes({name: m.get('name'), id: m.get('id')})
      )

    saveCurrentModel: -> if @currentModel().persisted() then @_updateCurrentModel() else @_saveNewModel()
    deleteCurrentModel: ->
      return @_deleteCurrentModel() unless @currentModel().persisted()

      @currentModel().url = @_deleteServerUrl(@currentModel().id)
      @currentModel().destroy(method: 'DELETE', wait: true).done(
        (res) =>
          if res.json_response.status == 'OK'
            @_deleteCurrentModel()
          else
            text = res.json_response.errors[0] || @msg.fieldDeleteFail
            @module.notify(text: text, type: 'error')
      )

    _deleteCurrentModel: ->
      @module._reqres.request('Map-removeSelectedShape')
      toDelete = if @currentModel().persisted() then @collection().getById(@currentModel().id) else @collection().getByOlId(@currentModel().olId())
      toDelete = toDelete || @currentModel()
      @collection().remove(toDelete)
      @module.notify(text: @msg.fieldDeleteSuccess)
      @_navigateToFields()
      @setMode('none')
      @setAction('none')

    _saveNewModel: ->
      @currentModel().url = @_saveUpdateServerUrl()
      @currentModel().save([], method:'POST').done(
        (res)=>
          if res.json_response.status == 'OK'
            @currentModel().updateId(res.json_response.data.id)
            @module.notify(text: @msg.fieldSaveSuccess)
            @_navigateToField(@currentModel())
          else
            text = res.json_response.errors[0] || @msg.fieldSaveFail
            @module.notify(text: text, type: 'error')
      )

    _updateCurrentModel: ->
      @currentModel().url = "#{@_saveUpdateServerUrl()}/#{@currentModel().id}"
      @currentModel().save([], method:'PUT').done(
        (res) =>
          if res.json_response.status == 'OK'
            @module.notify(text: @msg.fieldSaveSuccess)
          else
            text = res.json_response.errors[0] || @msg.fieldSaveFail
            @module.notify(text: text, type: 'error')
      )

  class _BaseView extends Backbone.View
    _getTemplateSource: (name) -> App.reqres.request('TemplateProvider-get', name)

    initialize: (opts={}) ->
      @rootModel = opts.rootModel if opts.rootModel
      @template = @_getTemplateSource(@_templateName) if @_templateName
      @_initialize(opts) if @_initialize

  class _View extends _BaseView
    _templateName: 'field_card/field_card'
    pageContentCssClass: 'page-content-no-padding'
    title: null
    _initialize: (opts={})->
      @_contentView = new _ContentView(rootModel: @rootModel)
      @_sidebarView = new _SideBarView(rootModel: @rootModel)
      @_introView   = new _IntroView(rootModel: @rootModel)
      @rootModel.on('change:state', @_onStateChanged, @)
      @rootModel.on('change:sidebar', @_onSidebarChanged, @)
      @rootModel.on('change:mode', @_onModeChanged, @)
      @rootModel.collection().on('change', @_onCollectionChanged, @)
      @rootModel.collection().on('remove', @_onCollectionChanged, @)

      @afterRender  = opts.afterRender

    remove: ->
      @_introView.remove()
      @_contentView.remove()
      @_sidebarView.remove()
      delete @_introView
      delete @_contentView
      delete @_sidebarView
      super

    events:
      'click #fcShowMapView':    'onClickBtnShowMapView'
      'click #fcShowListView':   'onClickBtnShowListView'
      'click #fcFullSizeView':   'onClickBtnFullSizeView'
      'click #fcNormalSizeView': 'onClickBtnNormalSizeView'
      'click .js-zoom-now':      'onClickBtnZoomNow'
      'click #breadcrumbFieldCardAddMapBtn': 'onClickBtnBreadcrumbAddField'
      'click #breadcrumbFieldCardCancelBtn': 'onClickBtnBreadcrumbCancel'
      'click #mapOpenStreetLayer': 'onClickBtnOpenStreetLayer'
      'click #mapOrtofotoLayer':   'onClickBtnShowOrtofotoLayer'
      'click #mapParcelsLayer':    'onClickBtnShowParcelsLayer'

    onClickBtnZoomNow:     (e)-> @rootModel.commandZoomTo(parseInt($(e.currentTarget).data('zoom')))
    onClickBtnShowMapView:    -> @rootModel.swapState('map')
    onClickBtnShowListView:   -> @rootModel.swapState('list')
    onClickBtnFullSizeView:   -> @rootModel.swapSidebar(false)
    onClickBtnNormalSizeView: -> @rootModel.swapSidebar(true)
    onClickBtnBreadcrumbAddField: -> @rootModel.addNewField()
    onClickBtnBreadcrumbCancel:   -> @rootModel.cancelActionByBreadcrumb()

    onClickBtnOpenStreetLayer:   -> @rootModel.toggleWMS('osm')
    onClickBtnShowOrtofotoLayer: -> @rootModel.toggleWMS('ortofoto')
    onClickBtnShowParcelsLayer:  -> @rootModel.toggleWMS('parcels')

    _onModeChanged: ->
      switch @rootModel.get('mode')
        when 'none'  then @_hideSidebarButtons()
        else @_onSidebarChanged()

    _hideSidebarButtons: ->
      @$el.find('#fcFullSizeView').addClass('hidden')
      @$el.find('#fcNormalSizeView').addClass('hidden')

    _onSidebarChanged: ->
      if @rootModel.get('sidebar')
        @$el.find('#fcFullSizeView').removeClass('hidden')
        @$el.find('#fcNormalSizeView').addClass('hidden').closest('.sidebar-controls').removeClass('sidebar-controls-folded')
        @rootModel.refreshMap()
      else
        @$el.find('#fcNormalSizeView').removeClass('hidden')
        @$el.find('#fcFullSizeView').addClass('hidden').closest('.sidebar-controls').addClass('sidebar-controls-folded')

    _onStateChanged: ->
      switch @rootModel.state()
        when 'map'  then @_onMapState()
        when 'list' then @_onListState()

    _onMapState: ->
      @$el.find('#fcShowListView').removeClass('hidden') if @rootModel.canSwitchToListView()
      @$el.find('#fcShowMapView').addClass('hidden')

    _onListState: ->
      @$el.find('#fcShowMapView').removeClass('hidden')
      @$el.find('#fcShowListView').addClass('hidden')

    _onCollectionChanged: ->
      if @rootModel.isMapState()
        @$el.find('#fcShowListView').removeClass('hidden') if @rootModel.canSwitchToListView()
        @$el.find('#fcShowListView').addClass('hidden') unless @rootModel.canSwitchToListView()

    _adjustView: ->
      @_onSidebarChanged()
      @_onStateChanged()
      @_onModeChanged()

    render: ->
      @$el.html @template({})
      @$el.append @_contentView.render().el
      @$el.append @_sidebarView.render().el
      @$el.append @_introView.render().el if @rootModel.shouldShowIntro()
      @_adjustView()
      @

    class _IntroView extends _BaseView
      _templateName: 'field_card/intro'
      _videoId: 'RLRS9C_YheA' #'68-fWlzwQpo'

      events:
        'click #locationSearch':        'searchLocation'
        'keypress #locationSearchText': 'searchLocationText'
        'click #showMyLocationOnMap':   'showMyLocationOnMap'

      render: ->
        @$el.html @template({video_id: @_videoId})
        @

      showMyLocationOnMap: (e) ->
        @rootModel.commandShowMyLocationOnMap()
        @_close()

      searchLocationText: (e) -> @searchLocation() if (e.which is 13)

      searchLocation: (e) ->
        $textBox = @$el.find('#locationSearchText')
        @_showSearchResults(@rootModel.requestLocationSearchResults($textBox.val()))
        $textBox.val('')

      _showSearchResults:(data) ->
        if data && data.length
          html = "<ul class='field-card-location-search-results'>" + _.map(data, (i)=> @rootModel.locationSearchItemTmpl(i)).join('') + "</ul>"
        else
          html = "<h3 class='red'><i class='icon-exclamation-circle'></i> Nie odnaleziono lokalizacji. spróbuj innej. </h3>"

        $('.js-search-results').html(html)
        $('.js-search-results li').on('click', (e) =>
          lonLat = $(e.target).data('lonlat').split(',')
          @rootModel.commandGoToLocationOnMap(lonLat)
          @rootModel.swapState('map')
          @_close()
        )

      _close: ->
        @rootModel.swapState('map')
        @remove()

    class _ContentView extends _BaseView
      _wasMapViewInitialized: false
      _wasListViewInitialized: false

      _initialize: (opts={}) ->
        @rootModel.on('change:state', @_onStateChanged, @)

      remove: ->
        @_mapView.remove() if @_wasMapInitialized
        @_listView.remove() if @_wasListViewInitialized
        delete @_mapView
        delete @_listView
        super

      render: ->
        @$el.html ''
        @_onStateChanged()
        @

      _onStateChanged: ->
        switch @rootModel.state()
          when 'map'   then @_initMapView()
          when 'list'  then @_initListView()
          when 'intro' then @_initMapView()

      _initMapView: ->
        unless @_wasMapViewInitialized
          @_mapView = new _MapView(rootModel: @rootModel, model: @rootModel.currentModel())
          @$el.append @_mapView.render().el
          @_wasMapViewInitialized = true
        @_mapView._onModeChanged()

      _initListView: ->
        unless @_wasListViewInitialized
          @_listView = new _ListView(rootModel: @rootModel)
          @$el.append @_listView.render().el
          @_wasListViewInitialized = true

      class _MapView extends _BaseView
        _templateName: 'field_card/map'

        _initialize: ->
          @rootModel.on('change:state', @_onStateChanged, @)
          @rootModel.on('change:mode', @_onModeChanged, @)
          @rootModel.on('change:sidebar', @_onSidebarChanged, @)
          @rootModel.on('change:action', @_onActionChanged, @)
          @rootModel.on('change:waitingFor', @_onWaitingForChanged, @)
          @rootModel.on('change:ortofotoLayer', @_onOrtofotoLayerChanged, @)
          @rootModel.on('change:parcelsLayer', @_onParcelsLayerChanged, @)
          @rootModel.on('change:currentModel', @_onCurrentModelChange, @)

        events:
          'click #mapAddNewField':    'onClickBtnNewField'
          'click #mapReadFromFile':   'onClickBtnReadFromFile'
          'click #mapDrawField':      'onClickBtnDrawField'
          'click #mapCancelAction':   'onClickBtnCancelAction'
          'click #mapExclusionShape': 'onClickBtnDrawExclusionShape'
          'click #mapUnionShape':     'onClickBtnDrawUnionShape'
          'click #mapClearShape':     'onClickBtnClearShape'
          'click #mapCreateParcel':   'onClickBtnCreateParcel'
          'click #mapAddParcel':      'onClickBtnAddParcel'
          'click #mapRemoveParcel':   'onClickBtnRemoveParcel'
          'click #mapWhatIsHere':     'onClickBtnWhatIsHere'
          'click #mapFindLocation':   'onClickBtnFindLocation'
          'click #mapLocationSearch':        'onClickBtnSearchLocation'
          'keypress #mapLocationSearchText': 'onkeypressTxtSearchLocationText'
          'click #showMyLocationOnMap':   'showMyLocationOnMap'

        showMyLocationOnMap: (e) -> @rootModel.commandShowMyLocationOnMap()
        onClickBtnNewField:     -> @rootModel.addNewField()
        onClickBtnCancelAction: -> @rootModel.cancelAction()
        onClickBtnDrawField:    -> @rootModel.setAction('drawing')
        onClickBtnClearShape:   -> @rootModel.clearShape()
        onClickBtnDrawUnionShape:     -> @rootModel.setAction('union')
        onClickBtnDrawExclusionShape: -> @rootModel.setAction('exclusion')
        onClickBtnCreateParcel:  -> @rootModel.setWaitingFor('createParcel')
        onClickBtnAddParcel:     -> @rootModel.setWaitingFor('addParcel')
        onClickBtnRemoveParcel:  -> @rootModel.setWaitingFor('removeParcel')
        onClickBtnWhatIsHere:    -> @rootModel.setWaitingFor('whatIsHere')
        onClickBtnFindLocation:  -> @rootModel.setWaitingFor('findLocation')
        onkeypressTxtSearchLocationText: (e) -> @_searchLocation() if (e.which is 13)
        onClickBtnSearchLocation:    -> @_searchLocation()
        onClickBtnReadFromFile:      -> @rootModel.setWaitingFor('loadFromFile')

        onClickBtnUploadParcelXmlFile: (e) ->
          e.preventDefault()
          $fileField = $('#uploadParcelFiles')
          $fileField.fileupload(
            dataType: 'json'
            done: (e, data) ->
              if data.result.json_response.status == 'OK'
                _.each(data.result.json_response.data, (d) =>
                  geo_json = d.shape.geo_json
                  projection = d.shape.info.projection
                  opts = {
                    projection: projection,
                    simplifyShape: true,
                    fixShape: true,
                    allowInvalid: true,
                    panToShape: true
                  }

                  feature = App.reqres.request('Map-addShapeFromGeoJson', geo_json, opts)
                )

              else
                log data.result.json_response.errors
            )
          $fileField.trigger('click')
          # @rootModel.module.notifyDestroy()

        onClickBtnUploadParcelShpFile: (e) ->
          e.preventDefault()
          $fileField = $('#uploadParcelFiles')
          $fileField.fileupload(
            dataType: 'json'
            done: (e, data) -> log [e, data.result]
          )
          $fileField.trigger('click')
          # @rootModel.module.notifyDestroy()

        _searchLocation: ->
          $textBox = @$el.find('#mapLocationSearchText')
          @_showSearchLocationResults(@rootModel.requestLocationSearchResults($textBox.val()))
          $textBox.val('')
          @rootModel.setWaitingFor('none')

        _showSearchLocationResults: (data) ->
          html = "<h4>Wyniki wyszukiwania</h4>"
          if data && data.length
            html += "<ul class='field-card-location-search-results'>" + _.map(data, (i)=> @rootModel.locationSearchItemTmpl(i)).join('') + "</ul>"
            type = 'info'
          else
            html += "<h5> Nie odnaleziono lokalizacji. spróbuj innej. </h3>"
            type = 'error'
          @rootModel.module.notify(html: html, type: type, time: 0)

          $("##{@rootModel.module.notifyManager.divId} li").on('click', (e) =>
            lonLat = $(e.target).data('lonlat').split(',')
            @rootModel.commandGoToLocationOnMap(lonLat)
            @rootModel.module.notifyDestroy()
          )

        _showUploadFileNotifyForm: ->
          url  = "api/files/upload_shape"
          html = '<h4 class="upload-files-header">Wczytaj plik pola z formatu</h4>'
          html += '<button id="uploadParcelXmlFile" class="btn btn-success btn-upload">XML</button><button id="uploadParcelShpFile" class="btn btn-warning btn-upload">SHP</button>'
          html += "<input id='uploadParcelFiles' type='file' name='files[]' data-url='#{url}' multiple class='hidden'>"

          @rootModel.module.notify(html: html, type: 'file-upload', time: 0)
          @$el.find('#mapReadFromFile').removeClass('active-wait-for')
          $('#uploadParcelXmlFile').click(@onClickBtnUploadParcelXmlFile)
          $('#uploadParcelShpFile').click(@onClickBtnUploadParcelShpFile)

        _onOrtofotoLayerChanged: ->
          $btn = @$el.find('#mapOrtofotoLayer')
          if @rootModel.get('ortofotoLayer') then $btn.addClass('active-wms') else $btn.removeClass('active-wms')
        _onParcelsLayerChanged:  ->
          $btn = @$el.find('#mapParcelsLayer')
          if @rootModel.get('parcelsLayer')  then $btn.addClass('active-wms') else $btn.removeClass('active-wms')

        _onWaitingForChanged: ->
          @$el.find('.active-wait-for').removeClass('active-wait-for')
          switch @rootModel.get('waitingFor')
            when 'createParcel' then @$el.find('#mapCreateParcel').addClass('active-wait-for')
            when 'addParcel'    then @$el.find('#mapAddParcel').addClass('active-wait-for')
            when 'removeParcel' then @$el.find('#mapRemoveParcel').addClass('active-wait-for')
            when 'whatIsHere'   then @$el.find('#mapWhatIsHere').addClass('active-wait-for')
            when 'loadFromFile'
              @$el.find('#mapReadFromFile').addClass('active-wait-for')
              @_showUploadFileNotifyForm()
            when 'findLocation'
              $btn = @$el.find('#mapFindLocation')
              $btn.addClass('active-wait-for')
              $searchCtrlContainer = $btn.closest('.btn-group').find('#searchControlsGroup')
              $searchCtrlContainer.addClass('active-wait-for')
              $searchCtrlContainer.find('#mapLocationSearchText').focus()

        _onActionChanged: ->
          switch @rootModel.get('action')
            when 'none'      then @_inActionNone()
            when 'drawing'   then @_inActionDrawing()
            when 'drawn'     then @_inActionDrawn()
            when 'union'     then @_inActionUnion()
            when 'exclusion' then @_inActionExclusion()
        _inActionNone:      -> @_onModeChanged()
        _inActionDrawing:   -> @_buttonsShowOnly(['CancelAction'])
        _inActionDrawn:     -> @_buttonsShowOnly(['AddParcel', 'RemoveParcel', 'ExclusionShape', 'UnionShape', 'ClearShape'])
        _inActionUnion:     -> @_buttonsShowOnly(['AddParcel', 'RemoveParcel', 'ExclusionShape', 'UnionShape', 'ClearShape', 'CancelAction'])
        _inActionExclusion: -> @_buttonsShowOnly(['AddParcel', 'RemoveParcel', 'ExclusionShape', 'UnionShape', 'ClearShape', 'CancelAction'])
        _onModeChanged: ->
          switch @rootModel.get('mode')
            when 'new'  then @_inNewMode()
            when 'info' then @_inInfoMode()
            when 'edit' then @_inEditMode()
            when 'none' then @_inNoneMode()

        _inNewMode: ->
          @_buttonsShowOnly(['DrawField', 'ReadFromFile', 'CreateParcel', 'CancelAction'])

        _inInfoMode: ->

        _inEditMode: ->
          if !@model.getFeature()? || @model.getFeature().hasEmptyGeo()
            @_inNewMode()
          else
            @_buttonsShowOnly(['AddParcel', 'RemoveParcel', 'ExclusionShape', 'UnionShape', 'ClearShape'])

        _inNoneMode: ->
          @_buttonsShowOnly(['AddNewField'])

        _allButtons: ['AddNewField', 'DrawField', 'ReadFromFile', 'CancelAction', 'ClearShape', 'ExclusionShape', 'UnionShape', 'CreateParcel','AddParcel', 'RemoveParcel']
        _buttonsHide: (buttons) -> _.each(buttons, (b) => @$el.find("#map#{b}").addClass('hidden'))
        _buttonsShow: (buttons) -> _.each(buttons, (b) => @$el.find("#map#{b}").removeClass('hidden'))
        _buttonsShowOnly: (buttons) ->
          @_buttonsHide(@_allButtons)
          @_buttonsShow(buttons)

        _onFeatureUpdated: -> @_onModeChanged()

        _onCurrentModelChange: () ->
          @_unbindEventsToModel()
          @model = @rootModel.currentModel()
          @_bindEventsToModel()
          @_onFeatureUpdated()

        _bindEventsToModel:   -> if @model then  @model.on('updated:feature', @_onFeatureUpdated, @)
        _unbindEventsToModel: ->if @model then @model.off('updated:feature', @_onFeatureUpdated, @)

        _onStateChanged: ->
          switch @rootModel.state()
            when 'map' then @show()
            when 'list' then @hide()

        _onSidebarChanged: ->
          if @rootModel.get('sidebar')
            @$el.find('#mapFullMapView').removeClass('hidden')
            @$el.find('#mapNormalMapView').addClass('hidden')
            @makeViewOnNormalSize()
          else
            @$el.find('#mapNormalMapView').removeClass('hidden')
            @$el.find('#mapFullMapView').addClass('hidden')
            @makeViewOnFullSize()

        makeViewOnFullSize: -> $('#map').removeClass('width-60')
        makeViewOnNormalSize: -> $('#map').addClass('width-60')

        show: -> @$el.removeClass('hidden')
        hide: -> @$el.addClass('hidden')

        render: ->
          @$el.html @template({items: @rootModel.collection().models})
          @

      class _ListView extends _BaseView
        events:
          'click #list.field-card tbody tr':'showField'

        showField: (e) ->
          target = $(e.target)
          target = $(e.target).closest('tr') unless target.is('tr')
          id = target.data('id')
          field = @rootModel.collection().getById(id)
          @rootModel.selectField(field, selectOnMap: true)

        _templateName: 'field_card/list'

        _initialize: ->
          @rootModel.on('change:state', @_onStateChanged, @)
          @rootModel.collection().on('change', @render, @)
          @rootModel.collection().on('remove', @render, @)
          @rootModel.collection().on('add',    @render, @)

        _onStateChanged: ->
          switch @rootModel.state()
            when 'map' then @hide()
            when 'list' then @show()

        show: -> @$el.removeClass('hidden')
        hide: -> @$el.addClass('hidden')
        render: ->
          @$el.html @template({items: @rootModel.collection().toJSON()})
          @

    class _SideBarView extends _BaseView
      _templateName: 'field_card/sidebar'
      _initialize: (opts={}) ->
        @_initTabs()
        @rootModel.on('change:sidebar', @_onSidebarChanged, @)

      _onSidebarChanged: ->
        if @rootModel.get('sidebar') then @show() else @hide()

      show: -> $('.field-info-box').removeClass('hidden')
      hide: -> $('.field-info-box').addClass('hidden')

      tabs: []

      _initTabs: ->
        @_infoSideBarView = new _InfoSideBarView(model: @rootModel.currentModel(), rootModel: @rootModel)
        @tabs.push(@_infoSideBarView)

      remove: ->
        _.each(@tabs, (t) -> t.remove())
        @tabs.splice(0, @tabs.length)
        super

      render: ->
        @_initTabs() if @tabs.length == 0
        @$el.html @template(items: _.map(@tabs, (t) -> t.tabProperties))
        container = @$el.find('.tab-content')
        _.each(@tabs, (t) => container.append t.render().el)
        @tabs[0].activate()
        @

      class _InfoSideBarView extends _BaseView
        _templateName: 'field_card/sidebar_info_section'
        attributes:
          id: "field-info"
          class: 'tab-pane active'

        events:
         'change #txt_info_name': 'onChangeTxtName'
         'keyup #txt_info_name':  'onChangeTxtName'
         'blur #txt_info_name':   'onChangeTxtName'
         'change .txt_field_edit': 'onChangeFieldTxtProperty'

         'click #infoSectionSave':   'onSaveFieldBtnClick'
         'click #infoSectionDelete': 'onDeleteFieldBtnClick'

         'mouseenter .js-parcels-tbody tr': 'onMouseenterTrParcelElement'
         'mouseleave .js-parcels-tbody tr': 'onMouseleaveTrParcelElement'
         'click .js-parcels-tbody tr .js-parcel-info':   'onClickTrParcelElementInfo'
         'click .js-parcels-tbody tr .js-parcel-edit':   'onClickTrParcelElementEdit'
         'click .js-parcels-tbody tr .js-parcel-delete': 'onClickTrParcelElementDelete'
         'click #btn_update_areas_panel':                'onClickBtnUpdateAreasPanel'
         'click #btn_not_update_areas_panel':            'onClickBtnNotUpdateAreasPanel'
         'click #btn_update_parcels_list':               'onClickBtnUpdateParcelsPanel'
         'click #btn_not_update_parcels_list':           'onClickBtnNotUpdateParcelsPanel'

         'click #add_new_parcel':      'onClickBtnAddNewParcel'

        getParcelFromClickParcelElementAction: (e) ->
          e.preventDefault()
          parcelId = parseInt($(e.currentTarget).closest('tr').data('id'))
          @model.parcels.models[parcelId]

        onClickTrParcelElementInfo: (e) ->
          e.preventDefault()
          parcel = @getParcelFromClickParcelElementAction(e)
          rootModel = @rootModel
          if parcel.getFeature()
            parcel.blockFadeInOut()
            parcel.featureFadeIn(50, force: true)
            rootModel.parcelNotifiInfo parcel, cbfn: ->
              parcel.featureFadeOut(50, force: true)
              parcel.unblockFadeInOut()
          else
            rootModel.parcelNotifiInfo(parcel)

        onDeleteFieldBtnClick: ->
          if confirm('Czy na pewno chcesz usunąć pole? Operacji nie można cofnąć')
            @rootModel.deleteCurrentModel()

        onSaveFieldBtnClick: (e) ->
          # if form.valid() TODO
          e.preventDefault()
          @rootModel.saveCurrentModel()

        onClickTrParcelElementEdit: (e) ->
          parcel = @getParcelFromClickParcelElementAction(e)
          @showParcelModal(parcel)

        onClickTrParcelElementDelete: (e) ->
          parcel = @getParcelFromClickParcelElementAction(e)
          parcel.destroyFeature()
          @rootModel.removeParcelFromCollection(parcel)

        onMouseenterTrParcelElement: (e) ->
          e.preventDefault()
          parcelId = parseInt($(e.currentTarget).data('id'))
          @model.parcels.models[parcelId].featureFadeIn(250) if parcelId? && !_.isNaN(parcelId)

        onMouseleaveTrParcelElement: (e) ->
          e.preventDefault()
          parcelId = parseInt($(e.currentTarget).data('id'))
          @model.parcels.models[parcelId].featureFadeOut(250) if parcelId? && !_.isNaN(parcelId)

        onChangeTxtName: (e)->
          text     = $.trim($(e.currentTarget).val())
          $lblName = @$el.find('#lbl_info_live_name')
          name = if text.length then text else 'Nowe pole'
          $lblName.html(name)
          @model.setFeatureName(name)

        onChangeFieldTxtProperty: (e) ->
          target = $(e.currentTarget)
          property = target.data('property')
          val = $.trim(target.val())

          if property == 'area' || property == 'growing_area'
            val = Math.abs(parseFloat(val.replace(',', '.')))
            val = @model.area() if _.isNaN(val) and property == 'area'
            val = @model.growingArea() if _.isNaN(val) and property == 'growing_area'

          if property == 'name' || property =='proper_name'
            val = $.trim(val)
            val = @model.get('name') if val.length == 0 and property == 'name'

          target.val(val)
          @model.set(property, target.val())

        showParcelModal: (parcelModel) ->
          @_modalView.remove() if @_modalView
          if parcelModel?
            mode = 'edit'
            @_parcelModel = parcelModel
          else
            mode = 'add'
            @_parcelModel = new (@rootModel.module._getModel('Parcel'))()
          @_modalView   = new _ParcelsModalView(model: @_parcelModel, mode: mode, rootModel: @rootModel)
          @$el.find('.js-modal-container').removeClass('hidden').html @_modalView.render().el
          @_modalView.afterRender()

        onClickBtnAddNewParcel: (e) -> @showParcelModal()

        updateModel: ->
          name = $.trim($('#txt_info_name').val())
          @model.set
            name:         if name.length then name else 'Nowe pole'
            proper_name:  $.trim($('#txt_info_proper_name').val())
            area:         $.trim($('#txt_info_area').val())
            growing_area: $.trim($('#txt_info_growing_area').val())
            usage:        $.trim($('#sel_info_usage').val())

        onClickBtnUpdateParcelsPanel: ->
          @rootModel.updateParcelsByFeature(@model.getFeature())
          @hideParcelsUpdateButton()

        onClickBtnNotUpdateParcelsPanel: -> @hideParcelsUpdateButton()

        onFeatureUpdated: ->
          if @fieldAreasAreEqZero() || @model.parcels.length == 0
            @onClickBtnUpdateParcelsPanel()
          else
            @showParcelsUpdateButton()

        showParcelsUpdateButton: -> @$el.find("#js-update-parcels-list-panel").addClass('alert alert-info')
        hideParcelsUpdateButton: -> @$el.find("#js-update-parcels-list-panel").removeClass('alert alert-info')
        onParcelsUpdated: ->
          $txtArea = $('#txt_info_area')
          $txtGrowingArea = $('#txt_info_growing_area')

          if $.trim($txtArea.val()).length is 0 && $.trim($txtGrowingArea.val()).length is 0
            $txtArea.val(@model.sumArea())
            $txtGrowingArea.val(@model.sumAreaUsed())

          @updateModel()
          @render()
          if @checkParcelsAreaAreEq()
            @hideAreasUpdateButton()
          else
            if @fieldAreasAreEqZero()
              area = @model.sumAreaUsed()
              @model.set({area: area, growing_area: area})
              @render()
            else
              @showAreasUpdateButton()

        fieldAreasAreEqZero: -> @model.area() == 0 && @model.growingArea() == 0

        onClickBtnUpdateAreasPanel: ->
          @$el.find('#txt_info_area').val(@model.sumAreaUsed())
          $growingArea = @$el.find('#txt_info_growing_area')
          $growingArea.val(@model.sumAreaUsed()) if parseFloat($growingArea.val()) == 0
          @hideAreasUpdateButton()

        onClickBtnNotUpdateAreasPanel: -> @hideAreasUpdateButton()

        checkParcelsAreaAreEq: ->
          area     = @$el.find('#txt_info_area').val()
          usedArea = @$el.find('#txt_info_growing_area').val()
          @model.sumArea() is parseFloat(area) && @model.sumAreaUsed() is parseFloat(usedArea)

        showAreasUpdateButton: -> @$el.find("#js-area-update-panel").addClass('alert alert-info')
        hideAreasUpdateButton: -> @$el.find("#js-area-update-panel").removeClass('alert alert-info')

        tabProperties:
          active: true
          tag_id: 'field-info'
          icon: 'info'
          title:'Info'

        _initialize: ->
          @rootModel.on('change:mode', @_onModeChanged, @)
          @rootModel.on('change:currentModel', @_onCurrentModelChange, @)
          @bindEventsToModel()

        activate: -> @$el.addClass('active')
        deactivate: -> @$el.removeClass('active')

        bindEventsToModel: () ->
          if @model
            @model.on('updated:feature', @onFeatureUpdated, @)
            @model.on('change:parcels',  @onParcelsUpdated, @)

        unbindEventsToModel: ->
          if @model
            @model.off('updated:feature', @onFeatureUpdated, @)
            @model.off('change:parcels',  @onParcelsUpdated, @)

        _onModeChanged: ->
          @updateModel()
          @render()

        onEditMode: -> @onNewMode()

        onNewMode: ->
          @$el.find('.js-show-mode').addClass('hidden')
          @$el.find('.js-edit-mode').removeClass('hidden')
          @$el.find('#infoSectionEdit').addClass('hidden')

        onInfoMode: ->
          @$el.find('.js-edit-mode').addClass('hidden')
          @$el.find('.js-show-mode').removeClass('hidden')
          @$el.find('#infoSectionEdit').removeClass('hidden')

        usageList: ['Grunty orne', 'Lasy', 'Sady i plantacje', 'Użytki zielone']
        render: ->
          json   = @model.toJSON()
          isEdit = @rootModel.get('mode') is 'edit' || 'new'
          usage  = @model.get('usage').toString()
          opts =
            mode:            { info: !isEdit, edit: isEdit }
            parcelsSumAreas: { area: @model.sumArea(), area_used: @model.sumAreaUsed() }
            elementsCount:   @model.parcels.length
            usageTmpl: if isEdit
              _.map( @usageList, (name, idx) =>
                isSelected = if usage is (idx + 1).toString() then "selected='selected'" else ""
                "<option value='#{idx + 1}' #{isSelected}>#{name}</option>"
              ).join('')
            else
              @usageList[usage + 1]

          @$el.html @template($.extend(json, opts))
          $("[data-fieldcard-tooltip=tooltip]").tooltip()
          @

        _onCurrentModelChange: () ->
          @unbindEventsToModel()
          @model = @rootModel.currentModel()
          @bindEventsToModel()
          @render()

        #.js-modal-container
        class _ParcelsModalView extends _BaseView
          _templateName: 'field_card/sidebar_info_section_parcels_modal'
          _initialize:(opts={}) ->
            @mode = opts.mode
            urls              = @rootModel.module._getUrl('SearchPrecinct')
            @_searchUrl       = urls.BaseUrl
            @_searchByCodeUrl = urls.ByCodeUrl
          _modalTitleAttr: ->
            if @mode is 'add'
              {mode: 'add', success_button_title: 'Dodaj', title: 'Dodaj działkę ewidencyjną'}
            else
              {mode: 'edit', success_button_title: 'Zapisz', title: 'Edytuj działkę ewidencyjną'}

          events:
            "click #cancel_parcel":         "onClickBtnCancel"
            "click .js-close":              "onClickBtnCancel"
            "click #create_or_save_parcel": "onClickBtnCreateOrSave"
            "change #txt_parcel_area":      "onParcelAreaTxtChange"
            "change #txt_parcel_area_used": "onParcelAreaTxtChange"

          onParcelAreaTxtChange: (e) ->
            target = $(e.target)
            val = Math.abs(parseFloat($.trim(target.val()).replace(',', '.'))) || 0
            target.val(val)

          onClickBtnCancel: ->
            @$el.closest('.sidebar-modal-container').addClass('hidden')
            @remove()

          onClickBtnCreateOrSave: ->
            @$el.closest('.sidebar-modal-container').addClass('hidden')
            if @mode is 'add' then @addParcel() else @updateParcel()

          addParcel:    ->
            @_syncParcelData()
            @rootModel.addParcelIntoCollection(@model)
            @remove()

          updateParcel: ->
            @_syncParcelData()
            @model.trigger('updateByUser')
            @remove()

          _prepareValue: (fieldId) ->
            $.trim(@$el.find("##{fieldId}").val()).replace(',', '.')

          _syncParcelData: ->
            code      = @_prepareValue('txt_parcel_code')
            codeAsArr = code.split('.')
            number    = codeAsArr[codeAsArr.length - 1]
            areaInHa  = Math.abs(parseFloat(@_prepareValue('txt_parcel_area'))) || 0
            areaUsed  = Math.abs(parseFloat(@_prepareValue('txt_parcel_area_used'))) || 0
            @model.set
              place:      @$el.find('#txt_parcel_location').val()
              code:       code
              number:     number
              area_in_ha: areaInHa
              area_used:  areaUsed

          afterRender: ->
            $locationTxt = $(@$el.find('#txt_parcel_location')[0])
            @locationTypeahead = new window.App.Helpers.Typeahead($locationTxt, @_searchUrl, decorator: @searchDecorator, selectItem: @searchSelectItem)
            @locationTypeahead.init()

          searchDecorator: (obj) -> "#{obj.district}/#{obj.name}"
          searchSelectItem: (item) => @$el.find("#txt_parcel_code").val(item.code)

          render: ->
            json = @model.toJSON()
            json = $.extend(json, @_modalTitleAttr())
            @$el.html @template(json)
            @
