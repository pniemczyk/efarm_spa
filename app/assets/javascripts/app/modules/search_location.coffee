class window.App.Modules.SearchLocation extends window.App.Modules.BaseModule
  @name: 'SearchLocation'
  @require: ->
    name: 'SearchLocation'
    libs: ['commands', 'reqres']

  _initialize: ->
    urls            = @_getUrl('SearchLocation')
    @_searchUrl     = urls.LocationByNameUrl
    @_whatIsHereUrl = urls.LocationByGeoUrl

  _registerEvents: ->
    @_reqres.setHandler("SearchLocation-search", @search, @)
    @_reqres.setHandler("SearchLocation-getCurrentLocation", @getCurrentLocation, @)
    @_reqres.setHandler("SearchLocation-whatIsHere", @whatIsHere, @)
    @_reqres.setHandler("SearchLocation-whatPlaceIsHere", @whatPlaceIsHere, @)

##search module
#http://open.mapquestapi.com/nominatim/
#http://open.mapquestapi.com/nominatim/v1/reverse.php?format=json&json_callback=renderExampleThreeResults&lat=51.521435&lon=-0.162714
#http://nominatim.openstreetmap.org/reverse?format=json&lat=50.2350864&lon=22.5301271&addressdetails=1
  search: (keyWords) ->
    result = null
    $.ajax(
      dataType: "json"
      type: "GET"
      url: @_searchUrl
      async: false
      data: @_defaultSearchData(keyWords)
    ).done((res) => result = @_filterSearchResults(res))
    result

  whatPlaceIsHere: (lonLat, projection=900913) ->
    result    = @whatIsHere(lonLat, projection)
    full_name = @_humanizeSearchName(result.display_name)
    name      = if result.address
      result.address.village ||  result.address.city || null
    else
      null

    full_name: full_name
    name: name

  whatIsHere: (lonLat, projection=900913) ->
    result = null
    lonLat = @_reqres.request('Geometry-buildLonLat', lonLat, projection)
    lonLat = lonLat.getTransformedLonLat(4326)
    data =
      format: 'json'
      lon: lonLat.lon
      lat: lonLat.lat
      addressdetails: 1
    $.ajax(
      dataType: "json"
      type: "GET"
      url: @_whatIsHereUrl
      async: false
      data: data
    ).done((res) => result = res)
    result

  getCurrentLocation: ->
    return unless navigator.geolocation
    @_getCurrentPositionDeferred(timeout:100000)

  _getCurrentPositionDeferred: (options) ->
    deferred = $.Deferred()
    navigator.geolocation.getCurrentPosition(deferred.resolve, deferred.reject, options)
    deferred.promise()

  _defaultSearchData: (keyWords) ->
    # countrycodes: 'de,lt,pl,ru,by,ua,sk'
    opts =
      countrycodes: 'pl'
      polygon: 0
      # viewbox: <left>,<top>,<right>,<bottom>
      limit: 10
      q: keyWords
      addressdetails: 1
      format: "json"

    opts["accept-language"] = 'pl'
    opts


  _allowedLoactionsTypes: ['administrative', 'peak', 'village', 'city']

  _filterSearchResults: (data) ->
    return [] unless data.length
    listNames = []
    result = for item in data when listNames.indexOf(@_humanizeSearchName(item.display_name)) is -1
      do (item) =>
        return null if _.indexOf(@_allowedLoactionsTypes, item.type) is -1
        name = @_humanizeSearchName(item.display_name)
        listNames.push(name)
        name:   name
        lonLat: [item.lon, item.lat]
        class:  item.class
        type:   item.type
    _.compact(result)

  _wordsInNameToTrim: ['European Union', 'Poland', 'Voivodeship', 'Zobten']
  _postCodeRegex: /^[0-9]{2}-[0-9]{3}$/i

  _humanizeSearchName: (name) ->
    splited = name.replace(';',',').split(',')
    _.uniq(_.compact(_.map(splited, (text) => $.trim(text) if _.every(@_wordsInNameToTrim, (w) => text.indexOf(w) is -1) && !@_postCodeRegex.test($.trim(text))))).join(', ')