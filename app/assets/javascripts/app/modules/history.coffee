# Moved here to have less garbage in Map module
class window.App.Modules.History extends window.App.Modules.BaseModule
  # History + Commands
  class MapCommandsHistory
    _history: []
    _pointer: -1

    add: (e) ->
      if @_pointer < @_history.length - 1 # it means user has done some undos and then pushed new action
        _.each(@_history.slice(@_pointer + 1), (c) -> c.swap())

      @_history.push(e)
      @_pointer = @_history.length - 1

    undo: () ->
      return null if @_pointer < 0 # empty queue
      @_history[@_pointer].execute()
      @_pointer -= 1

    redo: () ->
      return null if @_pointer == @_history.length - 1 # nothing more in queue
      @_pointer += 1
      @_history[@_pointer].execute()

    clear: ->
      @_history = []
      @_pointer = -1

  class BaseCommand
    last = 'redo'
    swap: () -> @last = if @_isRedo() then 'undo' else 'redo'

    _isRedo: () -> @last == 'redo'

  class ShapeChangeCommand extends BaseCommand
    constructor: (@module, @features) ->
    execute: () ->
      if @_isRedo()
        _.each(@features, (f) => @module.updateFeatureGeo(@module.getFeatureById(f.id), f.afterActionGeo))
      else
        _.each(@features, (f) => @module.updateFeatureGeo(@module.getFeatureById(f.id), f.beforeActionGeo))
      @swap()

  class CreateCommand extends BaseCommand
    constructor: (@module, @feature) ->
    execute: () ->
      if @_isRedo()
        @module.drawingLayer.addFeatures([@feature])
      else
        @module.drawingLayer.removeFeatures([@feature])
      @swap()

  class ShapeChangeData
    constructor: (@id, @beforeActionGeo, @afterActionGeo) ->