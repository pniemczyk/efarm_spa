class window.App.Modules.SettingManager extends window.App.Modules.BaseModule
  @name: 'SettingManager'
  @require: ->
    name: 'SettingManager'
    config: 'SettingManager'
    libs: ['commands', 'reqres', 'transaction']

  _model: null

  _registerEvents: ->
    @_reqres.setHandler("Settings-get", @get, @)
    @_commands.setHandler('Settings-set', @set, @)
    @_commands.setHandler('Settings-refresh', @refresh, @)

  _initialize: ->
    data = @_config.data || {}
    url = @_getUrl('SettingsUrl')
    @_model = new (@_getModel('Settings'))(data, url: url)
    @_model.fetch() if data is {}

  refresh:           -> @_model.fetch()
  get: (name)        -> @_model.get(name)
  set: (name, value) -> @_model.update(name, value)
