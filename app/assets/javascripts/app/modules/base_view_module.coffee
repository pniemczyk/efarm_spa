﻿class window.App.Modules.BaseViewModule extends window.App.Modules.BaseModule
  @name: 'BaseViewModule'
  @require: ->
    name: @name
    libs: ['commands', 'reqres']
    config: @name

  constructor: (opts={}, require={}) ->
    super(opts, require)
    @_commands.execute('PageSidebar-addItem', @sidebar) if @sidebar

  _getTemplate: (name) ->
    @_error("reqres is not configured", "#_getTemplate") unless @_reqres
    @_reqres.request('TemplateProvider-get', name)

  _getPagingHtml: (opts) ->
    @_error("reqres is not configured", "#_getPagingHtml") unless @_reqres
    @_reqres.request('Pagination-getHtml', opts)

  _showView: (view) ->
    @_error("commands is not configured", "#_showView") unless @_commands
    @_commands.execute('PageContent-setView', view)

  _showBreadcrumb: (breadcrumb) ->
    @_error("commands is not configured", "#_showBreadcrumb") unless @_commands
    @_commands.execute('PageBreadcrumb-set', breadcrumb)

  _showBreadcrumbButtons: (data) ->
    @_error("commands is not configured", "#_showBreadcrumbButtons") unless @_commands
    @_commands.execute('PageBreadcrumb-setButtons', data)

  _showLoader: ->
    @_error("commands is not configured", "#_showLoader") unless @_commands
    @_commands.execute('PageLoader-show')

  _formFields: (formId) ->
    fields = $("##{formId}").serializeArray()
    data = {}
    _.each(fields, (field)-> data[field.name] = field.value)
    data

  _defaultInfo:
    title: ''
    text: ''
    type: 'info'
    center: false
    time: 3000

  info: (opts={})->
    opts = $.extend(@_defaultInfo, opts)
    position = if opts.center then 'gritter-center' else ''
    opts.class_name = "gritter-#{opts.type} #{position}"
    delete opts.type
    delete opts.center
    $.gritter.add(opts)