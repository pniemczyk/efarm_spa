﻿class window.App.Modules.Profile extends window.App.Modules.BaseViewModule
  @name: 'Profile'
  @require: ->
    name: 'Profile'
    libs: ['commands', 'reqres']

  routes:
    'profile':'showProfile'
    'profile/changePassword':'showChangePassword'

  showProfile: => @_show()

  showChangePassword: =>  @_show()

  _show: ->
    @_showLoader()
    @_build()
    @_showView(@_view)
    @_breadcrumb()

  _showEditProfileButton: -> @_showBreadcrumbButtons([{icon:'edit', label:'Edytuj', cb: -> $('#btnProfileEdit').click() }])
  _showChangeProfilePasswordButtons: ->
    @_showBreadcrumbButtons([
      {type:'default', icon:'undo.bigger-110', label:'Wyczyść', cb: -> $('#btnResetPassword').click() }
      {type:'success', icon:'check', label:'Zapisz', cb: -> $('#btnSavePassword').click() }
    ])

  _breadcrumb: ->
    switch window.location.hash
      when "#profile"
        @_showBreadcrumb([ { title: "Profil"} ])
        @_showEditProfileButton()
      when "#profile/changePassword"
        @_showBreadcrumb([ { url: '#profile' , title: "Profil"},  {title: "Zmiana hasła"}])
        @_showChangeProfilePasswordButtons()
      else []

  _build: ->
    unless @_model
      @_defaultImageUrl = @_getUrl('DefaultAvatarImageUrl') unless @_defaultImageUrl
      @_url = @_getUrl('ProfileOwnUrl') unless @_url
      @_model = new _Model({image_url: @_defaultImageUrl}, {url: @_url})
      @_model.fetch()
    @_template = @_getTemplate(@_templateName) unless @_template
    if @_view
      @_view.remove()
      delete @_view
    @_view = new _View(model: @_model, template: @_template, module: @)

  _templateName: 'profile/index'

  class _PasswordModel extends Backbone.Model
    initialize: (model={}, opts={}) -> @url = opts.url
    defaults:
      old_password: ''
      password: ''
      password_confirmation: ''

  class _Model extends Backbone.Model
    initialize: (model={}, opts={}) -> @url = opts.url
    parse: (response, xhr) ->
      response = window.App.jsonResponse(response)
      profile = response.data.profile || {}
      profile.created_at = profile.created_at.replace('T', ' ') if profile.created_at
      profile

    getPercentOfDone: ->
      properties = ['first_name', 'last_name', 'nick']
      allItemsCount = 7
      itemValue = parseInt(100/allItemsCount)
      result = allItemsCount - properties.length
      (result++ if @get(prop))for prop in properties
      if result is allItemsCount then null else result * itemValue

    update: ->
      @save({}, method: 'PUT')

  class _View extends Backbone.View
    title: 'Profil'
    tagName: 'div'

    initialize: (opts={}) ->
      @_module = opts.module
      @_template = opts.template
      @model.on('change', @modelChange, @)

    events:
      'click #btnInviteFriend'  : 'inviteFriends'
      'click #btnSavePassword'  : 'savePassword'
      'click #btnProfileEdit'   : 'editProfile'
      'click #btnProfileCancel' : 'cancelEditProfile'
      'click #btnProfileSave'   : 'saveProfile'

    inviteFriends: (e) ->
      e.preventDefault()
      @_module._commands.execute('InviteFriends-showModal')

    savePassword: (e) ->
      e.preventDefault()
      if $('#resetPasswordForm').valid()
        data = @_module._formFields('resetPasswordForm')
        model = new _PasswordModel(data, url: @_module._url)
        model.save([],method: 'POST').done( (data) =>
          response = window.App.jsonResponse(data)
          $('#resetPasswordForm button[type=reset]').trigger('click')
          @_module.info(title: 'Zmiana hasła', text: response.message, type: response.type, center:true)

          $('#resetPasswordForm').validate().showErrors(response.data)
        ).fail( ->
          @_module.info(title: 'Zmiana hasła', text: 'Wystąpił problem podczas połączenia z serwerem. hasło nie zostało zmienione.', type: 'error', center:true)
        )

    editProfile: (e) ->
      e.preventDefault()
      @editMode()

    editMode: ->
      $('.js-show-mode').addClass('hidden')
      $('.js-edit-mode').removeClass('hidden')
      @_module._showBreadcrumbButtons([
        {type:'error', icon:'times', label:'Anuluj', cb: -> $('#btnProfileCancel').click() }
        {type:'success', icon:'check', label:'Zapisz', cb: -> $('#btnProfileSave').click() }
      ])

    cancelEditProfile: (e) ->
      e.preventDefault()
      @hideEditable()

    hideEditable: ->
      $('.js-edit-mode').addClass('hidden')
      $('.js-show-mode').removeClass('hidden')
      @_module._showEditProfileButton()

    saveProfile:(e) ->
      data = @_module._formFields('profileForm')
      data['is_man'] = data['is_man'] is '1'
      @model.set(data)
      @model.update()
      @hideEditable()

    _fields: (formId) ->

    showTab: ->
      @tabName = window.location.hash.replace("#",'').replace('/','-')
      tabBtnName = "btn-#{@tabName}"
      @$el.find("##{tabBtnName}").tab('show')

    _showProfileSave: ->
      @$el.find('.js-profile-save').removeClass('hidden')

    remove: ->
      @_removeValidateForm()
      super

    _removeValidateForm: ->
      $('#resetPasswordForm').data('validator', null)
      $("#resetPasswordForm").unbind('validate')

    setValidateForm: -> @_module.validateFormInit('resetPasswordForm', @_validateConfig)

    _validateConfig:
      rules:
        old_password:
          required: true
        password:
          required: true
          minlength: 6
        password_confirmation:
          required: true
          minlength: 6
          equalTo: "#password"
      messages:
        old_password:
          required: 'wypełnienie jest wymagane'
        password:
          required: 'wypełnienie jest wymagane'
          minlength: 'zbyt mała ilość znaków. minimum 6'
        password_confirmation:
          required: 'wypełnienie jest wymagane'
          minlength: 'zbyt mała ilość znaków. minimum 6'
          equalTo: "hasło musi się zgadzać z nowym hasłem"

    render: ->
      json = @model.toJSON()
      json.PercentOfDone = @model.getPercentOfDone()
      @$el.html @_template(json)
      @

    modelChange: ->
      @beforeRender()
      @render()
      @afterRender()

    beforeRender: ->
      #unbind all events if needed

    afterRender: ->
      @setValidateForm()
      @showTab()