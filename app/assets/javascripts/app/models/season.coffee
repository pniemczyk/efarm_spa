class window.App.Models.Season extends window.App.Models.Base
  parse: (response, xhr) ->
    return response.season if response.season?
    response

  defaults:
    id: ''
    name: ''
    first_year: ''
    active: ''

