class window.App.Models.FieldCard extends window.App.Models.Base
  _init: ->
    @parcels = new (App.reqres.request('Collection-get', 'Parcel'))(@get('parcels'), '')
    @parcels.on('add', @updateParcelsInModel, @)
    @parcels.on('remove', @updateParcelsInModel, @)
    @parcels.on('change', @updateParcelsInModel, @)
    @parcels.on('reset', @resetParcelsInModel, @)

  parse: (response, xhr) ->
    response

  defaults:
    id: -1
    name: 'Nowe pole'
    proper_name: ''
    area: 0
    growing_area: 0
    usage: 1
    parcels: null
    geo_json: null
    parcels: []
    # farm_in_season_id

  _feature: null

  getFeature: -> @_feature
  olId: -> if @_feature then @_feature.olId()
  getFeatureAreaInHa: -> if @_feature then @_feature.getAreaInfo().haRound else @sumAreaUsed()

  createFeature: (feature) -> @_feature = feature

  updateFeature: (feature) ->
    @_feature = feature
    @trigger('updated:feature')

  updateId: (newId) ->
    @set('id', newId)
    if @_feature then @_feature.addToAttributes(id: @get('id'))

  updateParcelsInModel: -> @set('parcels', @parcels.toJSON())

  sumArea:     -> @parcels.sumArea()
  sumAreaUsed: -> @parcels.sumAreaUsed()

  resetParcelsInModel:  -> @set('parcels', null)

  setFeatureName: (name) -> if @_feature then @_feature.setName(name)

  area: -> @_floatOrZero(@get('area'))
  growingArea: -> @_floatOrZero(@get('growing_area'))

  persisted: -> ~~@get('id') > 0

  toJSON: ->
    json = super()
    json.parcels = @parcels.toJSON()
    json.display_usage = @_displayUsage()
    json.geo_json = if @_feature then @_feature.toGeoJson() else null
    json

  _displayUsage: -> ['', 'Grunty orne', 'Lasy', 'Sady i plantacje', 'Użytki zielone'][~~@get('usage')]
  _floatOrZero: (val) ->
    val = parseFloat(val)
    if !_.isNaN(val) then val else 0