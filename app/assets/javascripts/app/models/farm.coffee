class window.App.Models.Farm extends window.App.Models.Base
  parse: (response, xhr) ->
    return response.farm if response.farm?
    response

  defaults:
    id: ''
    arimr: ''
    name: ''
    shortname: ''
    pesel: ''
    regon: ''
    nip: ''
    bank_account: ''
    enable: ''
    country: ''
    country_code: 'PL'
    city: ''
    post_code: ''
    street: ''
    number: ''