class window.App.Models.Base extends Backbone.Model
  initialize: (model={}, @opts={}) ->
    @on('error', @_onError, @)
    @url = opts.url if @opts.url
    @_gen prop for prop of @defaults
    @_init()

  _onError: (model, resp, options) ->
  _init: ->

  _gen: (name, prefix='attr') =>
    m = "#{prefix}#{_.str.classify(name)}"
    return if @[m]
    @[m] = (val, opts={}) ->
      return @get(name) unless val?
      @set(name, val, opts)