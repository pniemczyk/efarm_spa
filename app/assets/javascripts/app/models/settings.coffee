class window.App.Models.Settings extends window.App.Models.Base
  parse: (response, xhr) ->
    return response.settings if response.settings?
    response

  isNew: -> false

  update: (name, value) ->
    @set(name, value)
    attrToUpd = {}
    attrToUpd[name] = value
    @save(attrToUpd, {patch: true, method: 'PUT'})

  defaults:
    current_season_id: null
    current_farm_id: null
    ui_sidebar_minimized: false
    last_url: null