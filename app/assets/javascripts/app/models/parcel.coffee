class window.App.Models.Parcel extends window.App.Models.Base
  initialize: (model={}, @opts={}) ->
    if @opts.parcel
      @buildFromParcelObj(@opts.parcel)
    else
      @_buildFromSelf()

    @on('error', @_onError, @)
    @_gen prop for prop of @defaults

  defaults:
    code: ''
    number: ''
    area: ''
    area_in_ha: ''
    area_used: ''
    teryt: ''
    geo_type: ''
    geo_len: ''
    geo_area: ''
    province: ''
    community: ''
    district: ''
    precinct: ''
    place: ''
    feature: ''

  buildFromParcelObj: (data) ->
    attrs        = data.attributes
    areaInfo     = App.reqres.request("Geometry-getAreaInfoFromMeters", @_unifyStringFloatNumbers(attrs.POWIERZCHNIA))
    area         = areaInfo.m
    areaInHa     = areaInfo.haRound
    geometryType = data.geometryType.replace('esriGeometry', '')
    projection   = data.geometry.spatialReference.wkid
    feature      = @_createFeature(data.geometry, geometryType, projection)
    district     = @_stringOrNull(attrs.GMINA) || 'Brak danych'
    precinct     = @_stringOrNull(attrs.OBREB) || 'Brak danych'
    place        =  _.compact(_.uniq([district, precinct])).join('/') || 'Brak danych'

    @set
      code:       attrs.IDENTYFIKATOR
      number:     attrs.NUMER
      area:       area
      area_in_ha: areaInHa
      area_used:  areaInHa
      teryt:      attrs.TERYT
      geo_len:    @_unifyStringFloatNumbers(attrs['SHAPE.LEN'])
      geo_area:   @_unifyStringFloatNumbers(attrs['SHAPE.AREA'])
      province:   @_stringOrNull(attrs.WOJEWODZTWO) || 'Brak danych'
      community:  @_stringOrNull(attrs.POWIAT) || 'Brak danych'
      district:   district
      precinct:   @_stringOrNull(attrs.OBREB) || 'Brak danych'
      place:      place
      feature:    feature

  getFeature:    -> @get('feature')

  _blockFadeInOut: false
  blockFadeInOut:   -> @_blockFadeInOut = true
  unblockFadeInOut: -> @_blockFadeInOut = false

  featureFadeIn:  (delay=3000, opts={})->
    force = opts.force || false
    if force || !@_blockFadeInOut
      feature = @get('feature')
      feature.fadeIn(delay, opts.fn || null) if feature

  featureFadeOut: (delay=3000, opts={})->
    force = opts.force || false
    if force || !@_blockFadeInOut
      feature = @get('feature')
      feature.fadeOut(delay, opts.fn || null) if feature

  destroyFeature: ->
    feature = @getFeature()
    feature.destroy() if feature

  location: ->
    display: @attributes.place
    fullLocation: _.compact([
      @attributes.province
      @attributes.community
      @attributes.district
      @attributes.precinct
    ]).join(', ')
  getJsonForInfo: ->
    result = @toJSON()
    result['geo_len'] = Math.round(result['geo_len'] * 10)/10 || 0
    result

  _createFeature: (esriGeometry, type, projection)->
    App.reqres.request("Geometry-featureFromEsriGeometry", esriGeometry, {type: type, projection: projection})

  _unifyStringFloatNumbers: (number) -> parseFloat(number.replace(',','.'))
  _onError: (model, resp, options) ->

  _gen: (name, prefix='attr') =>
    m = "#{prefix}#{_.str.classify(name)}"
    return if @[m]
    @[m] = (val, opts={}) ->
      return @get(name) unless val?
      @set(name, val, opts)

  _buildFromSelf:  -> @set('number', @_numberFromCode())
  _numberFromCode: -> if @get('code')? then _.last(@get('code').split('.'))

  _stringOrNull: (val) ->
    return null unless val?
    if val.toString().match(/null/i) then null else val.toString()