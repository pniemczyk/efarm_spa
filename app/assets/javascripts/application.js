//= require jquery
//= require jquery_ujs
//= require jquery.ui.all
//= require jquery-fileupload/basic
//= require handlebars
//= require handlebars.runtime
//= require libs/esri_json_converter
//= require libs/lodash.underscore
//= require libs/underscore.string
//= require libs/backbone
//= require libs/backbone.wreqr
//= require libs/tag-it
//= require_self

window.log  = function(msg){ console.log(msg); };
window.App = {
    Core: {},
    Collections: {},
    Controllers: {},
    Models: {},
    Views: {},
    Helpers: {},
    Routers: {},
    Widgets: {},
    Modules: {},
    Urls: {}
};