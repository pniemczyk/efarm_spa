﻿class window.App.Core.HttpTransaction
  name: 'HttpTransaction'
  ev:  window.Events
  constructor: (opts={}) ->
    @_configurationError('token is not configured') unless opts.token
    @_configurationError('commands is not configured') unless opts.commands
    @_token = opts.token
    @_commands = opts.commands

  defaultOptions: ->
    self: => @
    transactionName: 'default'
    dataType: 'json'
    method:   'GET'
    async:    false
    data:     {}
    beforeSend: (xhr) -> xhr.setRequestHeader('AUTHORIZATION', "token #{@self()._token}")
    error:      (request, status, error) ->  @self()._commands.execute("transaction-error", @transactionName, request.status, error)
    success:    (data) ->
      #TODO: respons data structure maybe is wraped by json_response
      @self()._commands.execute('transaction-message', @transactionName, data.message, data.status) if data.message?
      if data.status is 'ERROR'
        errors = JSON.stringify(data.errors)
        @self()._commands.execute("transaction-error", @transactionName, data.status, error)
      else
        @self()._commands.execute("transaction-success", @transactionName)

  request: (options={}) ->
    throw new URIError("INVALID URL for transaction: [#{options.transactionName}]") unless options.url?
    options = $.extend(@defaultOptions(), options)
    $.ajax(options)

  ajaxOverride: (ajaxFn) => ajaxFn = @getAjaxOverriden

  getAjaxOverriden: =>
    (options) =>
      fail = options.error
      done = options.success
      always = options.complete
      delete options.error
      delete options.success
      delete options.complete
      @request(options).done(done).fail(fail).always(always)

   _configurationError: (message) -> throw new Error("HttpTransaction configuration ERROR: #{message}")