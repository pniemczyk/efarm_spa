class window.App.Core.ModelProvider
  name: 'ModelProvider'
  constructor: (opts={}) ->
    @_initializationError('commands is not configured') unless opts.commands
    @_initializationError('reqres is not configured') unless opts.reqres
    @_initializationError('config is not configured') unless opts.config
    @_commands    = opts.commands
    @_reqres      = opts.reqres
    @_models      = opts.config.modelsNamespace
    @_collections = opts.config.collectionsNamespace
    @_registerEvents()

  _registerEvents: ->
    @_reqres.setHandler("Model-get", @getModel, @)
    @_commands.setHandler("Model-set", @setModel, @)
    @_reqres.setHandler("Collection-get", @getCollection, @)
    @_commands.setHandler("Collection-set", @setCollection, @)

  getModel:                  (name) -> @_models[name]
  setModel:           (name, model) -> @_models[name] = model
  getCollection:    (name, opts={}) =>
    return @_collections[name] if opts.withModel is false
    collection = @_collections[name]
    collection::model = @getModel(name)
    collection

  setCollection: (name, collection) -> @_collections[name] = collection

  _initializationError: (message) -> throw new Error("#{@name} initialization ERROR: #{message}")