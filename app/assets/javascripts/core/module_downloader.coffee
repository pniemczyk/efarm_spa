﻿class window.App.Core.ModuleDownloader
  name: 'ModuleDownloader'
  constructor: (opts={}) ->
    @_initializationError('reqres is not configured') unless opts.reqres
    @_reqres = opts.reqres
    @_url = @_reqres.request('Url-get', 'ModuleDownloaderUrl')
    @_initializationError('url is not configured') unless @_url
    @_registerReqres()

  get: (module) -> null

  _registerReqres: -> @_reqres.setHandler("downloadModule", @get, @)

  _initializationError: (message) -> throw new Error("#{@name} initialization ERROR: #{message}")