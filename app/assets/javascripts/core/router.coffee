﻿class window.App.Modules.Router extends Backbone.Router
  name: "Router"

  routes: {}

  initialize: (opts={}) ->
    throw new Error("#{@name} configuration ERROR: commands is not configured") unless opts.commands
    routes = opts.routes || {}
    @_commands = opts.commands
    Backbone.history.on('route', @_routerProxy, @);
    @routes = $.extend(routes, @_defaultRoutes)
    @_bindRoutes()

  addRoutes: (routes={}) =>
    (@addRoute(action, routes[action], false)) for action of routes
    @_bindRoutes()

  addRoute: (action, methodName, bind=true) =>
    @_defaultError("methodName `#{methodName}` is not allowed") if @_isDeniedMethodName(methodName)
    @_defaultError("methodName `#{methodName}` is already registered") if @_isRegisteredMethodName(methodName)
    @_defaultError("action `#{action}` is already registered") if @_routeActionIsPresent(action)
    newRoute = {}
    newRoute[action] = methodName
    @routes = $.extend(newRoute, @routes)
    @_bindRoutes() if bind

  removeRoute:(actionName) ->
    (delete @routes[action] if action is actionName) for action of @routes
    @off(actionName)
    @_bindRoutes()

  removeRouteByMethodName:(methodName) ->
    (
      if @routes[action] is methodName
        delete @routes[action]
        @off(action)
    ) for action of @routes
    @_bindRoutes()

  silentNavigate: (where, opts={}) ->
    opts = opts || {}
    @navigate(where, $.extend(true, opts, {trigger: false}))

  remove: ->
    @off()
    @routes = @_defaultRoutes
    @_bindRoutes()

  _routerProxy: (router, name, args) -> @_commands.execute('Settings-set', 'last_url', window.location.href)

  _defaultRoutes:
    "*actions": "defaultRoute"

  _isDeniedMethodName: (methodName) ->
    (return true if prop is methodName) for prop of @
    false
  _isRegisteredMethodName: (methodName) ->
    (return true if @routes[action] is methodName) for action of @routes
    false
  _routeActionIsPresent: (actionName) ->
    (return true if action is actionName) for action of @routes
    false

  _defaultError: (msg) -> throw new Error("#{@name} module addRoute ERROR: #{msg}")