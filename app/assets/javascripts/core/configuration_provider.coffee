class window.App.Core.ConfigurationProvider
  name: 'ConfigurationProvider'
  constructor: (opts={}) ->
    @_initializationError('commands is not configured') unless opts.commands
    @_initializationError('reqres is not configured') unless opts.reqres
    @_initializationError('config is not configured') unless opts.config
    @_initializationError('urls is not configured') unless opts.urls
    @_commands = opts.commands
    @_reqres = opts.reqres
    @_config = opts.config || {}
    @_urls = opts.urls || {}
    @_registerEvents()

  _registerEvents: ->
    @_reqres.setHandler("Configuration-get", @get, @)
    @_commands.setHandler("Configuration-set", @set, @)
    @_reqres.setHandler("Url-get", @getUrl, @)
    @_commands.setHandler("Url-set", @setUrl, @)

  getUrl: (name)      -> @_urls[name]
  setUrl: (name, url) -> @_urls[name] = url

  get: (namespace, name) ->
    return @_config[namespace] unless name
    @_config[namespace][name] if @_config[namespace]

  set: (namespace, name, object) ->
    return @_config[namespace] = object unless name
    @_config[namespace] = {} unless @_config[namespace]
    @_config[namespace][name] = object

  _initializationError: (message) -> throw new Error("#{@name} initialization ERROR: #{message}")