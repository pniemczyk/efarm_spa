﻿class window.App.Core.TemplateProvider
  name: "TemplateProvider"
  constructor: (opts={}) ->
    @_error('reqres is not configured') unless opts.reqres
    @_error('config is not configured') unless opts.config
    @_reqres = opts.reqres
    @_config = opts.config
    @_prefix = if @_config.templatePrefix then "#{@_config.templatePrefix}/" else ''
    @_registerEvents()

  _registerEvents: ->
    @_reqres.setHandler("#{@name}-get", @get, @)

  get: (name)->
    fullName = "#{@_prefix}#{name}"
    if @_config.templatesNamespace[fullName]
      @_config.templatesNamespace[fullName]
    else
      @download(name)

  download: (name) ->
    #TODO: Download template when is not present
  _error: (message, type='initialization') -> throw new Error("#{@name} #{type} ERROR: #{message}")