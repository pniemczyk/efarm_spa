﻿class window.App.Core.ErrorLogger
  name: 'ErrorLogger'
  _browser: window.navigator.userAgent

  _errorList: []
  _push: (errorObj) -> @_errorList.push errorObj

  _jsonToSend: ->
    objToSend =
      data:
        browser:  @_browser
        user:     @_config.user
        site:     window.location.href
        errors:   @_errorList
    JSON.stringify(objToSend)

  _netsend: ->
    if (window.XMLHttpRequest)
      xhr = new XMLHttpRequest()
      xhr.open(@_config.method, @_config.url, true)
      xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8")
      xhr.setRequestHeader("AUTHORIZATION", "token #{@_config.token}")
      xhr.send(@_jsonToSend())
      @_errorList = []

  _config:
    propagateErrors:  false
    url:              null
    method:           'POST'
    user:             null
    token:            null
    sendOnDomReady:   true
    sendOnlyOnDomReady:false
    sendDelay:        2000

  _setConfig: (config={}) -> @_config[prop] = config[prop] for prop of config when config.hasOwnProperty(prop)

  constructor: (config={}) ->
    @_configurationError('url is not configured') unless config.url
    @_setConfig(config)
    @_init()

  _init: ->
    window.onerror = @_errorHandler
    (window.onload = => @_afterDomReady(@)) if @_config.sendOnDomReady
    @_initSending() unless @_config.sendOnlyOnDomReady

  _initSending: =>
    run = =>
      @_send() unless @_config.sendOnDomReady
      setTimeout(run
                ,@_config.sendDelay)

    setTimeout(run
              ,@_config.sendDelay)

  _send: -> @_netsend() if @_errorList.length > 0

  _afterDomReady: (logger)->
    logger._send()
    logger._config.sendOnDomReady = false

  _errorHandler: (msg, url, line) =>
    url = window.location.href unless url is ''
    errorObj = {message:msg, url:url, line:line }
    @_push(errorObj)
    !@_config.propagateErrors

  _configurationError: (message) -> throw new Error("#{@name} configuration ERROR: #{message}")