﻿class window.App.Core.ModuleManager
  name: 'ModuleManager'
  constructor: (opts={})->
    @_initializationError('commands is not configured') unless opts.commands
    @_initializationError('reqres is not configured') unless opts.reqres
    @_initializationError('broker is not configured') unless opts.broker
    @_initializationError('transaction is not configured') unless opts.transaction
    @_initializationError('router is not configured') unless opts.router
    @_initializationError('moduleDownloader is not configured') unless opts.moduleDownloader
    @_initializationError('configurationProvider is not configured') unless opts.configurationProvider
    @_initializationError('modulesNamespace is not configured') unless opts.modulesNamespace

    @_modules = []
    @_commands = opts.commands
    @_reqres = opts.reqres
    @_broker = opts.broker
    @_transaction = opts.transaction
    @_router = opts.router
    @_moduleDownloader = opts.moduleDownloader
    @_configurationProvider = opts.configurationProvider
    @_modulesNamespace = opts.modulesNamespace

    @_registerEvents()

  _registerEvents: ->
    @_reqres.setHandler("#{@name}-get", @get, @)
    @_commands.setHandler("#{@name}-remove", @remove, @)
    @_commands.setHandler("#{@name}-add", @add, @)
    @_commands.setHandler("#{@name}-clear", @get, @)

  add: (moduleName) ->
    return if @isPresent(moduleName)
    moduleClass = @_getModuleClass(moduleName)
    return unless moduleClass
    opts = @_getRequirments(moduleClass)
    module = new moduleClass(opts, moduleClass.require()) #TODO: test initialization with require
    @_modules.push module
    @_addRoutes(module) if module.routes

  _getModuleClass: (moduleName) -> @_modulesNamespace[moduleName] || @_moduleDownloader.get(moduleName)

  _getRequirments: (moduleClass) ->
    opts = {}
    return opts unless moduleClass.require()
    require = moduleClass.require()
    opts.config = @_configurationProvider.get(require.config) if require.config
    opts.commands = @_commands if _.indexOf(require.libs, 'commands') isnt -1
    opts.broker = @_broker if _.indexOf(require.libs, 'broker') isnt -1
    opts.reqres = @_reqres if _.indexOf(require.libs, 'reqres') isnt -1
    opts.transaction = @_transaction if _.indexOf(require.libs, 'transaction') isnt -1
    opts.router = @_router if _.indexOf(require.libs, 'router') isnt -1
    opts

  addAsModule: (module) ->
    @_modules.push module unless @isPresent(module.name)
    @_addRoutes(module) if module.routes

  isPresent: (moduleName) -> @get(moduleName) isnt null

  remove: (moduleName) ->
    module = @get(moduleName)
    index = @_getIndex(module)
    @_removeRoutesForModule(module)
    @_modules[index].remove()
    @_modules.splice(index, 1)

  get: (moduleName) ->
    (return m if m.name is moduleName) for m in @_modules
    null

  clear: ->
    m.remove() for m in @_modules
    @_removeRoutesForModule(m) for m in @_modules
    @_modules = []

  unbindEvents: ->
    @_reqres.removeHandler("#{@name}-get")
    @_commands.removeHandler("#{@name}-remove")
    @_commands.removeHandler("#{@name}-add")
    @_commands.removeHandler("#{@name}-clear")

  _getIndex: (module) -> @_modules.indexOf(module)

  _router: null

  _addRoutes: (module) ->
    (
      method = module.routes[action]
      @_router.addRoute(action, method)
      @_router.on("route:#{method}", module[method])
    ) for action of module.routes

  _removeRoutesForModule: (module) ->
    return unless module.routes
    (
      method = module.routes[action]
      @_router.off("route:#{method}")
      @_router.removeRoute(action)
    )for action of module.routes

  _initializationError: (message) -> throw new Error("#{@name} initialization ERROR: #{message}")