﻿class window.App.Core.Initializer
  name: 'CoreInitializer'
  constructor: (config, urls) ->
    @_configurationError('config is not configured') unless config
    @_configurationError('urls is not configured') unless urls
    @_config = config
    @_urls = urls

  _mmConfig: ->
    router: @_router
    broker: @_broker
    commands: @_commands
    reqres: @_reqres
    transaction: @_transaction
    moduleDownloader: @_moduleDownloader
    configurationProvider: @_configurationProvider
    modulesNamespace: @_configurationProvider.get('ModuleManager', 'modulesNamespace')

  _templateProviderConfig: ->
    reqres: @_reqres
    config: @_configurationProvider.get('TemplateProvider')

  _transactionConfig: ->  $.extend(@_configurationProvider.get('HttpTransaction'), commands: @_commands)
  _selfConfig: -> @_configurationProvider.get('CoreInitializer')

  _jsonResponse: (data) -> new window.App.Helpers.JsonResponse(data)

  init: ->
    @_configurationProvider = new window.App.Core.ConfigurationProvider(commands: @_commands, reqres: @_reqres, config: @_config, urls: @_urls)
    @_modelProvider = new window.App.Core.ModelProvider(commands: @_commands, reqres: @_reqres, config:@_configurationProvider.get('ModelProvider'))
    @_CountriesHelper = new window.App.Helpers.Countries(reqres: @_reqres)
    @_logger = new window.App.Core.ErrorLogger(@_configurationProvider.get('ErrorLogger'))
    @_router = new window.App.Modules.Router(commands: @_commands)
    @_moduleDownloader = new window.App.Core.ModuleDownloader(reqres: @_reqres)
    @_transaction = new window.App.Core.HttpTransaction(@_transactionConfig())
    window.App.transaction = @_transaction
    @_moduleManager = new window.App.Core.ModuleManager(@_mmConfig())
    window.App.module = (name) => @_moduleManager.get(name)
    @_templateProvider = new window.App.Core.TemplateProvider(@_templateProviderConfig())
    window.App.template = (name) => @_templateProvider.get(name)

    window.App.jsonResponse = @_jsonResponse
    @_initBaseEvents()
    @_backboneOverrode()

  registerBaseModules: (config={})->
    @_moduleRegistrationError('module manager is not initialized') unless @_moduleManager
    @_moduleManager.add('SettingManager')
    @_moduleManager.add('PageLoader')
    @_moduleManager.add('PageNavbar')
    @_moduleManager.add('PageSidebar')
    @_moduleManager.add('PageBreadcrumb')
    @_moduleManager.add('PageContent')

  registerModules: -> (@_moduleManager.add(m)) for m in @_selfConfig().modules

  startHistory: -> Backbone.history.start()

#Link: https://github.com/marionettejs/backbone.wreqr
  _broker:    new Backbone.Wreqr.EventAggregator()
  _commands:  new Backbone.Wreqr.Commands()
  _reqres:    new Backbone.Wreqr.RequestResponse()

  _backboneOverrode: ->
    Backbone.ajax = window.App.transaction.getAjaxOverriden()
    @_backboneSync = Backbone.sync
    Backbone.sync = @_sync

  _initBaseEvents: ->
    window.App.broker =   @_broker
    window.App.commands = @_commands
    window.App.reqres =   @_reqres
    window.App.Helpers.clickEventName = if $.fn.tap then "tap" else "click"

  _moduleRegistrationError: (message) -> throw new Error("#{@name} module registration ERROR: #{message}")
  _configurationError: (message) -> throw new Error("#{@name} configuration ERROR: #{message}")

  _sync: (method, model, options) =>
    success = options.success

    overrideSuccess = (response) ->
      resp = window.App.jsonResponse(response)
      if resp.pagination
        model.total = resp.pagination.total
        model.size = resp.pagination.size
        model.page = resp.pagination.page
      success(resp.data)

    options.success = overrideSuccess
    @_backboneSync(method, model, options)