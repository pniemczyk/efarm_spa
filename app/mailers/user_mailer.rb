# encoding: utf-8

class UserMailer < ActionMailer::Base
  default from: "noreply@synapsisproject.net"

  def invite_friends(user, emails)
    @user = user
    mail(:to => emails, :subject => "EFarm.pl - dostałeś zaproszenie od jednego z naszych użytkowników").deliver
  end

  def registration_confirmation(email, opts={})
    @url      = opts[:url]
    @help_url = opts[:help_url]
    @token    = opts[:token]
    mail(:to => email, :subject => "EFarm.pl - Potwierdzenie rejestracji konta.").deliver
  end

  def reset_password_confirmation(email, opts={})
    @url      = opts[:url]
    @help_url = opts[:help_url]
    @token    = opts[:token]
    mail(:to => email, :subject => "EFarm.pl - Resetowanie hasła.").deliver
  end

end