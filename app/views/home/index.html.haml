- @settings = user_settings(current_user)

- content_for :scripts do
  :coffeescript
    propagateErrors = "#{env}" is 'development'

    window.App.config =
      ErrorLogger:
        propagateErrors: propagateErrors
        url: "#{api_logger_error_url}"
        user: "#{current_user.email}"
        token: "#{current_user.auth_token}"

      HttpTransaction:
        token: "#{current_user.auth_token}"

      ModuleManager:
        modulesNamespace: window.App.Modules

      ModelProvider:
        modelsNamespace: window.App.Models
        collectionsNamespace: window.App.Collections

      CoreInitializer:
        modules: ['InviteFriends','Season', 'Pagination', 'Dashboard', 'Profile', 'Farm', 'FieldCard', 'ParcelsService', 'Geometry', 'SearchLocation', 'Map', 'WeatherService']

      PageSidebar:
        minimized: #{@settings['ui_sidebar_minimized'] || false}

      Map: {}

      ParcelsService: {}

      TemplateProvider:
        templatesNamespace: window.HandlebarsTemplates || window.tpl
        templatePrefix: if window.HandlebarsTemplates? then 'app/templates' else null

      SettingManager:
        data: #{@settings.to_json}

    window.App.urls =
      ModuleDownloaderUrl: 'none'
      InviteFriendsUrl: "#{api_send_email_invite_friends_url}"
      SeasonUrl: 'none'
      ProfileOwnUrl: "#{api_profiles_own_url}"
      FarmsUrl: "#{api_farms_index_url}"
      SeasonsUrl: "#{api_seasons_index_url}"
      SettingsUrl: "#{api_settings_url}"
      ParcelsUrl: "#{api_parcels_url}"
      FieldsUrl: "#{api_fields_index_url(fis_id: ':FIS_ID')}"
      DefaultAvatarImageUrl: "#{image_path('themes/avatars/avatar_default.jpg')}"
      SearchLocation:
        LocationByNameUrl: 'http://nominatim.openstreetmap.org/search'
        LocationByGeoUrl: 'http://nominatim.openstreetmap.org/reverse'
      SearchPrecinct:
        BaseUrl: "#{api_search_precinct_url}"
        ByCodeUrl: "#{api_search_precinct_by_code_url}"


    initializer = new window.App.Core.Initializer(window.App.config, window.App.urls)
    initializer.init()
    initializer.registerBaseModules()
    initializer.registerModules()
    initializer.startHistory()