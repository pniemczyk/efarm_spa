# encoding: utf-8

class AccountsController < ApplicationController
  layout 'empty'

  before_filter :authorize, only: [:login]

  #TODO: We should move this to the translation yaml files
  MESSAGES = {
    account_created: 'Konto utworzone. Odbierz email z linkiem potwierdzającym rejestrację',
    account_creation_fail: 'Konto nie zostało utworzone',
    account_reset_password_email_sent: 'Odbierz email aby zmienić swoje hasło',
    account_reset_password_email_send_fail: 'Hasło nie może zostać zresetowane.',
    account_password_changed: 'Hasło zmienione'
  }

  def index
  end

  def login
    session[:token] = @current_user.auth_token
    redirect_to :controller => 'home', :action => 'wizard'
  end

  def logout
    authorize_by_token

    @current_user.auth_token = nil
    @current_user.auth_token_expire_at = nil
    @current_user.save
    session[:token] = nil
  rescue Authenticate::AuthFailError
  end

  def register
    registration = Accounts::Registration.new(params[:registration])

    if registration.valid?
      user = registration.register
      UserMailer.registration_confirmation(user.email, url: confirm_email_url(user.confirmation_token), help_url: help_url)

      render_success_json(messages_for_status(:account_created), nil)
    else
      json = {
        status: Consts::Response::ERROR,
        message: messages_for_status(:account_creation_fail),
        errors: registration.errors.first[1],
        data: params
      }

      render json: json
    end
  end

  def confirm_email
    confirmation = Accounts::EmailConfirmation.new(confirmation_token: params[:confirmation_token])

    if confirmation.valid?
      @current_user = confirmation.confirm
      session[:token] = @current_user.auth_token
      redirect_to :controller => 'home', :action => 'wizard'
    else
      flash[:alert] = confirmation.errors.first[1]
      redirect_to :login_and_register_page
    end
  end

  def forgot_password
    password_forgotten = Accounts::PasswordForgotten.new(params[:forgotPassword])

    if password_forgotten.valid?
      user = password_forgotten.reset_password

      opts = {
        url: reset_password_start_url(user.reset_password_token),
        help_url: help_url
      }
      UserMailer.reset_password_confirmation(user.email, opts)
      render_success_json(messages_for_status(:account_reset_password_email_sent), nil)
    else
      json = {
        status: Consts::Response::ERROR,
        message: messages_for_status(:account_reset_password_email_send_fail),
        errors: password_forgotten.errors.first[1],
        data: params
      }

      render json: json
    end
  end

  def reset_password_start
    reset_password = Accounts::ResetPasswordStart.new(params[:reset_password_token])

    if reset_password.valid?
      proffer current_user: reset_password.user
    else
      flash[:alert] = reset_password.errors.first[1]
      redirect_to :login_and_register_page
    end
  end

  def reset_password_finish
    reset_password = Accounts::ResetPasswordFinish.new(params[:reset_password_token], params[:resetPassword])

    if reset_password.valid?
      reset_password.reset!
      flash[:notice] = messages_for_status(:account_password_changed)
      redirect_to :login_and_register_page
    else
      flash[:alert] = reset_password.errors.first[1]
      redirect_to reset_password_start_url(reset_password.user.try(:reset_password_token))
    end
  end

  private

  def messages_for_status(stat)
    MESSAGES[stat]
  end
end
