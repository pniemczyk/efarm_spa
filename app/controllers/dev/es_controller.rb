class Dev::EsController < ApplicationController
  def plants
    query = {
      body: {
        query: {
          query_string: {
            fields: ['name^1.4', 'final_name^1.1', 'botanical_name^0.5'],
            query: params[:q]
          }
        }
      }
    }

    query_es(:plants, query)
  end

  def precinct
    query = {
      body: {
        query: {
          query_string: {
            fields: ['province', 'district', 'community', 'name'],
            query: params[:q],
          }
        }
      }
    }

    query_es(:precinct, query)
  end

  def precinct_fields
    query = {
      body: {
        query: {
          query_string: {
            fields: params[:fields].split(','),
            query: params[:q],
          }
        }
      }
    }

    query_es(:precinct, query)
  end

  def agri
    query = {
      body: {
        query: {
          multi_match: {
            fields: ['code^1.2', 'name'],
            query: params[:q]
          }
        }
      }
    }

    query_es(:agri_pack, query)
  end

  private

  def query_es(type, query)
    render json: {res: dicts_repo.search(type, query)}
  rescue => e
    render json: {error: e.message}
  end

  def dicts_repo
    @dicts_repo ||= DictsRepo.new(es_client: es_client)
  end

  def es_config
    @es_config ||= Rails.application.config.custom[:elasticsearch]
  end

  def es_client
    @es_client ||= Elasticsearch::Client.new(hosts: es_config[:hosts])#, log: (Rails.env == 'development') )
  end
end