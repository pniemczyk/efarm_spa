class HomeController < ApplicationController
  before_filter :authorize_by_token
  layout 'empty', :only => [:wizard]

  def index
    seasons, farms = Set.new, Set.new

    FarmInSeason.joins(:user_on_farms).where(user_on_farms: {user_id: @current_user.id}).map do |i|
      farms.add({name: i.farm.shortname, id: i.farm.id})
      seasons.add({name: i.season.name, id: i.season.id})
    end

    proffer current_user: @current_user, seasons: seasons, farms: farms, env: Rails.env
  end

  def wizard
    if FarmInSeason.joins(:user_on_farms).where(user_on_farms: {user_id: @current_user.id}).count > 0
      return redirect_to :controller => 'home', :action => 'index'
    end
    current_year = DateTime.now.year
    proffer token: session[:token], current_year: current_year, current_user: @current_user
  end

  def wizard_completed
    session[:token] = params[:token]
    first_year_of_season, farm_name, farm_shortname = params[:season], params[:name], params[:short_name]
    season = Season.where(first_year: first_year_of_season.to_i).first
    farm = Farm.create(name: farm_name, shortname: farm_shortname)

    fis = FarmInSeason.new.tap do |fis|
      fis.season_id = season.id
      fis.farm_id   = farm.id
      fis.active    = true

      fis.save
    end

    UserOnFarm.new.tap do |uof|
      uof.user = @current_user
      uof.farm_in_season = fis
      uof.role = FarmRoles::OWNER
    end.save

    UserSettingsUpdater.update(@current_user, 'fis_id' => fis.id)
    @current_user.save
    redirect_to :controller => 'home', :action => 'index'
  end

  def help
  end
end

