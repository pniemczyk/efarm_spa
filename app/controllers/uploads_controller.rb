# encoding: utf-8

class UploadsController < ApplicationController
  def form
  end

  def confirm
    upload = ShapeUpload.new(@current_user, params)
    if upload.save_file!
      render json: {data: upload.read_shapes.as_json}
    else
      render json: {errors: upload.errors}
    end
  rescue => e
    puts e.message
    render json: {errors: [e.message]}
  end
end

