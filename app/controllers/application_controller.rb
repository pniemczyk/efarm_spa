class ApplicationController < ActionController::Base
  include Proffer
  include ApplicationHelper

  protect_from_forgery

  rescue_from Authenticate::AuthFailError do |e|
    flash[:alert] = e.message
    redirect_to :controller => 'accounts', :action => 'index'
  end

  def render_error_json(http_status, msg, data, errors=[])
    json = {
      status: 'ERROR',
      message: msg,
      errors: errors,
      data: data
    }

    render json: json, status: http_status
  end

  def render_success_json(message=nil, data={})
    json = {
      status: 'OK',
      message: message,
      data: data
    }

    render json: json, status: 200
  end

  protected

  def authorize
    @current_user = authenticate.by_email(params[:email], params[:password])
  end

  def authorize_by_token
    token = session[:token] || params[:token]
    @current_user = authenticate.by_token(token)
  end

  def authenticate
    @authenticate ||= Authenticate.new
  end
end
