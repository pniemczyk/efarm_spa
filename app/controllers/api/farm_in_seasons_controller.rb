# encoding: utf-8

class Api::FarmInSeasonsController < Api::BaseController
  def active_deactive
    farm_in_season = FarmInSeason.where(id: params[:id], user_id: @current_user.id)

    if farm_in_season
      farm_in_season.active = params[:active].to_s.downcase == 'active'
      farm_in_season.save

      json_success(data: farm_in_season)
    else
      json_error(errors: ['Nie można znaleźć gospodarstwa'])
    end
  end
end