# encoding: utf-8
class Api::FieldsController < Api::BaseController

  def index
    json_success(data: Field.q.user_fields_from_fis(@current_user, params[:fis_id]))
  end

  def create
    create_field = Field::Create.new(@current_user, params)

    if create_field.execute
      json_success(data: create_field.data, infos: :created)
    else
      json_error(errors: create_field.errors)
    end
  end

  def update
    update_field = Field::Update.new(@current_user, params)

    if update_field.execute
      json_success(data: update_field.data, infos: :updated)
    else
      json_error(errors: update_field.errors)
    end
  end

  def delete
    delete_field = Field::Delete.new(@current_user, params)

    if delete_field.execute
      json_success(data: delete_field.data, infos: :deleted)
    else
      json_error(errors: delete_field.errors)
    end
  end
end