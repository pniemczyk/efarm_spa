class Api::SeasonsController < Api::BaseController

  def index
    json_success(paginate_collection: Season.q.all_for_user(@current_user).paginate(pagination))
  end

  def season_farms
    if user_season
      json_sucess(data: user_season_farms)
    else
      json_error(error: ['Brak gospodarstw w tym sezonie'])
    end
  end

  private

  def user_season
    @user_season ||= Season.q.for_user(@current_user, params[:season_id])
  end

  def user_season_farms
    user_season.farm_in_seasons.map do |fis|
      {
        farm_in_season_id: fis.id,
        season_id: fis.season_id,
        name: fis.farm.name,
        active: fis.active
      }
    end
  end
end
