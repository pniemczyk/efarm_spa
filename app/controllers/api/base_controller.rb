class Api::BaseController < ActionController::Base
  before_filter :authorize

  protected

  attr_reader :current_user

  def authorize
    @current_user = authenticate.by_token(auth_token)
  rescue
    authorize_fail
  end

  def authorize_fail
    #return request data and login request in json response
  end

  def authenticate
    @authenticate ||= Authenticate.new
  end

  def auth_token
    auth_header = env['HTTP_AUTHORIZATION']
    if auth_header =~ /token/i
      auth_header.split(' ').last
    end
  end

  def json_success(opts={})
    opts[:status] = Consts::Response::OK
    collection = opts[:paginate_collection]
    response = JsonResponse.new(opts.except(:paginate_collection))
    response.collection_with_pagination(collection, params[:page], params[:size]) if collection
    render json: response
  end

  def json_error(opts={})
    opts[:status] = Consts::Response::ERROR
    render json: JsonResponse.new(opts)
  end

  def create_by_params(class_name)
    class_name.new(params.select{ |k, _| class_name.attributes.include?(k.to_sym)})
  end

  def pagination
    {
      page: params[:page],
      per_page: params[:size]
    }
  end

  def custom_config
    @custom_config ||= Rails.application.config.custom
  end

  def dicts_repo
    @dicts_repo ||= DictsRepo.new(es_client: es_client)
  end

  def es_client
    @es_client ||= Elasticsearch::Client.new(hosts: custom_config[:elasticsearch][:hosts])#, log: (Rails.env == 'development') )
  end
end
