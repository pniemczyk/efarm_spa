class Api::FilesUploadController < Api::BaseController
  def upload_shape
    upload = ShapeUpload.new(@current_user, params)
    if upload.save_file!
      res = upload.read_shapes
      res ? json_success(data: res.as_json) : json_error(errors: upload.errors)
    else
      json_error(errors: upload.errors)
    end
  rescue => e
    puts e.message
    json_error(errors: [e.message])
  end
end