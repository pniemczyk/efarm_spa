# encoding: utf-8

class Api::SettingsController < Api::BaseController

  def index
    json_success(data: JSON.parse(@current_user.ui_settings))
  end

  def update
    UserSettingsUpdater.update(@current_user, params[:setting])

    if @current_user.save
      json_success(data: @current_user.ui_settings)
    else
      json_error(errors:['Zapisanie ustawień nie powiodło się'])
    end
  end
end