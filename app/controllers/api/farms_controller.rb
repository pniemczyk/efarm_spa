# encoding: utf-8

class Api::FarmsController < Api::BaseController
  def index
    json_success(paginate_collection: user_farms)
  end

  def show
    if user_farm
      json_success(data: {farm: user_farm})
    else
      json_error(errors: ['Nie można znaleźć gospodarstwa'])
    end
  end

  def create
    create_farm = Farm::Create.new(@current_user, params)

    if create_farm.execute
      json_success(data: create_farm.data, infos: :created)
    else
      json_error(errors: create_farm.errors)
    end
  end

  def update
    update_farm = Farm::Update.new(@current_user, params)
    if update_farm.execute
      json_success(data: update_farm.data, infos: :created)
    else
      json_error(errors: update_farm.errors)
    end
  end

  def farm_seasons
    if user_farm
      json_success(data: {seasons: user_farm_seasons})
    else
      json_error(errors: ['Nie można znaleźć gospodarstwa'])
    end
  end

  private

  def user_farm
    @user_farm ||= Farm.q.for_user(@current_user, params[:id])
  end

  def user_farms
    @user_farms ||= Farm.q.all_for_user(@current_user).paginate(pagination)
  end

  def user_farm_seasons
    user_farm.farm_in_seasons.map do |fis|
      {
        farm_in_season_id: fis.id,
        season_id: fis.season_id,
        name: fis.season.name,
        active: fis.active
      }
    end
  end
end