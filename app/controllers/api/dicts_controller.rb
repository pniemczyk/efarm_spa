class Api::DictsController < Api::BaseController
  def get_precinct
    json_success(data: dicts_repo.get(:precinct, params.fetch(:id)))
  end

  def search_precinct_by_code
    return json_success(data: []) if params[:q].blank?

    json_success(data: dicts_repo.search(:precinct, Dicts::Q.search_precinct_by_code(params)))
  end

  def search_precinct
    return json_success(data: []) if params[:q].blank?

    json_success(data: dicts_repo.search(:precinct, Dicts::Q.search_precinct(params)))
  end

  def search_plants
    return json_success(data: []) if params[:q].blank?

    json_success(data: dicts_repo.search(:plants, Dicts::Q.search_plants(params)))
  end

  def search_agri_pack
    return json_success(data: []) if params[:q].blank?

    json_success(data: dicts_repo.search(:agri_pack, Dicts::Q.search_agri_pack(params)))
  end
end
