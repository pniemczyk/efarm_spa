# encoding: utf-8

class Api::ProfilesController < Api::BaseController

  def show_own
    json_success(data: profile)
  end

  def update_own
    profile = create_by_params(Profile)
    UserUpdater.by_profile(@current_user, profile)
    @current_user.save

    json_success(data: profile)
  end

  def change_password
    change_password = create_by_params(Accounts::ChangePassword)
    change_password.old_encrypted_password = @current_user.encrypted_password

    if change_password.valid?
      @current_user.encrypted_password = change_password.encrypted_password
      @current_user.save
      json_success(message: "hasło zostało zmienione")
    else
      json_error(data: change_password.errors, message: 'hasło nie zostało zmienione')
    end
  end

  private

  def profile
    ProfilePresenter.profile(@current_user)
  end
end
