class Api::SendMailsController < Api::BaseController

  def invite_friends
    emails = params[:emails] || []
    return json_success(message: 'NO-SEND') if emails.empty?

    #TODO Add emails to user as connections
    UserMailer.invite_friends(current_user, emails)
    json_success(message: 'OK')
  end
end
