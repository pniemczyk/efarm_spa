class Api::ParcelsController < Api::BaseController
  def by_geo
    parcels = Services::ParcelsService.new(http_client).find(JSON.parse(params[:data], symbolize_names: true))
    json_success(data: updated_parcels(parcels))
  end

  private

  def http_client
    RestClient
  end
  # TODO: Create class from this method :D
  def updated_parcels(parcels)
    precincts = {}
    parcels.map do |p|
      code = (p['attributes'] || {})['IDENTYFIKATOR']
      next p unless code
      id                   = code.split('.')[0..1].join('.')
      precincts[id]        = get_precinct(id) unless precincts[id]
      next p if precincts[id].empty?
      p['attributes'].tap do |attrs|
        attrs['WOJEWODZTWO'] = precincts[id]['province']
        attrs['POWIAT']      = precincts[id]['district']
        attrs['GMINA']       = precincts[id]['community']
        attrs['OBREB']       = precincts[id]['name']
      end
      p
    end
  end

  def get_precinct(id)
    dicts_repo.get(:precinct, id)['_source'] || {}
  rescue #Elasticsearch::Transport::Transport::Errors::NotFound
    {}
  end

end
