class Api::LoggerController < Api::BaseController
  def error
    Rails.logger.error prepare_message
    newrelic_log if Rails.env == 'production'

    render :json => {status: 'OK'}
  end

  private

  def newrelic_log
    NewRelic::Agent.notice_error(StandardError.new("Javascript Error on #{error_data['site']}"), error_data)
  end

  def error_data
    params['data']
  end
  def prepare_message
    "\nJAVASCRIPT ERROR: on '#{error_data['site']}', source: #{error_data.to_json}"
  end
end