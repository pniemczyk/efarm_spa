class DictsRepo
  class DictsHashStructureValidationError < StandardError; end
  class DocumentTypeValidationError < StandardError; end
  INDEX = 'dicts'
  TYPES = [:plants, :precinct, :agri_pack]
  VALIDATORS = {
    plants:{
      type: 'string', #typ
      name: 'string', # nazwa, np burak
      english_name: 'string', # burak po angielsku
      botanical_name: 'string', # burak po lacinie
      varietie: 'array' # np czerwonek
    },
    precinct:{ # obreby
      province:  'string', # wojewodztwo
      district:  'string', # powiat
      community: 'string', # gmina
      name:      'string', # nazwa obrebu
      code:      'string'  # kod
    },
    agri_pack:{
      code: 'string',
      name: 'string'
    }
  }

  def initialize(opts={})
    @es_client = opts.fetch(:es_client)
  end

  def create_index
    es_client.indices.create(index: INDEX, body: index_definition)
    true
  rescue Elasticsearch::Transport::Transport::Errors::BadRequest => e
    false
  end

  def delete_index
    es_client.indices.delete(index: INDEX)
    true
  rescue Elasticsearch::Transport::Transport::Errors::NotFound
    false
  end

  def insert(type, opts={})
    raise DocumentTypeValidationError.new(type) unless TYPES.include?(type)
    body, id = opts.fetch(:body), opts[:id]
    validator = HashValidator.validate(body, VALIDATORS[type])
    raise DictsHashStructureValidationError.new(source: body, errors: validator.errors) unless validator.valid?
    params = { index: INDEX, type: type, body: body }
    params.merge!({id: id}) if id
    es_client.index(params)
  end

  def clear_data_from_type(type)
    raise DocumentTypeValidationError.new(type) unless TYPES.include?(type)
    es_client.delete_by_query(index: INDEX, type: type, body: {match_all:{}})
  end

  def delete(type, id)
    raise DocumentTypeValidationError.new(type) unless TYPES.include?(type)
    es_client.delete(index: INDEX, type: type, id: id)
  end

  def count(type)
    raise DocumentTypeValidationError.new(type) unless TYPES.include?(type)
    es_client.count(index: INDEX, type: type)['count'] || 0
  end

  def search(type, opts={})
    raise DocumentTypeValidationError.new(type) unless TYPES.include?(type)
    present_search_results es_client.search(opts.merge({index: INDEX, type: type}))
  end

  def get(type, id)
    raise DocumentTypeValidationError.new(type) unless TYPES.include?(type)
    es_client.get(index: INDEX, type: type, id: id)
  end

  def refresh
    es_client.indices.refresh(index: INDEX)
  end

  private
  attr_reader :es_client

  def present_search_results(data)
    ((data['hits'] || {})['hits'] || []).map{ |elem| elem['_source'] }
  end

  def index_definition
    {
      settings: {
        index: {
          number_of_shards: 1,
          refresh_interval: -1
        },
        analysis: {
          filter: {
            long_ngram: {
              type: 'nGram',
              min_gram: 3,
              max_gram: 8
            },
            ngram: {
              type: 'nGram',
              min_gram: 3,
              max_gram: 6
            },
            short_ngram: {
              type: 'nGram',
              min_gram: 2,
              max_gram: 5
            },
            tiny_ngram: {
              type: 'nGram',
              min_gram: 1,
              max_gram: 3
            }
          },
          analyzer: {
            keyword: {
              tokenizer: 'keyword',
              filter: ['lowercase', 'asciifolding'],
              type: 'custom'
            },
            keyword_ngram: {
              tokenizer: 'keyword',
              filter: ['lowercase', 'asciifolding', 'ngram'],
              type: 'custom'
            },
            long_ngram: {
              tokenizer: 'standard',
              filter: ['standard', 'lowercase', 'asciifolding', 'long_ngram'],
              type: 'custom'
            },
            ngram: {
              tokenizer: 'standard',
              filter: ['standard', 'lowercase', 'asciifolding', 'ngram'],
              type: 'custom'
            },
            short_ngram: {
              tokenizer: 'standard',
              filter: ['standard', 'lowercase', 'asciifolding', 'short_ngram'],
              type: 'custom'
            },
            number: {
              tokenizer: 'standard',
              filter: ['standard', 'tiny_ngram'],
              type: 'custom'
            }
          }
        }
      },
      mappings: {
        plants: {
          properties:{
            type: {
              type: 'string',
              index: 'not_analyzed'
            },
            name: {
              type: 'string',
              index_analyzer: 'long_ngram',
              search_analyzer: 'long_ngram'
            },
            english_name: {
              type: 'string',
              index: 'not_analyzed'
            },
            botanical_name: {
              type: 'string',
              index_analyzer: 'ngram',
              search_analyzer: 'ngram'
            },
            varietie:{
              properties:{
                breeding_name: {
                  type: 'string',
                  index_analyzer: 'keyword_ngram',
                  search_analyzer: 'keyword_ngram',
                },
                final_name: {
                  type: 'string',
                  index_analyzer: 'long_ngram',
                  search_analyzer: 'long_ngram'
                }
              }
            }
          }
        },
        agri_pack: {
          properties:{
            code:{
              type: 'string',
              index_analyzer: 'number',
              search_analyzer: 'number'
            },
            name:{
              type: 'string',
              index_analyzer: 'ngram',
              search_analyzer: 'short_ngram'
            }
          }
        },
        precinct:{
          properties:{
            province: {
              type: 'string',
              index_analyzer: 'keyword',
              search_analyzer: 'short_ngram'
            },
            district: {
              type: 'string',
              index_analyzer: 'long_ngram',
              search_analyzer: 'ngram'
            },
            community:{
              type: 'string',
              index_analyzer: 'long_ngram',
              search_analyzer: 'keyword_ngram'
            },
            name: {
              type: 'string',
              index_analyzer: 'long_ngram',
              search_analyzer: 'keyword_ngram'
            },
            code: {
              type: 'string',
              index: 'not_analyzed'
            }
          }
        }
      }
    }
  end
end