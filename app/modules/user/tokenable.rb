module Modules
  module User
    module Tokenable
      def auth_token_not_expired?
        self.auth_token_expire_at.present? && self.auth_token_expire_at > DateTime.now
      end

      def extend_auth_token_in_days(days_count)
        self.auth_token_expire_at = DateTime.now + days_count.days

      end
    end
  end
end