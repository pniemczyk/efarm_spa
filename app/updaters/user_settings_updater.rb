class UserSettingsUpdater

  ATTRIBUTES = [:current_season_id, :current_farm_id, :ui_sidebar_minimized, :last_url, :fis_id, :last_map_state_on_fields]

  def self.update(user, settings)
    user_settings = user.ui_settings.blank? ? {} : JSON.parse(user.ui_settings)
    user.ui_settings = {}.tap do |h|
      ATTRIBUTES.each do |attr|
        attr = attr.to_s
        h[attr] = settings[attr].nil? ? user_settings[attr] : settings[attr]
      end
    end.to_json
  end
end