class UserUpdater
  class << self
    UPDATABLE_BY_PROFILE_ATTRIBUTES = [:first_name, :last_name, :nick, :is_man, :image_url]

    def by_profile(user, profile)
      UPDATABLE_BY_PROFILE_ATTRIBUTES.each do |attr|
        user.public_send("#{attr.to_s}=", profile.public_send(attr))
      end
    end
  end
end