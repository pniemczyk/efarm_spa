class UserSettingsPresenter
  ATTRIBUTES = [:current_season_id, :current_farm_id, :ui_sidebar_minimized, :last_url, :fis_id, :last_map_state_on_fields]

  def self.as_json(settings)
    settings = if settings.respond_to?(:blank?) && settings.blank?
      {}
    elsif settings.kind_of?(String)
      JSON.parse(settings)
    elsif settings.kind_of?(Hash)
      settings.as_json
    else
      {}
    end

    {}.tap do |h|
      ATTRIBUTES.each do |attr|
        h[attr] = settings[attr.to_s]
      end
    end.as_json
  end
end