class ProfilePresenter
  class << self
    def profile(current_user)
      Profile.new(current_user.as_json(only: Profile.attributes))
    end
  end
end