class User < ActiveRecord::Base
  has_many :user_on_farms
  has_many :farm_in_seasons, through: :user_on_farms

  attr_accessible :first_name, :last_name, :nick, :is_man, :image_url, :ui_settings

  attr_protected :email, :encrypted_password
  attr_protected :failed_attempts, :locked_at
  attr_protected :auth_token, :auth_token_expire_at
  attr_protected :confirmation_token, :confirmed_at
  attr_protected :reset_password_token, :reset_password_token_expire_at
end
