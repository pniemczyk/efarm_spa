class Crop < ActiveRecord::Base
  has_many :fields, through: :crops_on_fields

  attr_accessible :expected_distribution, :field_capacity_growth, :forecrop, :germination, :is_own_seed, :name, :number_of_certified_seed, :variety_name, :weight_of_thousand_seeds
end
