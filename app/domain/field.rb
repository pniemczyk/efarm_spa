class Field < ActiveRecord::Base
  extend QField

  has_many :parcels
  has_many :crops, through: :crops_on_fields
  belongs_to :farm_in_season

  accepts_nested_attributes_for :parcels

  attr_accessible :name, :proper_name, :area, :growing_area, :geo_json, :usage, :parcels_attributes
  attr_protected :root_id, :active
end
