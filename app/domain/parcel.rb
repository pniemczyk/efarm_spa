class Parcel < ActiveRecord::Base
  belongs_to :field

  attr_accessible :code, :area, :area_in_ha, :area_used, :area_used_in_ha, :place
end