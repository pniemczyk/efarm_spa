class UserOnFarm < ActiveRecord::Base
  set_table_name 'user_on_farms'

  belongs_to :user
  belongs_to :farm_in_season

  attr_protected :role
end