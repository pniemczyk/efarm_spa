class Farm < ActiveRecord::Base
  extend QFarm

  has_many :farm_in_seasons
  has_many :seasons, through: :farm_in_seasons
  has_many :user_on_farms, through: :farm_in_seasons

  attr_accessible :name, :shortname
  attr_accessible :city, :country, :country_code, :number, :street, :post_code
  attr_accessible :nip, :bank_account, :regon, :arimr, :pesel
end
