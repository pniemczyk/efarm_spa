class FarmInSeason < ActiveRecord::Base
  extend QFarmInSeason

  set_table_name 'farm_in_seasons'
  belongs_to :farm
  belongs_to :season
  has_many   :fields
  has_many :user_on_farms
  has_many :users, through: :user_on_farms

  attr_accessible :active
  attr_protected  :farm_id, :season_id
end