class Season < ActiveRecord::Base
  extend QSeason
  has_many :farm_in_seasons
  has_many :farms, through: :farm_in_seasons
  has_many :user_on_farms, through: :farm_in_seasons

  attr_accessible :name, :first_year
end
