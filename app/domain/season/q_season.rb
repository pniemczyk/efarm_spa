class Season
  module QSeason
    class Queries
      def self.all_for_user(user)
        Season.joins(:user_on_farms).where(user_on_farms: {user_id: user.id}).uniq
      end

      def self.for_user(user, season_id)
        Season.joins(:user_on_farms).where(id: season_id, user_on_farms: {user_id: user.id}).first
      end
    end

    def q
      q ||= Queries
    end
  end
end