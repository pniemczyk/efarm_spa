class Field
  module QField
    class Queries
      def self.user_fields_from_fis(user, fis_id, opts={})
        where_opts = {farm_in_season_id: fis_id, active: true, user_on_farms: {farm_in_season_id: fis_id, user_id: user.id}}
        where_opts[:user_on_farms][:role] = opts[:role] if opts[:role]

        fields = Field.includes(:parcels, farm_in_season: :user_on_farms).where(where_opts).to_a.tap { |f| after_query(f) }.as_json(:include => :parcels)
      end

      def self.user_field_by_id(user, opts={})
        fis_id = opts.fetch(:fis_id)
        where_opts = {
          id: opts.fetch(:field_id),
          active: true,
          'farm_in_seasons.id' => fis_id,
          'user_on_farms.farm_in_season_id' => fis_id,
          user_on_farms: {user_id: user.id}
        }
        where_opts[:user_on_farms][:role] = opts[:role] if opts[:role]

        Field.includes(:farm_in_season => :user_on_farms).where(where_opts).first.tap { |f| after_query(f) }
      end

      private

      def self.after_query(val)
        if val.is_a?(Array)
          val.each { |f| f.geo_json = JSON.parse(f.geo_json) if f.geo_json.present? }
        else
          val.geo_json = JSON.parse(val.geo_json) if val and val.geo_json.present?
        end
      end
    end

    def q
      @q ||= Queries
    end
  end
end