class CropsOnFields < ActiveRecord::Base
  belongs_to :fields
  belongs_to :crops
  has_one    :dict_qualifications_degree

  attr_accessible :is_main_crop, :area
end