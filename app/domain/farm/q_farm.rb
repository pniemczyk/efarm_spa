class Farm
  module QFarm
    class Queries
      def self.all_for_user(user)
        Farm.joins(:user_on_farms).where(user_on_farms: {user_id: user.id}).uniq
      end

      def self.for_user(user, farm_id)
        Farm.joins(:user_on_farms).where(id: farm_id, user_on_farms: {user_id: user.id}).first
      end
    end

    def q
      @q ||= Queries
    end
  end
end