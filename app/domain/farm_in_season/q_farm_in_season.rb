class FarmInSeason
  module QFarmInSeason
    class Queries
      def self.user_farm(user, fis_id, opts={})
        where_opts = {id: fis_id, user_on_farms: {user_id: user.id}}
        where_opts[:user_on_farms][:role] = opts[:role] if opts[:role]

        FarmInSeason.joins(:user_on_farms).where(where_opts).first
      end
    end

    def q
      q ||= Queries
    end
  end
end