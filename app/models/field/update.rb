class Field
  class Update
    include BaseAction

    def field
      @field ||= user_field
    end

    def execute
      return false unless all_preconditions?
      update!
      errors.blank?
    end

    def data
      field.reload
      {
        id: field.id,
        parcels: field.parcels
      }
    end

    private

    def all_preconditions?
      check_if_field_exist

      errors.blank?
    end

    def check_if_field_exist
      errors << 'Nie można znaleźć pola do zmiany, lub brak uprawnień' if field.blank?
    end

    def update!
      remove_parcels_no_longer_needed!

      return errors << field.errors unless (field.update_attributes(field_opts))
    rescue => e
      errors << e.message
    end

    def remove_parcels_no_longer_needed!
      ids_to_keep = field_opts[:parcels_attributes].map { |pa| pa[:id] }.compact
      field.parcels.select { |parcel| not ids_to_keep.include?(parcel.id) }.each(&:destroy)
    end
  end
end