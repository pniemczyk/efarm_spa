# encoding: utf-8
class Field
  class Create
    include BaseAction

    def field
      @field ||= Field.new(field_opts)
    end

    def execute
      return false unless all_preconditions?
      save!
      errors.blank?
    end

    private

    def all_preconditions?
      check_if_farm_in_season

      errors.blank?
    end

    def check_if_farm_in_season
      errors << 'Brak farmy dla wskazanych kryteriów, lub brak uprawnień do zmiany pola' if farm_in_season.blank?
    end

    def save!
      farm_in_season.fields << field
      field.root_id = field.id
      field.save
    rescue => e
      errors << e.message
    end
  end
end