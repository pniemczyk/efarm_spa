class Field
  module BaseAction
    attr_accessor :field, :errors
    attr_reader :user, :params

    def initialize(user, params)
      @user, @params = user, params
    end

    def errors
      @errors ||= []
    end

    def farm_in_season
      @farm_in_season ||= FarmInSeason.q.user_farm(user, params[:fis_id], {role: FarmRoles::OWNER})
    end

    def user_field
      @user_field = Field.q.user_field_by_id(user, {field_id: params[:id], fis_id: params[:fis_id], role: FarmRoles::OWNER})
    end

    def data
      {
        id: field.id
      }
    end

    def field_opts
      @field_opts ||= (params[:field] || {}).tap do |p|
        p[:geo_json] = p[:geo_json].present? ? p[:geo_json].to_json : nil
        p[:parcels_attributes]  = (params[:parcels] || []).map { |e| e.slice(:id, :code, :area, :area_in_ha, :area_used, :area_used_in_ha, :place) }
        p.delete(:root_id)
      end
    end
  end
end