class Field
  class Delete
    include BaseAction

    def field
      @field ||= user_field
    end

    def execute
      return false unless all_preconditions?
      delete!
      errors.blank?
    end

    private

    def all_preconditions?
      check_if_field_exist

      errors.blank?
    end

    def check_if_field_exist
      errors << 'Nie można znaleźć pola do zmiany, lub brak uprawnień' if field.blank?
    end

    def delete!
      field.active = false
      field.geo_json = nil
      return errors << field.errors unless (field.save)
    rescue => e
      errors << e.message
    end
  end
end