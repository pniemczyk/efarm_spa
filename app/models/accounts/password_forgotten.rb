 module Accounts
  class PasswordForgotten
    include ActiveModel::Model

    attr_accessor :email

    validate :user_for_email, :user_status

    def reset_password
      user.reset_password_token = EncryptorDecryptor.generate_token
      user.reset_password_token_expire_at = Time.now + Consts::PASSWORD_RESET_TOKEN_DAYS
      user.save
      user
    end

    private

    def user
      @user ||= User.where("email =?", email.downcase).first
    end

    def user_for_email
      errors.add(:user, 'Brak konta dla podanego adresu email') unless user.present?
    end

    def user_status
      errors.add(:user, 'Konto zablokowane') if user.present? and user.locked_at.present?
    end
  end
end