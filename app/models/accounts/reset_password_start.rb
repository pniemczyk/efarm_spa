# encoding: utf-8

module Accounts
  class ResetPasswordStart
    include ActiveModel::Model

    attr_accessor :reset_password_token

    validate :user_for_token, :user_status

    def initialize(reset_password_token)
      @reset_password_token = reset_password_token
    end

    def user
      @user ||= User.where("reset_password_token =?", reset_password_token).first
    end

    private

    def user_for_token
      errors.add(:user, 'Konto nie zostało znalezione') unless user.present?
    end

    def user_status
      return unless user.present?

      if user.reset_password_token_expire_at.present? and user.reset_password_token_expire_at < Time.now
        errors.add(:user, 'Upłynął czas na zresetowanie hasła')
      end

      errors.add(:user, 'Konto zablokowane') if user.locked_at.present?
    end
  end
end