# encoding: UTF-8

module Accounts
  class Registration
    include ActiveModel::Model

    attr_accessor :email, :password, :password_confirmation, :accept_terms

    validates_presence_of :email, message: 'Podaj email'
    validates_presence_of :password, message: 'Podaj hasło'
    validates_presence_of :password_confirmation, message: 'Powtórz hasło'

    validates_format_of :email, :with => Consts::EMAIL_REGEX, message: 'Email ma nieprawidłowy format'
    validate :email_not_taken, :passwords_are_equal, :terms_accepted

    def register
      new_user.save
      new_user
    end

    private

    def email_not_taken
      user = User.where("email =?", email.downcase).first

      if user.present?
        errors.add(:email, 'Konto o podanym adresie email już istnieje.')
      end
    end

    def passwords_are_equal
      errors.add(:password, 'Hasło i jego powtórzenie muszą być identyczne') unless password == password_confirmation
    end

    def terms_accepted
      errors.add(:accept_terms, 'Musisz zaakceptować regulamin') unless accept_terms.try(:downcase) == 'on'
    end

    def new_user
      @new_user ||= User.new.tap do|user|
        user.email = email.downcase
        user.encrypted_password = EncryptorDecryptor.generate_password(password)
        user.confirmation_token = EncryptorDecryptor.generate_token
      end
    end
  end
end