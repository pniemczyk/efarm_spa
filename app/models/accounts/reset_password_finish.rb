# encoding: utf-8

module Accounts
  class ResetPasswordFinish
    include ActiveModel::Model

    attr_accessor :reset_password_token, :password, :password_confirmation

    validates_presence_of :password, message: 'Podaj hasło'
    validates_presence_of :password_confirmation, message: 'Powtórz hasło'
    validate :user_for_token, :user_status

    def initialize(reset_password_token, opts={})
      super(opts)

      @reset_password_token = reset_password_token
    end

    def reset!
      user.encrypted_password = EncryptorDecryptor::generate_password(password)
      user.reset_password_token = nil
      user.reset_password_token_expire_at = nil
      user.failed_attempts = 0
      user.save
      user
    end

    private

    def user
      @user ||= User.where("reset_password_token =?", reset_password_token).first
    end

    def user_for_token
      errors.add(:user, 'Konto nie zostało znalezione') unless user.present?
    end

    def user_status
      return unless user.present?

      if user.reset_password_token_expire_at.present? and user.reset_password_token_expire_at < Time.now
        errors.add(:user, 'Upłynął czas na zresetowanie hasła')
      end

      errors.add(:user, 'Konto zablokowane') if user.locked_at.present?
    end

    def password_policy
      unless password == password_confirmation
        errors.add(:password, 'Hasło i jego powtórzenie muszą być takie same')
      end
    end
  end
end