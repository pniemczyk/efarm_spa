# encoding: utf-8

module Accounts
  class ChangePassword < Light::Model
    class MissingVariableError < StandardError; end

    attributes :old_password, :password, :password_confirmation
    attr_accessor :old_encrypted_password

    def encrypted_password
      @encrypted_password ||= generate_password(password)
    end

    validates :password, :presence => {message: 'hasło jest wymagane' }, :confirmation => {message: 'hasła nie zgadają się'}
    validate  :old_password_equality

    private

    def old_password_equality
      errors.add(:old_password, "hasło jest niepoprawne") unless old_passwords_equal?
    end

    def generate_password(password)
      EncryptorDecryptor.generate_password(password)
    end

    def old_passwords_equal?
      raise MissingVariableError.new('#old_encrypted_password missing') unless old_encrypted_password
      EncryptorDecryptor.passwords_equal?(old_encrypted_password, old_password)
    end

  end
end