# encoding: UTF-8
module Accounts
  class EmailConfirmation
    include ActiveModel::Model

    attr_accessor :confirmation_token

    validates_presence_of :confirmation_token, message: 'Brak tokena potwierdzającego'
    validate :user_for_token, :user_not_confirmed_yet

    def confirm
      user.tap do |u|
        u.extend(::Modules::User::Tokenable)
        u.auth_token = EncryptorDecryptor.generate_token
        u.extend_auth_token_in_days(Consts::AUTH_TOKEN_EXTEND_DAYS)
        u.confirmed_at = DateTime.now
        u.confirmation_token = nil

        u.save
      end
    end

    private

    def user_not_confirmed_yet
      if user && user.confirmed_at
        errors.add(:confirmation_token, 'Konto zostało już potwierdzone')
      end
    end

    def user_for_token
      unless user
        errors.add(:confirmation_token, 'Nie znaleziono konta oczekującego na potwierdzenie')
      end
    end

    def user
      @user ||= User.where("confirmation_token =?", confirmation_token).first
    end
  end
end