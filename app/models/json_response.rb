class JsonResponse < Light::Model
  attributes :data, :status, :errors, :infos, :message, :command, :pagination

  def collection_with_pagination(collection, page, size)
    @data = collection.to_a
    size ||= collection.klass.per_page
    page ||= 1

    @pagination = {
      page: page.to_i,
      size: size.to_i,
      total: @data.total_entries
    }

    self
  end
end