# encoding: UTF-8
class Authenticate
  class AuthFailError < StandardError; end

  AUTH_TOKEN_EXTEND_DAYS = 1
  MAX_PASSWORD_FAILURES  = 5

  def by_token(token)
    user = User.where("auth_token =?", token).first

    fail('Zaloguj ponownie') unless user.present?
    fail('Konto zablokowane')   if user.locked_at

    user.extend(::Modules::User::Tokenable)
    auth_token_not_expired?(user) ? user : fail('Zaloguj ponownie')
  end

  def by_email(email, password)
    user = User.where("email =?", email).first

    fail('Niepoprawny login lub hasło.') unless user.present?
    fail('Konto zablokowane')   if user.locked_at

    valid_password?(user, password) ? user : fail('Nieprawidłowe hasło')
  end

  private

  def valid_password?(user, password)
    if valid = EncryptorDecryptor.passwords_equal?(user.encrypted_password, password)
      user.extend(Modules::User::Tokenable)
      user.failed_attempts = 0
      user.auth_token = EncryptorDecryptor.generate_token
      user.extend_auth_token_in_days(AUTH_TOKEN_EXTEND_DAYS)
    else
      user.failed_attempts += 1
      user.locked_at = DateTime.now if user.failed_attempts >= MAX_PASSWORD_FAILURES
    end

    user.save
    valid
  end

  def auth_token_not_expired?(user)
    unless result = user.auth_token_not_expired?
      user.extend_auth_token_in_days(AUTH_TOKEN_EXTEND_DAYS)
      user.save
    end

    result
  end

  def fail(message)
    raise AuthFailError.new(message)
  end
end