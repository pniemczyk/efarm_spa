class Farm
  class Create < BaseAction
    def execute
      errors << 'Niepoprawny sezon' unless season
      errors << farm.errors unless (farm.valid? && farm.save)

      return false unless errors.blank?
      create_farm_in_seasons
    end

    def farm
      @farm ||= Farm.new(params[:farm])
    end

    private
    def create_farm_in_seasons
      farm_in_seasons do |season_id, active|
        FarmInSeason.create(season_id: season_id, farm_id: farm.id, user_id: user.id, active: active)
      end
      true
    end
  end
end