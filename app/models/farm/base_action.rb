class Farm

  private
  class BaseAction
    attr_accessor :errors, :farm
    alias :data :farm

    def initialize(user, params)
      @user, @params = user, params
    end

    def errors
      @errors ||= []
    end

    def season
      @season ||= Season.where(first_year: params[:season]).first
    end

    private
    attr_accessor :user, :params
    delegate :seasons, :to => :user, :prefix => true

    def user_season_first_year
      @user_season_first_year ||= user_seasons.present? ? user_seasons[0].first_year : season.first_year
    end

    def user_season_last_year
      @user_season_last_year ||= user_seasons.present? ? user_seasons[-1].first_year : season.first_year
    end

    def seasons_years_avail_for_user
      start_year = season.first_year > user_season_first_year ? user_season_first_year : season.first_year
      last_year  = season.first_year > user_season_first_year  ? season.first_year : user_season_first_year
      (start_year .. last_year)
    end

    def all_seasons
      @all_seasons ||= Season.all
    end

    def farm_in_seasons
      seasons_years_avail_for_user.each do |year|
        active = year >= season.first_year
        season_id = all_seasons.find { |s| s.first_year == year }.id
        yield(season_id, active)
      end
    end
  end
end
