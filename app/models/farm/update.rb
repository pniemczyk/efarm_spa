class Farm
  class Update < BaseAction
    def execute
      errors << 'Niepoprawny sezon' unless season
      errors << farm.errors unless farm
      update_farm_in_seasons
    end

    def farm
      @farm ||= Farm.update(params[:id], params[:farm])
    end

    private

    def update_farm_in_seasons
      farm_in_seasons do |season_id, active|
        farm_in_season = FarmInSeason.where(season_id: season_id, farm_id: farm.id, user_id: user.id).first
        FarmInSeason.update(farm_in_season.id, season_id: season_id, farm_id: farm.id, user_id: user.id, active: active)
      end
      true
    end
  end
end