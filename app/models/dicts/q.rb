module Dicts
  module Q
    def self.search_precinct(params)
      {
        body: {
          query: {
            query_string: {
              fields: params[:fields].present? ? params[:fields].split(',') : ['name', 'community'],
              query: params[:q],
            }
          }
        }
      }
    end

    def self.search_precinct_by_code(params)
      {
        body: {
          query: {
            filtered: {
              query: { match_all: {} },
              filter: {
                regexp: {
                  code: "#{params[:q]}.*"
                }
              }
            }
          }
        }
      }
    end

    def self.search_plants(params)
      {
        body: {
          query: {
            query_string: {
              fields: ['name^1.4', 'final_name^1.1', 'botanical_name^0.5'],
              query: params[:q]
            }
          }
        }
      }
    end

    def self.search_agri_pack(params)
      {
        body: {
          query: {
            multi_match: {
              fields: ['code^1.2', 'name'],
              query: params[:q]
            }
          }
        }
      }
    end
  end
end