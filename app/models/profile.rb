class Profile < Light::Model
  attributes :email, :first_name, :last_name, :nick, :is_man, :image_url, :created_at
end