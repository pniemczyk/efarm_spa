module ApplicationHelper
  def user_settings(user)
    user.blank? ? {} : UserSettingsPresenter.as_json(user.ui_settings)
  end
end