# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140307224421) do

  create_table "crops", :force => true do |t|
    t.string   "name"
    t.string   "variety_name"
    t.string   "forecrop"
    t.float    "weight_of_thousand_seeds"
    t.float    "germination"
    t.float    "field_capacity_growth"
    t.float    "expected_distribution"
    t.float    "number_of_certified_seed"
    t.boolean  "is_own_seed"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "crops_on_fields", :force => true do |t|
    t.integer "crops_id"
    t.integer "fields_id"
    t.float   "area"
    t.boolean "is_main_crop"
  end

  create_table "farm_in_seasons", :force => true do |t|
    t.integer "farm_id"
    t.integer "season_id"
    t.boolean "active"
  end

  create_table "farms", :force => true do |t|
    t.string   "name"
    t.string   "shortname"
    t.string   "nip"
    t.string   "regon"
    t.string   "bank_account"
    t.string   "arimr"
    t.string   "pesel"
    t.string   "country"
    t.string   "country_code"
    t.string   "city"
    t.string   "post_code"
    t.string   "street"
    t.string   "number"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "fields", :force => true do |t|
    t.string   "name"
    t.string   "proper_name"
    t.float    "area"
    t.float    "growing_area"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.integer  "farm_in_season_id",                   :null => false
    t.text     "geo_json"
    t.string   "usage"
    t.integer  "root_id"
    t.boolean  "active",            :default => true
  end

  create_table "parcels", :force => true do |t|
    t.float    "area"
    t.string   "code"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "field_id",        :null => false
    t.float    "area_used"
    t.float    "area_in_ha"
    t.float    "area_used_in_ha"
    t.string   "place"
  end

  create_table "seasons", :force => true do |t|
    t.string  "name"
    t.integer "first_year"
  end

  add_index "seasons", ["first_year"], :name => "index_seasons_on_first_year", :unique => true
  add_index "seasons", ["name"], :name => "index_seasons_on_name", :unique => true

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "user_on_farms", :force => true do |t|
    t.integer  "user_id",           :null => false
    t.integer  "farm_in_season_id", :null => false
    t.string   "role"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",                    :default => "", :null => false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "nick"
    t.boolean  "is_man"
    t.string   "image_url"
    t.text     "ui_settings"
    t.string   "auth_token"
    t.datetime "auth_token_expire_at"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.string   "reset_password_token"
    t.datetime "reset_password_token_expire_at"
    t.string   "password_verification_token"
    t.datetime "password_verification_token_expire_at"
    t.integer  "failed_attempts",                       :default => 0
    t.datetime "locked_at"
    t.datetime "created_at",                                            :null => false
    t.datetime "updated_at",                                            :null => false
  end

  add_index "users", ["auth_token"], :name => "index_users_on_auth_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["password_verification_token"], :name => "index_users_on_password_verification_token", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
