# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# users
test   = User.create({email: 'test@efarm.com',                   encrypted_password: EncryptorDecryptor.generate_password('test'),   first_name: 'Test'})
tomasz = User.create({email: 'tomasz.efarm@gmail.com',           encrypted_password: EncryptorDecryptor.generate_password('tomasz'), first_name: 'Tomasz'})
janusz = User.create({email: 'janusz.efarm@gmail.com',           encrypted_password: EncryptorDecryptor.generate_password('janusz'), first_name: 'Janusz'})
rafal  = User.create({email: 'rafal.klimek@gmail.com',           encrypted_password: EncryptorDecryptor.generate_password('rafal'),  first_name: 'Rafal'})
pawel  = User.create({email: 'xkido@o2.pl',                      encrypted_password: EncryptorDecryptor.generate_password('pawel'),  first_name: 'Paweł'})
damian = User.create({email: 'damian.jaszczurowski@hotmail.com', encrypted_password: EncryptorDecryptor.generate_password('damian'), first_name: 'Damian'})
users = [test, tomasz, janusz, rafal, pawel, damian]

# farms
alfa    = Farm.create({name: 'Alfa',    shortname: 'jablko',  city: 'Wilkowice',   country: 'Polska', country_code: 'Pl', nip: '123456789', arimr: 'arimr_1554'})
beta    = Farm.create({name: 'Beta',    shortname: 'malina',  city: 'Bystra',      country: 'Polska', country_code: 'Pl', nip: '736555892', arimr: 'arimr_7733'})
gamma   = Farm.create({name: 'Gamma',   shortname: 'grusza',  city: 'Wilkowice',   country: 'Polska', country_code: 'Pl', nip: '166356443', arimr: 'arimr_9911'})
delta   = Farm.create({name: 'Delta',   shortname: 'marchew', city: 'Łodygowice',  country: 'Polska', country_code: 'Pl', nip: '990887273', arimr: 'arimr_9088'})
epsilon = Farm.create({name: 'Epsilon', shortname: 'burak',   city: 'Łodygowice',  country: 'Polska', country_code: 'Pl', nip: '944445273', arimr: 'arimr_0077'})
zeta    = Farm.create({name: 'Zelta',   shortname: 'por',     city: 'Rybarzowice', country: 'Polska', country_code: 'Pl', nip: '445166389', arimr: 'arimr_1134'})
farms = [alfa, beta, gamma, delta, epsilon, zeta]

farms_seasons_range = 2010..2016
farms_seasons = Season.where(first_year: farms_seasons_range)

farms_seasons.each do |season|
  active = season.first_year >= 2012

  farms.each do |farm|
    FarmInSeason.new.tap do |fis|
      fis.season_id = season.id
      fis.farm_id   = farm.id
      fis.active    = active

      fis.save
    end
  end
end

FarmInSeason.find(:all).each do |fis|
  users.each do |user|
    UserOnFarm.new.tap do |uof|
      uof.user = user
      uof.farm_in_season = fis
      uof.role = FarmRoles::OWNER
    end.save
  end
end