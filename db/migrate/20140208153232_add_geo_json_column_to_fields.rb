class AddGeoJsonColumnToFields < ActiveRecord::Migration
  def change
    add_column :fields, :geo_json, :text
  end
end
