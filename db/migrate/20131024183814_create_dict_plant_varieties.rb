class CreateDictPlantVarieties < ActiveRecord::Migration
  def change
    create_table :dict_plant_varieties do |t|
      t.string :breeding_name
      t.string :final_name

      t.belongs_to :dict_plant
    end
  end
end
