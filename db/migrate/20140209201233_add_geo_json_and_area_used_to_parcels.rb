class AddGeoJsonAndAreaUsedToParcels < ActiveRecord::Migration
  def change
    add_column :parcels, :area_used, :float
    add_column :parcels, :geo_json,  :text
  end
end
