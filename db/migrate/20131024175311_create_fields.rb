class CreateFields < ActiveRecord::Migration
  def change
    create_table :fields do |t|
      t.string   :name
      t.string   :literal_name
      t.float    :registry_area
      t.float    :growing_area
      t.boolean  :leased

      t.timestamps

      t.belongs_to :farms_in_seasons
    end
  end
end
