class RenameTerytNumberColumnToTeryt < ActiveRecord::Migration
  def up
    rename_column :parcels, :teryt_number, :teryt
  end

  def down
    rename_column :parcels, :teryt, :teryt_number
  end
end
