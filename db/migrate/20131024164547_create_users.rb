class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string   :email,              null: false, default: ""
      t.string   :encrypted_password, null: false, default: ""

      t.string   :first_name
      t.string   :last_name
      t.string   :nick
      t.boolean  :is_man
      t.string   :image_url
      t.text     :ui_settings

      t.string   :auth_token
      t.datetime :auth_token_expire_at

      t.string   :confirmation_token
      t.datetime :confirmed_at

      t.string   :reset_password_token
      t.datetime :reset_password_token_expire_at

      t.string   :password_verification_token
      t.datetime :password_verification_token_expire_at

      t.integer  :failed_attempts, :default => 0

      t.datetime :locked_at

      t.timestamps
    end

    add_index :users, :email,                       unique: true
    add_index :users, :reset_password_token,        unique: true
    add_index :users, :auth_token,                  unique: true
    add_index :users, :password_verification_token, unique: true

  end
end
