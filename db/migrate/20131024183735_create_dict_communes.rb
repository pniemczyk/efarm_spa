class CreateDictCommunes < ActiveRecord::Migration
  def change
    create_table :dict_communes do |t|
      t.string :name

      t.belongs_to :dict_district
    end
  end
end
