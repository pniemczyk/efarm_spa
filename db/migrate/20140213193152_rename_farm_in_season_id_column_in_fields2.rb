class RenameFarmInSeasonIdColumnInFields2 < ActiveRecord::Migration
  def up
    rename_column :fields, :farm_in_seasons_id, :farm_in_season_id
  end

  def down
  end
end
