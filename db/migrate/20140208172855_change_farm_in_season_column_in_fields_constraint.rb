class ChangeFarmInSeasonColumnInFieldsConstraint < ActiveRecord::Migration
  def up
    change_column :fields, :farms_in_seasons_id, :integer, :null => false
  end

  def down
  end
end
