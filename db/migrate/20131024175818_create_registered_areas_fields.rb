class CreateRegisteredAreasFields < ActiveRecord::Migration
  def change
    create_table :registered_areas_fields do |t|
      t.belongs_to :field
      t.belongs_to :registered_area
    end
  end
end
