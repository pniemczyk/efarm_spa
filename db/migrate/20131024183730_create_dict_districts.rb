class CreateDictDistricts < ActiveRecord::Migration
  def change
    create_table :dict_districts do |t|
      t.string :name

      t.belongs_to :dict_province
    end
  end
end
