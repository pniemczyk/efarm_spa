class AddUsageColumnToFields < ActiveRecord::Migration
  def change
    add_column :fields, :usage, :string
  end
end
