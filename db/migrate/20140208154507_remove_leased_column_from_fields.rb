class RemoveLeasedColumnFromFields < ActiveRecord::Migration
  def up
    remove_column :fields, :leased
  end

  def down
    add_column :fields, :leased
  end
end
