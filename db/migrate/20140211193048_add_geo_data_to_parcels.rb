class AddGeoDataToParcels < ActiveRecord::Migration
  def change
    add_column :parcels, :area_in_ha, :float
    add_column :parcels, :geo_len,    :float
    add_column :parcels, :geo_area,   :float
  end
end
