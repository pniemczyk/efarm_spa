class RemoveUserIdFromFis < ActiveRecord::Migration
  def up
    remove_column :farm_in_seasons, :user_id
  end

  def down
  end
end
