class AddActiveToField < ActiveRecord::Migration
  def change
    add_column :fields, :active, :boolean, default: true
  end
end
