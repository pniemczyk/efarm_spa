class CreateDictPlants < ActiveRecord::Migration
  def change
    create_table :dict_plants do |t|
      t.string :name
      t.string :english_name
      t.string :botanical_name
      t.string :kind

      t.timestamps
    end
  end
end
