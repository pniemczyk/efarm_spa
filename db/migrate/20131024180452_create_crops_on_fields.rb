class CreateCropsOnFields < ActiveRecord::Migration
  def change
    create_table :crops_on_fields do |t|
      t.belongs_to :crops
      t.belongs_to :fields

      t.float      :area
      t.boolean    :is_main_crop
    end
  end
end
