class DropRegisteredAreas < ActiveRecord::Migration
  def up
    drop_table :registered_areas_fields
  end

  def down
  end
end
