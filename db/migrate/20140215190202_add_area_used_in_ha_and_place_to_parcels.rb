class AddAreaUsedInHaAndPlaceToParcels < ActiveRecord::Migration
  def change
    add_column :parcels, :area_used_in_ha, :float
    add_column :parcels, :place, :string
  end
end
