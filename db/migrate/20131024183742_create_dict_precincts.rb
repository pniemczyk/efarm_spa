class CreateDictPrecincts < ActiveRecord::Migration
  def change
    create_table :dict_precincts do |t|
      t.string :name
      t.string :code

      t.belongs_to :dict_commune
    end
  end
end
