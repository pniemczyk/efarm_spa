class ChangeFieldsTableColumnNames < ActiveRecord::Migration
  def up
    rename_column :fields, :literal_name,  :proper_name
    rename_column :fields, :registry_area, :area
  end

  def down
    rename_column :fields, :proper_name, :literal_name
    rename_column :fields, :area,        :registry_area
  end
end
