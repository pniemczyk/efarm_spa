class CreateDictQualificationsDegrees < ActiveRecord::Migration
  def change
    create_table :dict_qualifications_degrees do |t|
      t.belongs_to :crops

      t.boolean :is_qualified
      t.string  :name
    end
  end
end
