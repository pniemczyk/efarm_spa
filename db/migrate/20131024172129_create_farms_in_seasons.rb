class CreateFarmsInSeasons < ActiveRecord::Migration
  def change
    create_table :farms_in_seasons do |t|
      t.belongs_to :farm
      t.belongs_to :season
      t.belongs_to :user
      t.boolean    :active
    end
  end
end