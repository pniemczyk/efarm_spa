class AddFieldIdColumnToParcel < ActiveRecord::Migration
  def change
    add_column :parcels, :field_id, :integer, null: false
  end
end
