class RenameParcelsRegisteredNumberAndSheetNumberColumns < ActiveRecord::Migration
  def up
    rename_column :parcels, :registered_number, :code
    rename_column :parcels, :sheet_number,      :number
  end

  def down
    rename_column :parcels, :code,   :registered_number
    rename_column :parcels, :number, :sheet_number
  end
end
