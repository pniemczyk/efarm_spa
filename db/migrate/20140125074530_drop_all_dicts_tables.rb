class DropAllDictsTables < ActiveRecord::Migration
  def up
    drop_table :dict_plant_varieties
    drop_table :dict_plants
    drop_table :dict_precincts
    drop_table :dict_communes
    drop_table :dict_districts
    drop_table :dict_provinces
    drop_table :dict_qualifications_degrees
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end