class RenameFarmsInSeasonsToFarmInSeasons < ActiveRecord::Migration
  def change
    rename_table :farms_in_seasons, :farm_in_seasons
  end
end
