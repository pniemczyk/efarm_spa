class CreateFarms < ActiveRecord::Migration
  def change
    create_table :farms do |t|
      t.string :name
      t.string :shortname
      t.string :nip
      t.string :regon
      t.string :bank_account
      t.string :arimr
      t.string :pesel
      t.string :country
      t.string :country_code
      t.string :city
      t.string :post_code
      t.string :street
      t.string :number

      t.timestamps
    end
  end
end
