class RenameFarmInSeasonIdColumnInFields < ActiveRecord::Migration
  def up
    rename_column :fields, :farms_in_seasons_id, :farm_in_seasons_id
  end

  def down
  end
end
