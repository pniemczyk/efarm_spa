class AddRootIdToField < ActiveRecord::Migration
  def change
    add_column :fields, :root_id, :integer
  end
end
