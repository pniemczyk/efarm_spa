class CreateSeasons < ActiveRecord::Migration
  def change
    create_table :seasons do |t|
      t.string  :name
      t.integer :first_year
    end

    add_index :seasons, :name,        unique: true
    add_index :seasons, :first_year,  unique: true

    (2000..2019).each do |year|
      name = "#{year}/#{year.next}"
      Season.create(
        name: name,
        first_year: year
      )
    end
  end
end
