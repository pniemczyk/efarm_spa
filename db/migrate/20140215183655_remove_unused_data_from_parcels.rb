class RemoveUnusedDataFromParcels < ActiveRecord::Migration
  def up
    remove_column :parcels, :province
    remove_column :parcels, :district
    remove_column :parcels, :community
    remove_column :parcels, :precinct
    remove_column :parcels, :number
    remove_column :parcels, :teryt
    remove_column :parcels, :geo_json
    remove_column :parcels, :geo_len
    remove_column :parcels, :geo_area
  end

  def down
  end
end
