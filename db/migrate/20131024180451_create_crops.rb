class CreateCrops < ActiveRecord::Migration
  def change
    create_table :crops do |t|
      t.string  :name
      t.string  :variety_name
      t.string  :forecrop
      t.float   :weight_of_thousand_seeds
      t.float   :germination
      t.float   :field_capacity_growth
      t.float   :expected_distribution
      t.float   :number_of_certified_seed
      t.boolean :is_own_seed

      t.timestamps
    end
  end
end
