class AddUserOnFarms < ActiveRecord::Migration
  def up
    create_table :user_on_farms do |t|
      t.integer :user_id,           null: false
      t.integer :farm_in_season_id, null: false
      t.string :role

      t.timestamps
    end
  end

  def down
    drop_table :user_on_farms
  end
end
