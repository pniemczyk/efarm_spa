class CreateRegisteredAreas < ActiveRecord::Migration
  def change
    create_table :registered_areas do |t|
      t.float  :area
      t.string :province
      t.string :district
      t.string :community
      t.string :precinct
      t.string :sheet_number
      t.string :registered_number
      t.string :teryt_number

      t.timestamps
    end
  end
end
