class RenameRegistereAreasTableToParcels < ActiveRecord::Migration
  def up
    rename_table :registered_areas, :parcels
  end

  def down
    rename_table :parcels, :registered_areas
  end
end
