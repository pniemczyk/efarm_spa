require 'fileutils'

class ShapeFolder
  SHP_EXTENSTIONS = ['shp', 'shx', 'dbf']

  def initialize(user)
    @user = user || OpenStruct.new(id: 'nieznany')
  end

  def can_save?(file)
    SHP_EXTENSTIONS.any? { |e| file.is?(e) } || file.is?('xml')
  end

  def save(file)
    path = save_path(file)
    FileUtils.mkdir_p(File.dirname(path)) unless File.exist?(File.dirname(path)) # create user folder if necessary
    File.open(path, 'wb') { |f| f.write(file.read) }
  end

  def all_files?(file)
    return true if file.is?('xml')
    SHP_EXTENSTIONS.all? { |e| File.exist?(path_to_file_with_ext(file.name, e)) }
  end

  def missing_files(file)
    return [] if file.is?('xml')
    SHP_EXTENSTIONS.select { |e| not File.exist?(path_to_file_with_ext(file.name, e)) }
  end

  def shp_file_path(file)
    path_to_file_with_ext(file.name, file.is?('xml') ? 'xml' : 'shp')
  end

  def cleanup(file)
    if file.is?('xml')
      File.delete(path_to_file_with_ext(file.name, 'xml'))
    else
      SHP_EXTENSTIONS.each { |e| File.delete(path_to_file_with_ext(file.name, e)) }
    end

    FileUtils.rm_rf(user_folder) if Dir.entries(user_folder).size == 2 # folder is empty
    FileUtils.rm_rf(yesterday_folder)
  rescue => e
    puts "could not cleanup #{e.message}"
  end

  private

  attr_reader :user, :extensions

  def user_folder
    @user_folder ||= "#{base_folder}/#{today}/#{user.id}/"
  end

  def yesterday_folder
    "#{base_folder}/#{yesterday}"
  end

  def today
    @today ||= "#{DateTime.now.day}#{DateTime.now.month}#{DateTime.now.year}"
  end

  def yesterday
    @yesterday ||= "#{DateTime.yesterday.day}#{DateTime.yesterday.month}#{DateTime.yesterday.year}"
  end

  def base_folder
    @base_folder ||= "#{Rails.root}/tmp/shp"
  end

  def save_path(file)
    @save_path ||= "#{user_folder}#{file.name}.#{file.extension}" # rails_root/tmp/shp/1/15102013/1/polygon.shp
  end

  def path_to_file_with_ext(file_name, e)
    "#{user_folder}#{file_name}.#{e}"
  end
end
