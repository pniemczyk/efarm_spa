module Toolkit
  class GeoJsonHelper
    POLYGON      = 'Polygon'
    LINE_STRING  = 'LineString'
    MULTIPOLYGON = 'MultiPolygon'
    UNKNOWN      = 'unknown'

    def self.detect_type(geo_json)
      coordinates = geo_json[:coordinates] || geo_json['coordinates'] || []

      return UNKNOWN     if coordinates.blank?
      return LINE_STRING if coordinates[0][0].is_a?(Float)
      return POLYGON     if coordinates[0][0][0].is_a?(Float)
      MULTIPOLYGON
    end

    def self.detect_projection(geo_json)
      coordinates = geo_json[:coordinates] || geo_json['coordinates'] || []

      case geo_json[:type]
      when UNKNOWN     then nil
      when LINE_STRING then projection_from_point(coordinates[0])
      when POLYGON     then projection_from_point(coordinates[0][0])
      else
        projection_from_point(coordinates[0][0][0])
      end
    end

    private

    def self.projection_from_point(point=[])
      case point[0].to_s.split('.')[0].length
      when 1..3 then '4326'
      when 6..8 then '2180'
      else nil # TODO add logging
      end
    end
  end
end