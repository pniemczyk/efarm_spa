module Consts
  EMAIL_REGEX = /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i
  AUTH_TOKEN_EXTEND_DAYS = 1.day
  PASSWORD_RESET_TOKEN_DAYS = 1.day
  module Response
    OK    = 'OK'
    ERROR = 'ERROR'
  end
end