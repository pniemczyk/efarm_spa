# encoding: utf-8

class ShapeUpload
  def initialize(user, params)
    @user, @params = user, params
  end

  def errors
    @errors ||= []
  end

  def save_file!
    if files.present? && files.all? { |f| f.ok? and shp_folder.can_save?(f) }
      begin
        files.each { |f| shp_folder.save(f) }
        true
      rescue => e
        errors << "Błąd zapisu. Komunikat błędu: #{e.message}"
        false
      end
    else
      errors << 'Uszkodzony plik(i) lub nieprawidłowy format'
      false
    end
  end

  def read_shapes
    file = files[0]

    if file.is?('xml')
      reader = XmlShpReader.new
    else
      unless shp_folder.all_files?(file)
        errors << "Brakuje plików o roszerzeniach #{shp_folder.missing_files(file)}"
        return false
      end

      reader = ShpReader.new
    end

    reader.read(shp_folder.shp_file_path(file)).tap do |shapes|
      shp_folder.cleanup(file)
    end
  end

  private

  attr_reader :params, :user

  def files
    @files ||= (params[:files] || []).map { |f| UploadFile.new(f) }
  end

  def shp_folder
    @shp_folder ||= ShapeFolder.new(user)
  end
end
