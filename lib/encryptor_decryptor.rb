require 'securerandom'
require 'bcrypt'

class EncryptorDecryptor
  def self.generate_token
    SecureRandom.base64(15).tr('+/=lIO0', 'pqrsxyz')
  end

  def self.generate_password(password)
    ::BCrypt::Password.create(password)
  end

  def self.passwords_equal?(encrypted_password, password)
    ::BCrypt::Password.new(encrypted_password) == password
  end
end