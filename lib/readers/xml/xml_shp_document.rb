module Xml
  class BaseSax < ::Ox::Sax
    def initialize
      @ancestors = []
    end

    def start_element(name)
      @ancestors.push(name)
    end

    def end_element(name)
      @ancestors.pop
    end

    protected

    def ancestors
      @ancestors
    end
  end

  class XmlShpDocument < BaseSax
    def initialize
      super
      @shapes = []
    end

    def start_element(name)
      super(name)

      case ancestors
      when [:gospodarstwo, :pole]
        @shape = Shape.new
      when [:gospodarstwo, :pole, :Pomiar]
        @measurement = []
      when [:gospodarstwo, :pole, :Pomiar, :punkt]
        @point = []
      end
    end

    def end_element(name)
      super(name)

      case name
      when :pole
        shape.geo_json[:type]   = Toolkit::GeoJsonHelper.detect_type(shape.geo_json)
        shape.info[:projection] = Toolkit::GeoJsonHelper.detect_projection(shape.geo_json)
        shapes << shape
      when :Pomiar
        measurement << measurement[0] unless measurement[0] == measurement[-1]
        shape.geo_json[:coordinates].push(measurement)
      when :punkt
        measurement.push(point) if ancestors == [:gospodarstwo, :pole, :Pomiar]
      end
    end

    def attr(name, value)
      super(name, value)

      case name
      when :nazwa
        shape.info[:pole_nazwa]   = value if ancestors == [:gospodarstwo, :pole]
        shape.info[:pomiar_nazwa] = value if ancestors == [:gospodarstwo, :pole, :Pomiar]
        shape.info[:typ_nazwa]    = value if ancestors == [:gospodarstwo, :pole, :Pomiar, :typ]
      when :longitude
        point << value.sub(',', '.').to_f
      when :latitude
        point << value.sub(',', '.').to_f
      end
    end

    def text(value)
    end

    def point
      @point ||= []
    end

    def measurement
      @measurement ||= []
    end

    def shape
      @shape ||= Shape.new
    end

    def shapes
      @shapes ||= []
    end
  end
end