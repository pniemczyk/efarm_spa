require 'ox'

class XmlShpReader
  def read(path_to_file)
    doc = Xml::XmlShpDocument.new

    File.open(path_to_file, 'r') do |f|
      Ox.sax_parse(doc, f, {symbolize: true})
      doc.shapes
    end
  rescue => e
    ap e
    []
  end
end