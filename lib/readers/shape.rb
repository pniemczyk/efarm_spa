class Shape < Light::Model
  attributes :id, :geo_json, :info

  def info
    @info ||= {}
  end

  def geo_json
    @geo_json ||= {
      type: 'unknown',
      coordinates: []
    }
  end
end