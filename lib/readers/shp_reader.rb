require 'rgeo/shapefile'
require 'rgeo/geo_json'

class ShpReader
  def read(path_to_file)
    [].tap do |records|
      RGeo::Shapefile::Reader.open(path_to_file) do |file|
        file.each do |record|
          shape = Shape.new(info: record.attributes || {}, geo_json: RGeo::GeoJSON.encode(record.geometry))
          shape.info[:projection] = Toolkit::GeoJsonHelper.detect_projection(shape.geo_json)

          records << shape
        end
      end
    end
  end
end