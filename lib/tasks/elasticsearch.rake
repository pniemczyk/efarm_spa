# rhc ssh -a elasticsearch '$OPENSHIFT_DATA_DIR/elasticsearch/bin/./elasticsearch &'
# rhc ssh -a elasticsearch 'killall -9 java'
# rhc ssh -a elasticsearch 'ps -A | grep java'
# try use require 'open3'
namespace :es do

  desc 'Create Indexes'
  task :create_indexes => :environment do
    puts "Creating indexes"
    res = dicts_repo.create_index
    res ? 'successed' : 'fail'
  end

  desc 'Drop Indexes'
  task :delete_indexes => :environment do
    puts "Deleting indexes"
    res = dicts_repo.delete_index
    res ? 'successed' : 'fail'
  end

  desc 'Recreate indexes'
  task :recreate_indexes => :environment do
    puts "Recreating indexes"
    dicts_repo.delete_index
    res = dicts_repo.create_index
    res ? 'successed' : 'fail'
  end

  desc 'Insert Dicts#Precinct'
  task :insert_dicts_precinct_data => :environment do
    puts 'Insert Dicts#Precinct'
    get_provinces_list.each do |data|
      build_precinct_from_province_for_es(data).each do |body|
        dicts_repo.insert(:precinct, id: body[:code], body: body)
      end
    end

    dicts_repo.refresh
    puts 'done'
  end

  desc 'Insert Dicts#Plants'
  task :insert_dicts_plants_data => :environment do
    puts 'Insert Dicts#Plants'
    dicts_repo.clear_data_from_type(:plants)
    get_plants_list.each do |data|
      build_plants_for_es(data).each do |body|
        dicts_repo.insert(:plants, body: body)
      end
    end

    dicts_repo.refresh
    puts 'done'
  end

  desc 'Insert Dicts#Agri_pack'
  task :insert_dicts_agri_pack_data => :environment do
    puts 'Insert Dicts#Agri_pack'
    dicts_repo.clear_data_from_type(:agri_pack)
    path = Rails.root.join('db', 'data', 'package_agri', 'packages.json')
    JSON.parse(IO.read(path)).each do |d|
      code = d['key']
      name = d['value'].gsub("#{code}.", '').strip
      dicts_repo.insert(:agri_pack, body: { code: code, name: name })
    end

    dicts_repo.refresh
    puts 'done'
  end

  def clear_plants_data
    dicts_repo.search()
  end

  def build_plants_for_es(plants)
    data = JSON.parse(IO.read(plants[:filepath]))
    kind_name = plants[:type]
    data.map do |k|
      {
        type: kind_name,
        name: k['Name'],
        english_name: k['EnglishName'],
        botanical_name: k['BotanicalName'],
        varietie: (k['Varieties'] || []).map do |v|
          {
            breeding_name: v['BreedingName'],
            final_name: v['FinalName']
          }
        end
      }
    end
  end

  def get_plants_list
    path = Rails.root.join('db', 'data', 'plants')
    Dir["#{path}/*.json"].map do |f|
      {filepath: f, type:File.basename(f).gsub('.json','')}
    end
  end

  def build_precinct_from_province_for_es(province)
    data = JSON.parse(IO.read(province[:filepath]))
    province_name = province[:name]
    [].tap do |result|
      data["powiaty"].each do |p|
        district_name = p["nazwa"]
        p["gminy"].each do |g|
          community_name = g["nazwa"]
          g['obreby'].each do |o|
            result << {
              province:  province_name,
              district:  district_name,
              community: community_name,
              name:      o['nazwa'],
              code:      o['kod']
            }
          end
        end
      end
    end
  end

  def get_provinces_list
    path = Rails.root.join('db', 'data', 'regions')
    Dir["#{path}/*.json"].map do |f|
      {filepath: f, name:File.basename(f).gsub('.json','')}
    end
  end

  def dicts_repo
    @dicts_repo ||= DictsRepo.new(es_client: es_client)
  end

  def es_config
    @es_config ||= Rails.application.config.custom[:elasticsearch]
  end

  def es_client
    @es_client ||= Elasticsearch::Client.new(hosts: es_config[:hosts])#, log: (Rails.env == 'development') )
  end

  namespace :service do
    desc 'Check elasticsearch service is running on openshift'
    task :check_es_on_os do
      p es_config
      sh "rhc ssh -a elasticsearch 'ps -A | grep java'" do |ok, res|
        p 'verification service: '
        p ok ? 'running' : 'stoped'
      end
    end

    desc 'Start elasticsearch service on openshift'
    task :start_es_on_os do
      sh  "rhc ssh -a elasticsearch '$OPENSHIFT_DATA_DIR/elasticsearch/bin/./elasticsearch &'" do |ok, res|
        p ok ? 'service started' : 'start service fail'
      end
      Rake::Task['es:service:check_es_on_os'].invoke
    end

    desc 'Stop elasticsearch service on openshift'
    task :stop_es_on_os do
      sh  "rhc ssh -a elasticsearch 'killall -9 java'"  do |ok, res|
        p ok ? 'stop service fail' : 'stop service successed'
      end
      Rake::Task['es:service:check_es_on_os'].invoke
    end

    desc 'Restart elasticsearch service on openshift'
    task :restart_es_on_os do
      sh  "rhc ssh -a elasticsearch 'killall -9 java'"  do |ok, res|
        p ok ? 'stop service fail' : 'stop service successed'
      end
      sh  "rhc ssh -a elasticsearch '$OPENSHIFT_DATA_DIR/elasticsearch/bin/./elasticsearch &'" do |ok, res|
        p ok ? 'service started' : 'start service fail'
      end
      Rake::Task['es:service:check_es_on_os'].invoke
    end
  end if Rails.env == 'development'
end