class UploadFile
  def initialize(file)
    @file = file
  end

  def ok?
    file.present? and file.kind_of?(ActionDispatch::Http::UploadedFile)
  end

  def name
    @name ||= original_filename.split('.')[0]
  end

  def extension
    @extension ||= original_filename.split('.')[-1]
  end

  def original_filename
    @original_filename ||= sanitaze(file.original_filename)
  end

  def read
    file.read
  end

  def is?(ext)
    extension.to_sym == ext.to_sym
  end

  private

  attr_reader :file

  def sanitaze(file_name)
    File.basename(file_name.to_s).sub(/[^\w\.\-]/,'_')
  end
end