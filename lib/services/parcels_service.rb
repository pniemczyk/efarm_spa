require 'uri'

module Services
  class ParcelsService
    GEOPORTAL_URL = Rails.application.config.custom[:geoportal_url]

    def initialize(http_client)
      @http_client = http_client
    end

    def find(opts={})
      res = JSON.parse(@http_client.get(URI.escape("#{GEOPORTAL_URL}#{geoportal_params(opts)}")))
      res['results'].present? ? res['results'] : []
    end

    private

    def geoportal_params(opts)
      params = {
        geometryType: geometry_type(opts[:esriGeometry]),
        geometry: opts[:esriGeometry].except(:spatialReference),
        sr: opts[:esriGeometry][:spatialReference],
        tolerance: opts[:tolerance],
        imageDisplay: '800,600,96',
        layers: 'top:0,1,2',
        mapExtent: opts[:mapExtent],
        f: 'json'
      }

      "geometryType=#{params[:geometryType]}&geometry=#{params[:geometry].to_json}&sr=#{params[:sr].to_json}&tolerance=#{params[:tolerance]}&imageDisplay=#{params[:imageDisplay]}&layers=#{params[:layers]}&mapExtent=#{params[:mapExtent]}&f=json"
    end

    def geometry_type(esri_geometry={})
      esri_geometry[:rings].present? ? 'esriGeometryPolygon' : esri_geometry[:points].present? ? 'esriGeometryMultipoint' : 'esriGeometryPoint'
    end
  end
end