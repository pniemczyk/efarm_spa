# EFarm.pl - best app for farmers

## Note

This project was started in 2012 and we abandoned his development in 2014.
It is opensource now. Keep in mind that this is old project and some components stop working. 
Enjoy !.

###Postgress installation:
link to tutorial: <https://help.ubuntu.com/community/PostgreSQL>

instalation on ubuntu:

    sudo apt-get install postgresql
    sudo apt-get install postgresql-client
    sudo apt-get install pgadmin3

db create:

    RAILS_ENV=development bundle exec rake db:create

links:

- <http://guides.rubyonrails.org/generators.html>
- <http://davidpots.com/blog/rails-basics-has-many-through/>

Generate scaffold:

    rails g scaffold User name:string

or

    rails g model User name:string

real example:

    rails g scaffold User first_name:string last_name:string nick:string email:string image_url:string ui_settings:string auth_token:string auth_token_expiration_date:datetime confirmation_token:string encrypted_password:string password_verification_token:string password_verification_token_expiration_date:datetime password_failures_since_last_success:integer is_man:boolean is_confirmed:boolean is_locked:boolean

active model genration types:

    :string, :text, :integer, :float, :decimal, :datetime, :timestamp, :time, :date, :binary, :boolean, :references

links:

- <http://api.rubyonrails.org/classes/ActiveRecord/ConnectionAdapters/TableDefinition.html#method-i-column>
- <https://github.com/rails-api/rails-api>
- <http://guides.rubyonrails.org/association_basics.html>
- <http://guides.rubyonrails.org/getting_started.html#associating-models>
- <http://api.rubyonrails.org/classes/ActiveRecord/ConnectionAdapters/TableDefinition.html#method-i-column>
- <http://guides.rubyonrails.org/active_record_querying.html>
- <http://edgeguides.rubyonrails.org/active_record_basics.html>

### Connection data(development env)

    database:   efarm
    port:       5432
    user:       efarm
    password:   sedes

# instalacja PostGis
1. zainstaluj postgresql
2. zainstaluj GEOS
  2.1 sciagnij z http://download.osgeo.org/geos/geos-3.4.2.tar.bz2
  2.2 rozpakuj i przejdz do katalogu
  2.3 postepuj zgodnie z (zacznij og naglowka Configure a dokladniej zdania Now, run configuration script) http://trac.osgeo.org/geos/wiki/BuildingOnUnixWithAutotools
3. zainstaluj PROJ
  3.1 sciagnij z http://download.osgeo.org/proj/proj-4.8.0.tar.gz
  3.2 postepuj identycznie jak z GEOS (./configure | make  | sudo make install)
4. sudo apt-get install postgresql-9.1-postgis
5. sudo apt-get install postgresql-contrib
6. sudo apt-get install libgdal1-dev
7. sudo apt-get install postgresql-server-dev-9.1
8. zainstaluj Postgis 2.0.4
  8.1 sciagnij z http://download.osgeo.org/postgis/source/postgis-2.0.4.tar.gz
  8.2 zainstaluj jak GEOS (./configure | make  | sudo make install)

9. bundle

10. su postgres (Twoje haslo to prawdopodobnie postgres)
11. psql efarm
Kolejne po prostu przeklejaj
12. DROP EXTENSION PostGIS;
13. CREATE SCHEMA postgis;
14. CREATE EXTENSION PostGIS WITH SCHEMA postgis;
15. GRANT ALL ON postgis.geometry_columns TO PUBLIC;
16. GRANT ALL ON postgis.spatial_ref_sys TO PUBLIC;

Teraz upewnij sie ze masz wylaczony serwer aplikacji i pgadmin
17. bundle exec rake db:drop
18. bundle exec rake db:create
19. bundle exec rake db:migrate
20. teraz baza bedzie czysta - opcjonalnie mozecie sobie uruchomic bundle exec rake db:seed. bedziecie miec konta na adresy email z pliku seeds.rb. Haslami sa wasze imiona.
