﻿class window.BackboneWreqrFactory
  class FakeBroker
    constructor: -> @names = []
    on: (name, fn)-> @names.push(name)
    off: (name) -> (@names.splice(@names.indexOf(n), 1)) for n in @names
    clear: -> @names = []
    length: -> @names.length
    isPresent: (name) ->
      return true if e is name for e in @names
      false
  
  class FakeWreqr
    constructor: ->
      @names = []
      @requests = []
    setHandler: (name, fn)-> @names.push(name)
    removeHandler: (name) -> (@names.splice(@names.indexOf(n), 1)) for n in @names
    removeAllHandlers: -> @names = []
    length: -> @names.length
    request: (name)-> @requests.push name
    isPresent: (name) ->
      return true if e is name for e in @names
      false
    clear: -> 
      @names = []
      @requests = []

  fakeBroker: -> new FakeBroker()
  fakeCommands: -> new FakeWreqr()
  fakeReqRes: -> new FakeWreqr()
