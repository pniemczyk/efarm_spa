#= require core/configuration_manager

describe "ConfigurationManager class", ->
  bFac = new window.BackboneWreqrFactory()
  commands = bFac.fakeCommands()
  reqres = bFac.fakeReqRes()

  describe 'initialize', ->
    afterEach ->
      commands.clear()
      reqres.clear()

    commandsError = new Error('ConfigurationManager initialization ERROR: commands is not configured')
    reqresError = new Error('ConfigurationManager initialization ERROR: reqres is not configured')
    configError = new Error('ConfigurationManager initialization ERROR: config is not configured')

    it 'throw initialization error when commands not configured', ->
      expect(-> new window.App.Core.ConfigurationManager()).toThrow(commandsError)

    it 'throw initialization error when reqres not configured', ->
      expect(-> new window.App.Core.ConfigurationManager(commands: commands)).toThrow(reqresError)

    it 'throw initialization error when config not configured', ->
      expect(-> new window.App.Core.ConfigurationManager(commands: commands, reqres: reqres)).toThrow(configError)

    it 'register reqres #get', ->
      new window.App.Core.ConfigurationManager(commands: commands, reqres: reqres, config: {})
      expect(reqres.isPresent('ConfigurationManager-get')).toEqual(true)

    it 'register command #set', ->
      new window.App.Core.ConfigurationManager(commands: commands, reqres: reqres, config: {})
      expect(commands.isPresent('ConfigurationManager-set')).toEqual(true)

    it 'store configuration', ->
      testConf =
        one:'test'
      subject = new window.App.Core.ConfigurationManager(commands: commands, reqres: reqres, config: testConf)
      expect(subject._config).toEqual(testConf)

  describe 'method', ->
    beforeEach ->
      @testConf =
        global:
          token: '123'
          user: 'ja'
        ui:
          design: 'slim'

      @subject = new window.App.Core.ConfigurationManager(commands: commands, reqres: reqres, config: @testConf)

    it '#get returns configuration by namespace and name', ->
      expect(@subject.get('global','token')).toEqual('123')

    it '#get returns configuration by namespace', ->
      expect(@subject.get('global')).toEqual(@testConf.global)

    it '#set override configuration by namespace and name', ->
      @subject.set('global','token', 1)
      expect(@subject._config.global.token).toEqual(1)

    it '#set store configuration by namespace and name', ->
      @subject.set('global','newOne', 2)
      expect(@subject._config.global.newOne).toEqual(2)

    it '#set override configuration by namespace', ->
      @subject.set('global', null, ui: 'none')
      expect(@subject._config.global).toEqual(ui: 'none')

    it '#set store configuration by namespace', ->
      @subject.set('user',null, name: 'pawel')
      expect(@subject._config.user).toEqual(name: 'pawel')