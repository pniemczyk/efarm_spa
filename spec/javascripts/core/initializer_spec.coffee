﻿#= require core

describe "Core initializer", ->
  config =
    ErrorLogger:
      propagateErrors: true
      url: 'fake'
      user: 'pawel'
      token: '123'
    ModuleDownloader:
      url: 'fakeUrl'
    HttpTransaction:
      token: '123'
    ModuleManager:
      modulesNamespace: {}
    TemplateProvider:
      templatesNamespace: {}

  describe 'throw configuration error', ->
    configNotPresent = new Error("CoreInitializer configuration ERROR: config is not configured")

    it 'when config is not present', ->
      expect(-> new window.App.Core.Initializer()).toThrow(configNotPresent)

  describe '#init', ->
    beforeEach ->
      @subject = new window.App.Core.Initializer(config)
      spyOn(@subject, '_editableFieldsBaseConfiguration')
    afterEach ->
      delete window.App.module
      delete window.App.transaction
      delete window.App.broker
      delete window.App.commands
      delete window.App.reqres
      delete window.App.template
      delete window.App.Helpers.clickEventName

    it 'initialize Error logger', ->
      expect(@subject._logger).toEqual(undefined)
      @subject.init()
      expect(@subject._logger).not.toEqual(undefined)
    it 'initialize router', ->
      expect(@subject._router).toEqual(undefined)
      @subject.init()
      expect(@subject._router).not.toEqual(undefined)
    it 'initialize module downloader', ->
      expect(@subject._moduleDownloader).toEqual(undefined)
      @subject.init()
      expect(@subject._moduleDownloader).not.toEqual(undefined)
    it 'initialize module manager', ->
      expect(@subject._moduleManager).toEqual(undefined)
      @subject.init()
      expect(@subject._moduleManager).not.toEqual(undefined)
    it 'map window.App.module to moduleManager #get', ->
      expect(window.App.module).toEqual(undefined)
      @subject.init()
      expect(window.App.module).not.toEqual(undefined)
    it 'initialize template provider', ->
      expect(@subject._templateProvider).toEqual(undefined)
      @subject.init()
      expect(@subject._templateProvider).not.toEqual(undefined)
    it 'map window.App.template to templateProvider #get', ->
      expect(window.App.template).toEqual(undefined)
      @subject.init()
      expect(window.App.template).not.toEqual(undefined)
    it 'initialize transaction as #_transaction', ->
      expect(@subject._transaction).toEqual(undefined)
      @subject.init()
      expect(@subject._transaction).not.toEqual(undefined)
    it 'initialize transaction', ->
      expect(window.App.transaction).toEqual(undefined)
      @subject.init()
      expect(window.App.transaction).not.toEqual(undefined)
    it 'initialize broker', ->
      expect(window.App.broker).toEqual(undefined)
      @subject.init()
      expect(window.App.broker).not.toEqual(undefined)
    it 'initialize commands', ->
      expect(window.App.commands).toEqual(undefined)
      @subject.init()
      expect(window.App.commands).not.toEqual(undefined)
    it 'initialize reqres', ->
      expect(window.App.reqres).toEqual(undefined)
      @subject.init()
      expect(window.App.reqres).not.toEqual(undefined)
    it 'initialize clickEventName', ->
      expect(window.App.Helpers.clickEventName).toEqual(undefined)
      @subject.init()
      expect(window.App.Helpers.clickEventName).not.toEqual(undefined)
    it 'initialize editable base configuration', ->
      @subject.init()
      expect(@subject._editableFieldsBaseConfiguration).toHaveBeenCalled()
    it 'execute #_backboneOverrode', ->
      @spyOn(@subject, '_backboneOverrode')
      @subject.init()
      expect(@subject._backboneOverrode).toHaveBeenCalled()