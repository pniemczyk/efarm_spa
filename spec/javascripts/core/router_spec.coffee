﻿#= require core/router
describe "Router module", ->

  describe 'initialization', ->
    it 'build router with default routes as default', ->
      subject = new window.App.Modules.Router()
      expect(subject.routes).toEqual("*actions": "defaultRoute")

    it 'build module with proper routes', ->
      subject = new window.App.Modules.Router('hotel/:mh': 'hotelDetails')
      expect(subject.routes).toEqual(
        "hotel/:mh": 'hotelDetails'
        "*actions": "defaultRoute"
      )

  describe 'methods', ->
    beforeEach ->
      @subject = new window.App.Modules.Router(
        'hotel/:mh': 'hotelDetails'
        'test': 'test'
      )

    it 'name is Router', ->  expect(@subject.name).toEqual('Router')

    describe '#addRoute', ->
      beforeEach ->
        @subject = new window.App.Modules.Router('hotel/:mh': 'hotelDetails')

      it 'add new route', ->
        action = 'one/:mm'
        expect(@subject.routes[action]).toEqual(null)
        @subject.addRoute('one/:mm', 'one')
        expect(@subject.routes[action]).toEqual('one')

      it 'throw Error when method name already registered', ->
        message = 'Router module addRoute ERROR: methodName `defaultRoute` is already registered'
        expect(=> @subject.addRoute('hotels', 'defaultRoute')).toThrow(new Error(message))

      it 'throw Error when method name exists in Routere instance', ->
        message = 'Router module addRoute ERROR: methodName `addRoute` is not allowed'
        expect(=> @subject.addRoute('members', 'addRoute')).toThrow(new Error(message))

      it 'throw Error when action already registered', ->
        message = 'Router module addRoute ERROR: action `hotel/:mh` is already registered'
        expect(=> @subject.addRoute('hotel/:mh', 'nextTest')).toThrow(new Error(message))

      it 'called _bindRoutes to refresh bindings as default', ->
        spyOn(@subject, '_bindRoutes')
        @subject.addRoute('one/:mm', 'one')
        expect(@subject._bindRoutes).toHaveBeenCalled()

      it 'do not call _bindRoutes when argument bind is false', ->
        spyOn(@subject, '_bindRoutes')
        @subject.addRoute('one/:mm', 'one', false)
        expect(@subject._bindRoutes).not.toHaveBeenCalled()

    describe '#addRoutes', ->
      beforeEach ->
        @subject = new window.App.Modules.Router()
        @testRoutes =
          'one': 'one'
          'two': 'two'

      it 'call addRoute for each route in object', ->
        spyOn(@subject,'addRoute')
        @subject.addRoutes(@testRoutes)
        expect(@subject.addRoute.calls.length).toEqual(2)

      it 'call _bindRoutes only one', ->
        spyOn(@subject,'addRoute')
        spyOn(@subject,'_bindRoutes')
        @subject.addRoutes(@testRoutes)
        expect(@subject._bindRoutes.calls.length).toEqual(1)


    describe '#removeRoute', ->
      beforeEach ->
        @action = 'hotel/:mh'
        @subject = new window.App.Modules.Router('hotel/:mh': 'hotelDetails')

      it 'remove route by action', ->
        expect(@subject.routes[@action]).toEqual('hotelDetails')
        @subject.removeRoute(@action)
        expect(@subject.routes[@action]).toEqual(null)

      it 'called _bindRoutes to refresh bindings', ->
        spyOn(@subject, '_bindRoutes')
        @subject.removeRoute(@action)
        expect(@subject._bindRoutes).toHaveBeenCalled()

      it 'remove event handler for route action', ->
        spyOn(@subject, 'off')
        @subject.removeRoute(@action)
        expect(@subject.off).toHaveBeenCalledWith(@action)

    describe '#removeRouteByMethodName', ->
      beforeEach ->
        @action = 'hotel/:mh'
        @methodName = 'hotelDetails'
        @subject = new window.App.Modules.Router('hotel/:mh': 'hotelDetails')

      it 'remove route by method name', ->
        expect(@subject.routes[@action]).toEqual(@methodName)
        @subject.removeRouteByMethodName(@methodName)
        expect(@subject.routes[@action]).toEqual(null)

      it 'called _bindRoutes to refresh bindings', ->
        spyOn(@subject, '_bindRoutes')
        @subject.removeRouteByMethodName(@methodName)
        expect(@subject._bindRoutes).toHaveBeenCalled()

      it 'remove event handler for route action', ->
        spyOn(@subject, 'off')
        @subject.removeRouteByMethodName(@methodName)
        expect(@subject.off).toHaveBeenCalledWith(@action)

    describe '#remove', ->
      beforeEach ->
        @subject = new window.App.Modules.Router('hotel/:mh': 'hotelDetails')

      it 'remove all event handler for route action', ->
        spyOn(@subject, 'off')
        @subject.remove()
        expect(@subject.off).toHaveBeenCalled()

      it 'called _bindRoutes to refresh bindings', ->
        spyOn(@subject, '_bindRoutes')
        @subject.remove()
        expect(@subject._bindRoutes).toHaveBeenCalled()

      it 'routes reset to default', ->
        expect(@subject.routes).not.toEqual(@subject._defaultRoutes)
        @subject.remove()
        expect(@subject.routes).toEqual(@subject._defaultRoutes)
