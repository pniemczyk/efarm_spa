﻿#= require core/error_logger

describe "ErrorLogger class", ->
  describe 'initialization', ->
    it 'throw url not configured error', ->
      error = new Error('ErrorLogger configuration ERROR: url is not configured')
      expect(-> new window.App.Core.ErrorLogger()).toThrow(error)

    describe 'can override default configuration', ->
      it '#propagateErrors', ->
        currentSubject = new window.App.Core.ErrorLogger(url: 'fake', propagateErrors: true)
        expect(currentSubject._config.propagateErrors).toBe(true)
      it '#method', ->
        currentSubject = new window.App.Core.ErrorLogger(url: 'fake', method: 'GET')
        expect(currentSubject._config.method).toEqual('GET')
      it '#user', ->
        currentSubject = new window.App.Core.ErrorLogger(url: 'fake', user: 'test_user')
        expect(currentSubject._config.user).toEqual('test_user')
      it '#token', ->
        currentSubject = new window.App.Core.ErrorLogger(url: 'fake', token: '123')
        expect(currentSubject._config.token).toEqual('123')
      it '#sendOnDomReady', ->
        currentSubject = new window.App.Core.ErrorLogger(url: 'fake', sendOnDomReady: false)
        expect(currentSubject._config.sendOnDomReady).toEqual(false)
      it '#sendOnlyOnDomReady', ->
        currentSubject = new window.App.Core.ErrorLogger(url: 'fake', sendOnlyOnDomReady: true)
        expect(currentSubject._config.sendOnlyOnDomReady).toEqual(true)
      it '#sendDelay', ->
        currentSubject = new window.App.Core.ErrorLogger(url: 'fake', sendDelay: 5000)
        expect(currentSubject._config.sendDelay).toEqual(5000)
#TODO: Test sending error to configured url
