﻿#= require core/module_manager
#= require support/backbone_wreqr_factory

describe "ModuleManager", ->
  bFac= new window.BackboneWreqrFactory()
  commands = bFac.fakeCommands()
  reqres = bFac.fakeReqRes()
  broker = bFac.fakeBroker()
  transaction = {}
  router =
    removeActions: []
    routes: []
    ons: []
    offs: []
    removeRoute: (action) -> @removeActions.push action
    clear: ->
      @removeActions = []
      @routes = []
      @ons = []
      @offs = []
    addRoute: (action, method) ->
      route = {}
      route[action] = method
      @routes.push route
    on: (action) -> @ons.push action
    off: (method) -> @offs.push method

  moduleDownloader =
    get: ->
  configurationManager =
    get: ->
  modulesNamespace = {}
  class modulesNamespace.testModuleOne
    @require: ->
    name: 'testModuleOne'
    remove: ->
    routes:
      'test': 'testMethod'
  class modulesNamespace.testModuleTwo
    @require: ->
      libs: ['commands', 'broker', 'reqres']
      config: 'testModuleTwo'
    name: 'testModuleTwo'
    remove: ->

  newSubject = ->
    new window.App.Core.ModuleManager(
              commands: commands
              reqres: reqres
              broker: broker
              router: router
              transaction: transaction
              moduleDownloader: moduleDownloader
              configurationManager: configurationManager
              modulesNamespace: modulesNamespace
            )

  describe 'initialize', ->
    afterEach ->
      commands.clear()
      reqres.clear()
      broker.clear()

    error = (name) -> new Error("ModuleManager initialization ERROR: #{name} is not configured")

    it 'throw initialization error when commands not configured', ->
      expect(-> new window.App.Core.ModuleManager()).toThrow(error('commands'))

    it 'throw initialization error when reqres not configured', ->
      expect(-> new window.App.Core.ModuleManager(commands: commands)).toThrow(error('reqres'))

    it 'throw initialization error when broker not configured', ->
      expect(-> new window.App.Core.ModuleManager(commands: commands, reqres: reqres)).toThrow(error('broker'))

    it 'throw initialization error when transaction not configured', ->
      expect(-> new window.App.Core.ModuleManager(commands: commands, reqres: reqres, broker: broker)).toThrow(error('transaction'))

    it 'throw initialization error when router not configured', ->
      expect(-> new window.App.Core.ModuleManager(
        commands: commands
        reqres: reqres
        broker: broker
        transaction: transaction
      )).toThrow(error('router'))

    it 'throw initialization error when moduleDownloader not configured', ->
      expect(-> new window.App.Core.ModuleManager(
        commands: commands
        reqres: reqres
        broker: broker
        router: router
        transaction: transaction
      )).toThrow(error('moduleDownloader'))

    it 'throw initialization error when moduleDownloader not configured', ->
      expect(-> new window.App.Core.ModuleManager(
        commands: commands
        reqres: reqres
        broker: broker
        router: router
        transaction: transaction
        moduleDownloader: moduleDownloader
      )).toThrow(error('configurationManager'))

    it 'throw initialization error when modulesNamespace not configured', ->
      expect(-> new window.App.Core.ModuleManager(
        commands: commands
        reqres: reqres
        broker: broker
        router: router
        transaction: transaction
        moduleDownloader: moduleDownloader
        configurationManager: configurationManager
      )).toThrow(error('modulesNamespace'))

    it 'register get event', ->
      expect(reqres.isPresent('ModuleManager-get')).toEqual(true)
    it 'register add event', ->
      expect(commands.isPresent('ModuleManager-add')).toEqual(true)
    it 'register remove event', ->
      expect(commands.isPresent('ModuleManager-remove')).toEqual(true)
    it 'register clear event', ->
      expect(commands.isPresent('ModuleManager-clear')).toEqual(true)

  describe 'method', ->
    describe '#add', ->
      beforeEach ->
        router.clear()
        @subject = newSubject()

      it 'add new module', ->
        expect(@subject._modules.length).toEqual(0)

        @subject.add('testModuleOne')
        expect(@subject._modules[0].name).toEqual('testModuleOne')

      it 'not add module when was added before', ->
        expect(@subject._modules.length).toEqual(0)
        @subject.add('testModuleOne')
        expect(@subject._modules.length).toEqual(1)
        expect(@subject._modules[0].name).toEqual('testModuleOne')

        @subject.add('testModuleOne')
        expect(@subject._modules.length).toEqual(1)

      it 'get configuration for module', ->
        spyOn(configurationManager, 'get').andReturn(test:true)
        @subject.add('testModuleTwo')
        expect(configurationManager.get).toHaveBeenCalledWith('testModuleTwo')

      it 'download module if not exist in namespace', ->
        spyOn(moduleDownloader, 'get').andReturn(modulesNamespace.testModuleTwo)
        @subject.add('unknown')
        expect(moduleDownloader.get).toHaveBeenCalled()

      it 'register routes when module has property routes', ->
        expect(router.routes).toEqual([])
        @subject.add('testModuleOne')
        expect(router.routes).toEqual([{ test : 'testMethod' }])

      it 'set handler for route action when module has property routers', ->
        expect(router.ons).toEqual([])
        @subject.add('testModuleOne')
        expect(router.ons).toEqual(['route:testMethod'])

    describe '#get', ->
      it 'return module by his name', ->
        subject = @subject = newSubject()
        subject._modules.push new modulesNamespace.testModuleTwo()
        subject._modules.push new modulesNamespace.testModuleOne()

        expect(subject.get('testModuleOne').name).toEqual('testModuleOne')

    describe '#remove', ->
      removeCall = 0
      beforeEach ->
        @subject = newSubject()
        @subject._modules.push new modulesNamespace.testModuleOne()
        removeCall = 0
        @subject._modules[0].remove = -> removeCall++
        router.clear()

      it 'execute remove method on module', ->
        expect(removeCall).toEqual(0)
        @subject.remove('testModuleOne')
        expect(removeCall).toEqual(1)

      it 'remove him from modules', ->
        expect(@subject._modules.length).toEqual(1)
        @subject.remove('testModuleOne')
        expect(@subject._modules.length).toEqual(0)

      it 'unregister module routes', ->
        expect(router.removeActions.length).toEqual(0)
        expect(router.offs.length).toEqual(0)
        @subject.remove('testModuleOne')
        expect(router.removeActions[0]).toEqual('test')
        expect(router.offs[0]).toEqual('route:testMethod')

    describe '#clear', ->
      removeCall = 0
      beforeEach ->
        @subject = newSubject()
        @subject._modules.push new modulesNamespace.testModuleTwo()
        @subject._modules.push new modulesNamespace.testModuleOne()
        removeCall = 0
        @subject._modules[0].remove = -> removeCall++
        @subject._modules[1].remove = -> removeCall++
        router.clear()

      it 'execute remove method on all modules and remove clear modules collection', ->
        expect(removeCall).toEqual(0)
        expect(@subject._modules.length).toEqual(2)
        @subject.clear()

        expect(@subject._modules.length).toEqual(0)
        expect(removeCall).toEqual(2)

      it 'unregister all routes', ->
        expect(router.removeActions).toEqual([])
        @subject.clear()
        expect(router.removeActions).toEqual(['test'])

    describe '#unbindEvents', ->
      beforeEach ->
        commands.clear()
        reqres.clear()
        @subject = newSubject()
      afterEach ->
        commands.clear()
        reqres.clear()

      it 'should unbind all events', ->
        expect(commands.names.length).toEqual(3)
        expect(reqres.names.length).toEqual(1)
        @subject.unbindEvents()
        expect(commands.names.length).toEqual(0)
        expect(reqres.names.length).toEqual(0)