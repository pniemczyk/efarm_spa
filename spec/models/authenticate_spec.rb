require 'spec_helper'

describe Authenticate do
  subject { Authenticate.new }

  describe '#by_token' do
    context 'user not found' do
      before { User.should_receive(:where).with('auth_token=?', 'token').and_return(nil) }

      it 'raises error' do
        expect { subject.by_token('token') }.to raise_error(Authenticate::AuthFailError)
      end
    end

    context 'user found' do
      before do
        @user = User.new
        User.should_receive(:where).with('auth_token=?', 'token').and_return(@user)
      end

      context 'user is locked' do
        before { @user.is_locked = true }

        it 'raises error' do
          expect { subject.by_token('token') }.to raise_error(Authenticate::AuthFailError)
        end
      end

      context 'token has expired' do
        before { @user.auth_token_expiration_date = Time.now - 1.day }

        it 'raises error' do
          expect { subject.by_token('token') }.to raise_error(Authenticate::AuthFailError)
        end
      end

      context 'everything is ok' do
        before { @user.auth_token_expiration_date = Time.now + 1.day }

        it 'extends token and returns user otherwise' do
          @user.should_receive(:update)

          subject.by_token('token').should == @user
        end
      end
    end
  end

  describe '#by_email' do
    context 'user not found' do
      before { User.should_receive(:where).with('email =?', 'e').and_return(nil) }

      it 'raises error' do
        expect { subject.by_email('e', 'p') }.to raise_error(Authenticate::AuthFailError)
      end
    end

    context 'user found' do
      before do
        @user = User.new
        User.should_receive(:where).with('email =?', 'e').and_return(@user)
      end

      context 'user is locked' do
        before { @user.is_locked = true }

        it 'raises error' do
          expect { subject.by_email('e', 'p') }.to raise_error(Authenticate::AuthFailError)
        end
      end

      context 'passwords not same' do
        before do
          @user.failed_attempts = 2
          @user.encrypted_password = '$2a$10$uqWzD2kDvl74S8Tpsg40J.w5.jmfZ3BUUgHXiVZgl3tx2LbZkAmYq'
        end

        it 'updates user password failures and raises error' do
          @user.should_receive(:update)

          expect { subject.by_email('e', 'p') }.to raise_error(Authenticate::AuthFailError)
          @user.failed_attempts.should == 3
        end
      end

      context 'passwords same' do
        before do
          @user.failed_attempts = 2
          @user.encrypted_password = '$2a$10$uqWzD2kDvl74S8Tpsg40J.w5.jmfZ3BUUgHXiVZgl3tx2LbZkAmYm'
        end

        it 'resets passwords failures, updates user and return' do
          @user.should_receive(:update)

          subject.by_email('e', 'secret').should == @user

          @user.failed_attempts.should == 0
        end
      end
    end
  end
end