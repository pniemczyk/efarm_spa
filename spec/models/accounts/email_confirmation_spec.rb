require 'spec_helper'

describe Accounts::EmailConfirmation do
  let(:user_repo) { double }
  let(:confirmation_token) { '12345' }

  let(:user) { User.new }

  before do
    user_repo
      .stub(:find_by_confirmation_token)
      .with(confirmation_token)
      .and_return(user)
  end

  subject { described_class.new(user_repo, confirmation_token) }

  describe '#valid?' do
    context 'confirmation_token not present' do
      let(:confirmation_token) { nil }

      specify do
        subject.valid?.should == false
        subject.errors[:confirmation_token].should_not be_blank
      end
    end

    context 'confirmation_token present' do
      context 'no user for confirmation_token' do
        let(:user) { nil }

        specify do
          subject.valid?.should == false
          subject.errors[:confirmation_token].should_not be_blank
        end
      end

      context 'user already confirmed' do
        let(:user) { User.new(is_confirmed: true) }

        specify do
          subject.valid?.should == false
          subject.errors[:confirmation_token].should_not be_blank
        end
      end
    end

    it 'returns true otherwise' do
      subject.valid?.should == true
    end
  end

  describe '#confirm' do
    let(:auth_token) { '12345678' }

    before do
      EncryptorDecryptor.should_receive(:generate_token).and_return(auth_token)
    end

    it 'returns updated user' do
      user_repo.should_receive(:update).with(user)

      confirmed_user = subject.confirm

      confirmed_user.auth_token.should == auth_token
      confirmed_user.auth_token_expiration_date.day == Time.now + 1.day
      confirmed_user.is_confirmed.should == true
    end
  end
end