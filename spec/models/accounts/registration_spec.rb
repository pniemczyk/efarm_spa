require 'spec_helper'

describe Accounts::Registration do
  let(:email) { 'jk@o2.pl' }
  let(:password) { 'password' }
  let(:password_confirmation) { 'password' }
  let(:user_repo) { double }
  let(:accept_terms) { 'on' }

  let(:opts) do
    {
      email: email,
      password: password,
      password_confirmation: password_confirmation,
      accept_terms: accept_terms
    }
  end

  subject { described_class.new(user_repo, opts) }

  describe '#valid?' do
    let(:user) { nil }

    before do
      user_repo.stub(:find_by_email).and_return(user)
    end

    context 'obligatory param is missing' do
      it 'returns false' do
        described_class.new(user_repo, opts.except(:email)).valid?.should == false
        described_class.new(user_repo, opts.except(:password)).valid?.should == false
        described_class.new(user_repo, opts.except(:password_confirmation)).valid?.should == false
      end
    end

    context 'email format not ok' do
      let(:email) { 'jk.o2.pl' }

      it 'returns false' do
        subject.valid?.should == false

        subject.errors[:email].should_not be_blank
      end
    end

    context 'password and password_confirmation are not equal' do
      let(:password) { '__password__' }

      it 'returns false' do
        subject.valid?.should == false

        subject.errors[:password].should_not be_blank
      end
    end

    context 'user found by email' do
      context 'user not confirmed' do
        let(:user) { double(is_confirmed: false) }

        it 'returns false' do
          subject.valid?.should == false

          subject.errors[:email].should_not be_blank
        end
      end

      context 'user confirmed' do
        let(:user) { double(is_confirmed: true) }

        it 'returns false' do
          subject.valid?.should == false

          subject.errors[:email].should_not be_blank
        end
      end
    end

    context 'terms not accepted' do
      let(:accept_terms) { nil }

      it 'returns false' do
        subject.valid?.should == false

        subject.errors[:accept_terms].should_not be_blank
      end
    end

    it 'returns true otherwise' do
      subject.valid?.should == true
    end
  end

  describe '#register' do
    let(:token) { 'token' }
    let(:encrypted_password) { 'secret' }

    before do
      EncryptorDecryptor.should_receive(:generate_password).with('password').and_return(encrypted_password)
      EncryptorDecryptor.should_receive(:generate_token).and_return(token)
      user_repo.should_receive(:insert)
    end

    it 'creates new user with email, encrypted password and token' do
      subject.stub(:valid?).and_return(true)

      new_user = subject.register

      new_user.email.should == 'jk@o2.pl'
      new_user.confirmation_token.should == token
      new_user.encrypted_password.should == encrypted_password
    end
  end
end