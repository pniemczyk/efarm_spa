source 'http://rubygems.org'

ruby '2.1.1'

gem 'rails', '~> 3.2.17'
gem 'rack', '~> 1.4.5'
gem 'haml'
gem 'bcrypt-ruby'
gem 'mail'
gem 'proffer'
gem 'light'
gem 'yajl-ruby', :require => 'yajl'
gem 'pg'
gem 'will_paginate'
gem 'rake'
gem 'elasticsearch'
gem 'hash_validator'
gem 'hashie'
gem 'puma'

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'

gem "rest-client"
gem 'ox'

group :production, :acceptance do
  gem 'newrelic_rpm'
  gem "nginx"
  gem 'unicorn'
end

group :development do
  gem 'quiet_assets'
  gem 'pry-rails'

  gem 'capistrano',         require: false
  gem 'capistrano-rvm',     require: false
  gem 'capistrano-rails',   require: false
  gem 'capistrano-bundler', require: false
  # gem 'capistrano-unicorn', require: false

end

group :test do
  gem 'sqlite3'
  gem 'guard-rails'
  gem 'rspec-rails'
end

group :development, :test do
  gem 'thin'
  gem 'turbo-sprockets-rails3'
  gem "teaspoon"
  gem "guard-teaspoon"
  gem "rb-fsevent"
  gem 'awesome_print'
  # gem 'better_errors'
  gem 'binding_of_caller'
  gem 'guard'
  gem 'guard-livereload', require: false
  gem 'guard-rspec',      require: false
  gem 'guard-rails-assets'
  gem 'guard-bundler'
  gem 'meta_request'
  #gem "jasminerice", :git => 'https://github.com/bradphelan/jasminerice.git'
end

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'jquery-rails'
  gem 'jquery-ui-rails'
  gem 'coffee-rails'
  gem 'sass-rails',   '~> 3.2.3'
  gem 'compass-rails'
  gem 'haml-rails'
  gem 'hamlbars'
  gem 'handlebars_assets'
  gem 'openlayers-rails', '~> 0.0.4'
  gem "jquery-fileupload-rails"
  gem 'breakpoint'
  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  gem 'therubyracer', :platforms => :ruby

  gem "closure-compiler"
  gem 'yui-compressor'
  gem 'uglifier', '>= 2.4.0'
end

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# To use debugger
# gem 'debugger'

# This version needs to be hardcoded for OpenShift compatibility
gem 'thor', '= 0.14.6'

# This needs to be installed so we can run Rails console on OpenShift directly
gem 'minitest'
